﻿using System;
using System.Windows.Forms;

namespace CDGFontEditor
{
    public partial class CharacterChoiceForm : Form
    {
        #region Construction

        public CharacterChoiceForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the character entered by the user.
        /// </summary>
        public char Character
        {
            get
            {
                return _Character;
            }
        }

        #endregion

        #region Event Handlers

        private void CharacterChoiceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK &&  _Character == char.MinValue)
            {
                MessageBox.Show("Please enter a character");
                e.Cancel = true;
            }
        }

        private void _CharTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_CharTextBox.Text.Length == 1)
            {
                _Character = _CharTextBox.Text[0];
            }
            else
            {
                _Character = char.MinValue;
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Character entered by the user
        /// </summary>
        char _Character;

        #endregion
    }
}
