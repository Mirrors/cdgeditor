﻿namespace CDGFontEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._Menu = new System.Windows.Forms.MenuStrip();
            this._FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._FileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._FileSave = new System.Windows.Forms.ToolStripMenuItem();
            this._FileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._FileSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._FileExit = new System.Windows.Forms.ToolStripMenuItem();
            this._EditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditAdd = new System.Windows.Forms.ToolStripMenuItem();
            this._EditDel = new System.Windows.Forms.ToolStripMenuItem();
            this._FontContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._AddContext = new System.Windows.Forms.ToolStripMenuItem();
            this._DelContext = new System.Windows.Forms.ToolStripMenuItem();
            this._EditPanel = new System.Windows.Forms.Panel();
            this._CharGrid = new Cdg.Controls.GridControl();
            this._HeightNumeric = new System.Windows.Forms.NumericUpDown();
            this._WidthNumeric = new System.Windows.Forms.NumericUpDown();
            this._WidthLabel = new System.Windows.Forms.Label();
            this._HeightTextBox = new System.Windows.Forms.Label();
            this._CharLabel = new System.Windows.Forms.Label();
            this._CharTextBox = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this._ArrowButton = new System.Windows.Forms.ToolStripButton();
            this._PencilButton = new System.Windows.Forms.ToolStripButton();
            this._CharacterListView = new BrightIdeasSoftware.ObjectListView();
            this._FontCharColumn = new BrightIdeasSoftware.OLVColumn();
            this._Menu.SuspendLayout();
            this._FontContextMenu.SuspendLayout();
            this._EditPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._HeightNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._WidthNumeric)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._CharacterListView)).BeginInit();
            this.SuspendLayout();
            // 
            // _Menu
            // 
            this._Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._FileMenu,
            this._EditMenu});
            this._Menu.Location = new System.Drawing.Point(0, 0);
            this._Menu.Name = "_Menu";
            this._Menu.Size = new System.Drawing.Size(531, 24);
            this._Menu.TabIndex = 1;
            this._Menu.Text = "menuStrip1";
            // 
            // _FileMenu
            // 
            this._FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._FileOpen,
            this._FileSave,
            this._FileSaveAs,
            this._FileSeparator1,
            this._FileExit});
            this._FileMenu.Name = "_FileMenu";
            this._FileMenu.Size = new System.Drawing.Size(35, 20);
            this._FileMenu.Text = "&File";
            // 
            // _FileOpen
            // 
            this._FileOpen.Name = "_FileOpen";
            this._FileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._FileOpen.Size = new System.Drawing.Size(163, 22);
            this._FileOpen.Text = "&Open...";
            this._FileOpen.Click += new System.EventHandler(this._FileOpen_Click);
            // 
            // _FileSave
            // 
            this._FileSave.Name = "_FileSave";
            this._FileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._FileSave.Size = new System.Drawing.Size(163, 22);
            this._FileSave.Text = "&Save";
            this._FileSave.Click += new System.EventHandler(this._FileSave_Click);
            // 
            // _FileSaveAs
            // 
            this._FileSaveAs.Name = "_FileSaveAs";
            this._FileSaveAs.Size = new System.Drawing.Size(163, 22);
            this._FileSaveAs.Text = "Save &As...";
            this._FileSaveAs.Click += new System.EventHandler(this._FileSaveAs_Click);
            // 
            // _FileSeparator1
            // 
            this._FileSeparator1.Name = "_FileSeparator1";
            this._FileSeparator1.Size = new System.Drawing.Size(160, 6);
            // 
            // _FileExit
            // 
            this._FileExit.Name = "_FileExit";
            this._FileExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._FileExit.Size = new System.Drawing.Size(163, 22);
            this._FileExit.Text = "E&xit";
            this._FileExit.Click += new System.EventHandler(this._FileExit_Click);
            // 
            // _EditMenu
            // 
            this._EditMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._EditAdd,
            this._EditDel});
            this._EditMenu.Name = "_EditMenu";
            this._EditMenu.Size = new System.Drawing.Size(37, 20);
            this._EditMenu.Text = "&Edit";
            // 
            // _EditAdd
            // 
            this._EditAdd.Name = "_EditAdd";
            this._EditAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Insert)));
            this._EditAdd.Size = new System.Drawing.Size(202, 22);
            this._EditAdd.Text = "&Add Character";
            this._EditAdd.Click += new System.EventHandler(this._EditAdd_Click);
            // 
            // _EditDel
            // 
            this._EditDel.Name = "_EditDel";
            this._EditDel.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this._EditDel.Size = new System.Drawing.Size(202, 22);
            this._EditDel.Text = "&Delete Character";
            this._EditDel.Click += new System.EventHandler(this._EditDel_Click);
            // 
            // _FontContextMenu
            // 
            this._FontContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._AddContext,
            this._DelContext});
            this._FontContextMenu.Name = "_FontContextMenu";
            this._FontContextMenu.Size = new System.Drawing.Size(117, 48);
            // 
            // _AddContext
            // 
            this._AddContext.Name = "_AddContext";
            this._AddContext.Size = new System.Drawing.Size(116, 22);
            this._AddContext.Text = "&Add";
            this._AddContext.Click += new System.EventHandler(this._EditAdd_Click);
            // 
            // _DelContext
            // 
            this._DelContext.Name = "_DelContext";
            this._DelContext.Size = new System.Drawing.Size(116, 22);
            this._DelContext.Text = "&Delete";
            this._DelContext.Click += new System.EventHandler(this._EditDel_Click);
            // 
            // _EditPanel
            // 
            this._EditPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._EditPanel.AutoScroll = true;
            this._EditPanel.Controls.Add(this._CharGrid);
            this._EditPanel.Location = new System.Drawing.Point(166, 59);
            this._EditPanel.Name = "_EditPanel";
            this._EditPanel.Size = new System.Drawing.Size(327, 226);
            this._EditPanel.TabIndex = 4;
            // 
            // _CharGrid
            // 
            this._CharGrid.CellSize = 12;
            this._CharGrid.Enabled = false;
            this._CharGrid.GridHeight = 10;
            this._CharGrid.GridWidth = 10;
            this._CharGrid.Location = new System.Drawing.Point(0, 0);
            this._CharGrid.Name = "_CharGrid";
            this._CharGrid.Selection = new System.Drawing.Rectangle(0, 0, 0, 0);
            this._CharGrid.SelectionMode = false;
            this._CharGrid.Size = new System.Drawing.Size(121, 121);
            this._CharGrid.TabIndex = 0;
            this._CharGrid.TabStop = false;
            this._CharGrid.Changed += new System.EventHandler(this._CharGrid_Changed);
            // 
            // _HeightNumeric
            // 
            this._HeightNumeric.Enabled = false;
            this._HeightNumeric.Location = new System.Drawing.Point(88, 160);
            this._HeightNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._HeightNumeric.Name = "_HeightNumeric";
            this._HeightNumeric.Size = new System.Drawing.Size(59, 20);
            this._HeightNumeric.TabIndex = 3;
            this._HeightNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this._HeightNumeric.ValueChanged += new System.EventHandler(this._Numeric_ValueChanged);
            // 
            // _WidthNumeric
            // 
            this._WidthNumeric.Enabled = false;
            this._WidthNumeric.Location = new System.Drawing.Point(88, 121);
            this._WidthNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._WidthNumeric.Name = "_WidthNumeric";
            this._WidthNumeric.Size = new System.Drawing.Size(59, 20);
            this._WidthNumeric.TabIndex = 2;
            this._WidthNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this._WidthNumeric.ValueChanged += new System.EventHandler(this._Numeric_ValueChanged);
            // 
            // _WidthLabel
            // 
            this._WidthLabel.AutoSize = true;
            this._WidthLabel.Location = new System.Drawing.Point(85, 105);
            this._WidthLabel.Name = "_WidthLabel";
            this._WidthLabel.Size = new System.Drawing.Size(38, 13);
            this._WidthLabel.TabIndex = 5;
            this._WidthLabel.Text = "Width:";
            // 
            // _HeightTextBox
            // 
            this._HeightTextBox.AutoSize = true;
            this._HeightTextBox.Location = new System.Drawing.Point(85, 144);
            this._HeightTextBox.Name = "_HeightTextBox";
            this._HeightTextBox.Size = new System.Drawing.Size(41, 13);
            this._HeightTextBox.TabIndex = 5;
            this._HeightTextBox.Text = "Height:";
            // 
            // _CharLabel
            // 
            this._CharLabel.AutoSize = true;
            this._CharLabel.Location = new System.Drawing.Point(85, 66);
            this._CharLabel.Name = "_CharLabel";
            this._CharLabel.Size = new System.Drawing.Size(56, 13);
            this._CharLabel.TabIndex = 5;
            this._CharLabel.Text = "Character:";
            // 
            // _CharTextBox
            // 
            this._CharTextBox.Enabled = false;
            this._CharTextBox.Location = new System.Drawing.Point(88, 82);
            this._CharTextBox.MaxLength = 1;
            this._CharTextBox.Name = "_CharTextBox";
            this._CharTextBox.Size = new System.Drawing.Size(20, 20);
            this._CharTextBox.TabIndex = 1;
            this._CharTextBox.TextChanged += new System.EventHandler(this._CharTextBox_TextChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ArrowButton,
            this._PencilButton});
            this.toolStrip1.Location = new System.Drawing.Point(496, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(35, 273);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "_ToolStrip";
            // 
            // _ArrowButton
            // 
            this._ArrowButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ArrowButton.Image = ((System.Drawing.Image)(resources.GetObject("_ArrowButton.Image")));
            this._ArrowButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._ArrowButton.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this._ArrowButton.Name = "_ArrowButton";
            this._ArrowButton.Size = new System.Drawing.Size(33, 36);
            this._ArrowButton.Text = "toolStripButton1";
            this._ArrowButton.Click += new System.EventHandler(this._ArrowButton_Click);
            // 
            // _PencilButton
            // 
            this._PencilButton.Checked = true;
            this._PencilButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this._PencilButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._PencilButton.Image = ((System.Drawing.Image)(resources.GetObject("_PencilButton.Image")));
            this._PencilButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this._PencilButton.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this._PencilButton.Name = "_PencilButton";
            this._PencilButton.Size = new System.Drawing.Size(33, 36);
            this._PencilButton.Text = "toolStripButton2";
            this._PencilButton.Click += new System.EventHandler(this._PencilButton_Click);
            // 
            // _CharacterListView
            // 
            this._CharacterListView.AllColumns.Add(this._FontCharColumn);
            this._CharacterListView.AlternateRowBackColor = System.Drawing.Color.Empty;
            this._CharacterListView.AlwaysGroupByColumn = null;
            this._CharacterListView.AlwaysGroupBySortOrder = System.Windows.Forms.SortOrder.None;
            this._CharacterListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this._CharacterListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._FontCharColumn});
            this._CharacterListView.ContextMenuStrip = this._FontContextMenu;
            this._CharacterListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._CharacterListView.HideSelection = false;
            this._CharacterListView.SelectedBackColor = System.Drawing.Color.Empty;
            this._CharacterListView.SelectedForeColor = System.Drawing.Color.Empty;
            this._CharacterListView.LastSortColumn = null;
            this._CharacterListView.LastSortOrder = System.Windows.Forms.SortOrder.None;
            this._CharacterListView.Location = new System.Drawing.Point(12, 59);
            this._CharacterListView.Name = "_CharacterListView";
            this._CharacterListView.ShowGroups = false;
            this._CharacterListView.Size = new System.Drawing.Size(70, 226);
            this._CharacterListView.TabIndex = 0;
            this._CharacterListView.UseCompatibleStateImageBehavior = false;
            this._CharacterListView.View = System.Windows.Forms.View.Details;
            this._CharacterListView.SelectionChanged += new System.EventHandler(this._CharacterListView_SelectionChanged);
            // 
            // _FontCharColumn
            // 
            this._FontCharColumn.AspectName = "Character";
            this._FontCharColumn.FillsFreeSpace = true;
            this._FontCharColumn.Text = "Character";
            this._FontCharColumn.Width = 65;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 297);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this._CharTextBox);
            this.Controls.Add(this._HeightTextBox);
            this.Controls.Add(this._CharLabel);
            this.Controls.Add(this._WidthLabel);
            this.Controls.Add(this._WidthNumeric);
            this.Controls.Add(this._HeightNumeric);
            this.Controls.Add(this._EditPanel);
            this.Controls.Add(this._CharacterListView);
            this.Controls.Add(this._Menu);
            this.MainMenuStrip = this._Menu;
            this.Name = "MainForm";
            this.Text = "CDG Font Editor";
            this._Menu.ResumeLayout(false);
            this._Menu.PerformLayout();
            this._FontContextMenu.ResumeLayout(false);
            this._EditPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._HeightNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._WidthNumeric)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._CharacterListView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView _CharacterListView;
        private System.Windows.Forms.MenuStrip _Menu;
        private System.Windows.Forms.ToolStripMenuItem _FileMenu;
        private System.Windows.Forms.ToolStripMenuItem _FileOpen;
        private System.Windows.Forms.ToolStripMenuItem _FileSave;
        private System.Windows.Forms.ToolStripMenuItem _FileSaveAs;
        private System.Windows.Forms.ToolStripSeparator _FileSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _FileExit;
        private System.Windows.Forms.ToolStripMenuItem _EditMenu;
        private System.Windows.Forms.ToolStripMenuItem _EditAdd;
        private System.Windows.Forms.ToolStripMenuItem _EditDel;
        private System.Windows.Forms.ContextMenuStrip _FontContextMenu;
        private System.Windows.Forms.ToolStripMenuItem _AddContext;
        private System.Windows.Forms.ToolStripMenuItem _DelContext;
        private BrightIdeasSoftware.OLVColumn _FontCharColumn;
        private Cdg.Controls.GridControl _CharGrid;
        private System.Windows.Forms.Panel _EditPanel;
        private System.Windows.Forms.NumericUpDown _HeightNumeric;
        private System.Windows.Forms.NumericUpDown _WidthNumeric;
        private System.Windows.Forms.Label _WidthLabel;
        private System.Windows.Forms.Label _HeightTextBox;
        private System.Windows.Forms.Label _CharLabel;
        private System.Windows.Forms.TextBox _CharTextBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton _ArrowButton;
        private System.Windows.Forms.ToolStripButton _PencilButton;
    }
}

