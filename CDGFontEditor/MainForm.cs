﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Cdg.Validation;

namespace CDGFontEditor
{
    /// <summary>
    /// The main user interface for the CDG Font Editor.
    /// </summary>
    public partial class MainForm : Form
    {
        #region Construction

        public MainForm()
        {
            InitializeComponent();

            _Font = new Cdg.Validation.Font("Undefined");
            _CharacterListView.Objects = _Font.Characters;

            EnableEditControls();
        }

        #endregion

        #region Event Handlers

        #region Menus

        #region File Menu

        private void _FileOpen_Click(object sender, EventArgs e)
        {

        }

        private void _FileSave_Click(object sender, EventArgs e)
        {

        }

        private void _FileSaveAs_Click(object sender, EventArgs e)
        {

        }

        private void _FileExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region Edit Menu

        private void _EditAdd_Click(object sender, EventArgs e)
        {
            CharacterChoiceForm form = new CharacterChoiceForm();
            if (DialogResult.OK == form.ShowDialog(this))
            {
                FontChar fontChar = new FontChar(form.Character, new Size(10, 10));
                _Font.Characters.Add(fontChar);
                _CharacterListView.Objects = _Font.Characters;

                // Select added item
                _CharacterListView.SelectedObject = fontChar;
                _CharacterListView.SelectedItem.EnsureVisible();
            }
        }

        private void _EditDel_Click(object sender, EventArgs e)
        {
            FontChar fontChar = (FontChar)_CharacterListView.SelectedObject;

            _Font.Characters.Remove(fontChar);
            _CharacterListView.Objects = _Font.Characters;
            EnableEditControls();
        }

        #endregion

        #endregion

        #region Controls

        private void _CharacterListView_SelectionChanged(object sender, EventArgs e)
        {
            _SelectionChanging = true;

            EnableEditControls();

            FontChar fontChar = (FontChar)_CharacterListView.SelectedObject;
            if (fontChar != null)
            {
                _CharGrid.Pixels = fontChar.Pixels;
                _CharTextBox.Text = fontChar.Character.ToString();
                _WidthNumeric.Value = fontChar.Pixels.Size.Width;
                _HeightNumeric.Value = fontChar.Pixels.Size.Height;

                //_CharGrid.Selection = new Rectangle(1, 0, 4, 2);

                _CharGrid.Visible = true;
            }
            else
            {
                _CharGrid.Visible = false;
            }

            _SelectionChanging = false;
        }

        private void _Numeric_ValueChanged(object sender, EventArgs e)
        {
            if (sender == _HeightNumeric)
            {
                _CharGrid.GridHeight = (int)_HeightNumeric.Value;
            }
            else
            {
                _CharGrid.GridWidth = (int)_WidthNumeric.Value;
            }
        }
        
        private void _CharTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!_SelectionChanging)
            {
                FontChar fontChar = (FontChar)_CharacterListView.SelectedObject;
                if (_CharTextBox.Text.Length == 1)
                {
                    fontChar.Character = _CharTextBox.Text[0];
                    _CharacterListView.Objects = _Font.Characters;
                    _CharacterListView.SelectedObject = fontChar;
                }
            }
        }

        private void _CharGrid_Changed(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Tool buttons

        private void _ArrowButton_Click(object sender, EventArgs e)
        {
            _ArrowButton.Checked = true;
            _PencilButton.Checked = false;
            _CharGrid.SelectionMode = true;
        }

        private void _PencilButton_Click(object sender, EventArgs e)
        {
            _ArrowButton.Checked = false;
            _ArrowButton.Checked = false;
            _CharGrid.SelectionMode = false;

        }

        #endregion

        #endregion

        #region Private Methods

        void EnableEditControls()
        {
            bool enable = _CharacterListView.SelectedObjects.Count == 1;
            _CharTextBox.Enabled = enable;
            _WidthNumeric.Enabled = enable;
            _HeightNumeric.Enabled = enable;
            _EditPanel.Enabled = enable;
            _CharGrid.Enabled = enable;
            _DelContext.Enabled = enable;

            _EditDel.Enabled = _CharacterListView.SelectedObjects.Count > 0;
        }

        #endregion

        #region Data

        /// <summary>
        /// The font being edited.
        /// </summary>
        Cdg.Validation.Font _Font;

        /// <summary>
        /// Indicates that the selection in the character list view
        /// is changing - this stops the textbox change notification
        /// from changing the list view.
        /// </summary>
        bool _SelectionChanging;

        #endregion
    }
}
