﻿using System;
using System.Windows.Forms;

namespace CDGEditor
{
    public partial class InsertForm : Form
    {
        #region Constants

        enum Mode
        {
            ChunkCount,
            TimeSpan
        }

        #endregion

        #region Construction

        public InsertForm()
        {
            InitializeComponent();
        }

        #endregion 

        #region Properties

        /// <summary>
        /// Gets the number of chunks chosen.
        /// </summary>
        public int ChunkCount
        {
            get
            {
                return _ChunkCount;
            }
        }

        #endregion

        #region Event Handlers

        private void _ChunkModeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (_ChunkModeRadioButton.Checked &&_Mode != Mode.ChunkCount)
            {
                _Mode = Mode.ChunkCount;
                int seconds = (int)_AmountNumeric.Value;
                int chunks = seconds * Cdg.CdgFile.ChunksPerSecond;
                _AmountNumeric.Value = chunks;
                _ModeLabel.Text = "Chunks";
            }
        }

        private void _TimeModeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (_TimeModeRadioButton.Checked && _Mode != Mode.TimeSpan)
            {
                _Mode = Mode.TimeSpan;
                int chunks = (int)_AmountNumeric.Value;
                int seconds = chunks / Cdg.CdgFile.ChunksPerSecond;
                _AmountNumeric.Value = seconds;
                _ModeLabel.Text = "Seconds";
            }
        }

        private void _AmountNumeric_ValueChanged(object sender, EventArgs e)
        {
            if (_Mode == Mode.ChunkCount)
            {
                _ChunkCount = (int)_AmountNumeric.Value;
            }
            else
            {
                _ChunkCount = (int)_AmountNumeric.Value * Cdg.CdgFile.ChunksPerSecond;
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Current mode.
        /// </summary>
        Mode _Mode = Mode.ChunkCount;

        /// <summary>
        /// Number of chunks chosen
        /// </summary>
        int _ChunkCount = 1;

        #endregion
    }
}
