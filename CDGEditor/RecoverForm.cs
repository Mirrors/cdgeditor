﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Cdg.Chunks;

namespace CDGEditor
{
    public partial class RecoverForm : Form
    {
        #region Constants

        const int RANGE = 50; // Number of chunks before and after to show

        #endregion

        #region Display Helper Class

        class DisplayChunk
        {
            public DisplayChunk(Chunk.InstructionType type)
            {
                _Type = type;
            }

            public Chunk.InstructionType Type
            {
                get
                {
                    return _Type;
                }
            }

            public override string ToString()
            {
                return Chunk.GetTypeDescription(_Type);
            }

            Chunk.InstructionType _Type;
        }


        #endregion

        #region Construction

        public RecoverForm(Cdg.CdgFile file, int cdgIndex)
        {
            InitializeComponent();

            _File = file;

            // Determine the range of chunks to show.
            _StartIndex = 0;

            if (file.CdgChunks.Count > 0 && cdgIndex >= 0)
            {
                _NearestCDGChunk = file.CdgChunks[cdgIndex];
                _StartIndex = _NearestCDGChunk.Index.Value;
            }

            int index = 0;
            foreach (Chunk.InstructionType type in Enum.GetValues(typeof(Chunk.InstructionType)))
            {
                _InstructionComboBox.Items.Add(new DisplayChunk(type));

                if (_NearestCDGChunk != null)
                {
                    if (_NearestCDGChunk.Type == type)
                    {
                        _InstructionComboBox.SelectedIndex = index;
                    }
                }
                else
                {
                    if (type == Chunk.InstructionType.TileXor)
                    {
                        _InstructionComboBox.SelectedIndex = index;
                    }
                }
                index++;
            }            

            _ChunkList.RowFormatter =
                delegate(BrightIdeasSoftware.OLVListItem olvItem)
                {
                    Chunk listChunk = (Chunk)olvItem.RowObject;
                    if (listChunk.Type == Chunk.InstructionType.Unknown)
                    {
                        olvItem.ForeColor = Color.Gray;
                    }
                };

            PopulateChunkList();
        }

        void PopulateChunkList()
        {
            this.Cursor = Cursors.WaitCursor;

            // Find the chunk index to start at.
            int index = _StartIndex;
            int start = Math.Max(0, index - RANGE);
            int end = Math.Min(_File.Chunks.Count - 1, index + RANGE);

            _ChunkList.BeginUpdate();
            _ChunkList.ClearObjects();

            for (int i = start; i <= end; i++)
            {
                _ChunkList.AddObject(_File.Chunks[i]);
            }
            _ChunkList.EndUpdate();

            if (_NearestCDGChunk != null)
            {
                _ChunkList.SelectObject(_NearestCDGChunk);
                _ChunkList.FocusedItem = _ChunkList.SelectedItem;
                _ChunkList.EnsureVisible(_ChunkList.SelectedIndex);
            }

            _ShowingLabel.Text = string.Format("Showing {0} chunks before and after:", RANGE);
            _TimeTextBox.Text = _File.Chunks[_StartIndex].Time;

            this.Cursor = Cursors.Arrow;
        }

        #endregion

        #region Properties

        public List<Chunk> SelectedChunks
        {
            get
            {
                List<Chunk> result = new List<Chunk>();
                if (_SelectedChunks != null)
                {
                    foreach (Chunk chunk in _SelectedChunks)
                    {
                        if (!chunk.Cdg)
                        {
                            result.Add(chunk);
                        }
                    }
                }
                return result;
            }
        }

        public Chunk.InstructionType ChunkType
        {
            get
            {
                return (_InstructionComboBox.SelectedItem as DisplayChunk).Type;
            }
        }

        #endregion

        #region Event Handlers

        private void _ChunkList_SelectionChanged(object sender, EventArgs e)
        {
            _SelectedChunks = new ArrayList(_ChunkList.SelectedObjects);
        }

        private void _ChunkList_DoubleClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void _GotoButton_Click(object sender, EventArgs e)
        {
            string[] parts = _TimeTextBox.Text.Split(':');
            int minutes = int.Parse(parts[0]);
            int seconds = int.Parse(parts[1]);
            int ms = int.Parse(parts[2]);

            int chunkIndex = minutes * 18000 +
                seconds * 300 +
                (int)(ms * 0.3);

            if (chunkIndex >= 0 && chunkIndex < _File.Chunks.Count)
            {
                _StartIndex = chunkIndex;
                _NearestCDGChunk = null;
                PopulateChunkList();

                Chunk nearestChunk = _File.Chunks[chunkIndex];
                _ChunkList.SelectObject(nearestChunk);
                _ChunkList.FocusedItem = _ChunkList.SelectedItem;
                _ChunkList.EnsureVisible(_ChunkList.SelectedIndex);
                _ChunkList.Focus();
            }
            else
            {
                MessageBox.Show(string.Format("Cannot navigate to {0} File is only {1} long.",
                    _TimeTextBox.Text, _File.Chunks[_File.Chunks.Count - 1].Time));
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// CDG File we are recovering a chunk from.
        /// </summary>
        Cdg.CdgFile _File;

        /// <summary>
        /// Chunks selected for recovery
        /// </summary>
        ArrayList _SelectedChunks;

        /// <summary>
        /// Index of first chunk to show in chunk list.
        /// </summary>
        int _StartIndex = 0;

        /// <summary>
        /// Nearest CDG Chunk.
        /// </summary>
        Chunk _NearestCDGChunk;

        #endregion        
    }
}
