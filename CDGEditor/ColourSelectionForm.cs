﻿using System;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace CDGEditor
{
    public partial class ColourSelectionForm : Form
    {
        #region Construction

        public ColourSelectionForm(ColorPalette palette, int? colourIndex)
        {
            InitializeComponent();

            if (colourIndex.HasValue && colourIndex <= _ColourNumeric.Maximum)
            {
                _ColourNumeric.Value = colourIndex.Value;
            }
            else
            {
                _ColourNumeric.Controls[1].Text = "";
            }

            _Palette = palette;

            for (int index = 0; index < 16; index++)
            {
                Cdg.Controls.ColourControl control = 
                    (Cdg.Controls.ColourControl)Controls[index];

                control.Palette = palette;
                control.Clicked += new EventHandler(control_Clicked);
                control.Index = index;
                if (index == colourIndex)
                {
                    control.Selected = true;
                }
            }
        }

        #endregion

        #region Properties

        public bool EditColours
        {
            get
            {
                return _EditColours;
            }
            set
            {
                _EditColours = value;
                foreach (Control control in Controls)
                {
                    Cdg.Controls.ColourControl colourControl = control as Cdg.Controls.ColourControl;
                    if (colourControl != null)
                    {
                        colourControl.ReadOnly = _EditColours;
                        colourControl.ColourChanged += new EventHandler(ColourControl_ColourChanged);
                    }
                }
            }
        }

        public ColorPalette Palette
        {
            get
            {
                return _Palette;
            }
        }

        void ColourControl_ColourChanged(object sender, EventArgs e)
        {
            
        }

        public bool HalfSize
        {
            get
            {
                return Size == MinimumSize;
            }
            set
            {
                _ColourControl1.Selected = false;
                Size = value ? MinimumSize : MaximumSize;
                _EditColours = false;
            }
        }

        public bool ShowFirstHalf
        {
            set
            {
                if (value == false)
                {
                    // Re-number colour controls
                    int index = 8;
                    foreach(Cdg.Controls.ColourControl control in Controls)
                    {                      
                        control.Index = index++;
                        if (index > 15)
                            break;
                        //(Controls[i] as CDG.Controls.ColourControl).Index = i + 8;
                    }
                }
            }
        }

        public int? ColourIndex
        {
            get
            {
                return _Colour;
            }
        }

        public int? RepeatValue
        {
            get
            {
                return _RepeatValue;
            }
            set
            {
                _RepeatValue = value;
                _RepeatNumeric.Visible = true;
                _RepeatLabel.Visible = true;
                if (value.HasValue)
                {
                    _RepeatNumeric.Value = Math.Max(0, Math.Min(value.Value, _RepeatNumeric.Maximum));
                }
                else
                {
                    _RepeatNumeric.Controls[1].Text = "";
                }
            }
        }

        #endregion

        #region Event Handlers

        private void _RepeatNumeric_ValueChanged(object sender, EventArgs e)
        {
            _RepeatValue = (int)_RepeatNumeric.Value;
        }

        private void _ColourNumeric_ValueChanged(object sender, EventArgs e)
        {
            _Colour = (int)_ColourNumeric.Value;
            foreach (Control control in Controls)
            {
                Cdg.Controls.ColourControl colourControl = control as Cdg.Controls.ColourControl;
                if (colourControl != null)
                {
                    colourControl.Selected = colourControl.Index == _Colour;
                }
            }
        }
        
        void control_Clicked(object sender, EventArgs e)
        {
            Cdg.Controls.ColourControl senderControl = (sender as Cdg.Controls.ColourControl);

            if (!HalfSize)
            {
                Cdg.Controls.ColourControl control =
                    (Cdg.Controls.ColourControl)Controls[(int)_ColourNumeric.Value];
                control.Selected = false;
                _Colour = senderControl.Index.Value;
                _ColourNumeric.Value = senderControl.Index.Value;
                senderControl.Selected = true;
            }
            else
            {
                ColorDialog form = new ColorDialog();
                form.Color = senderControl.Palette.Entries[senderControl.Index.Value];
                if (DialogResult.OK == form.ShowDialog(this))
                {
                    _Palette.Entries[senderControl.Index.Value] = form.Color;
                    senderControl.Palette = _Palette;
                }
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Whether the user is allowed to edit the colours shown (for Load Color command)
        /// </summary>
        bool _EditColours = false;

        /// <summary>
        /// Colour palette to use
        /// </summary>
        ColorPalette _Palette;

        /// <summary>
        /// Selected colour index
        /// </summary>
        int? _Colour = null;

        /// <summary>
        /// Repeat value
        /// </summary>
        int? _RepeatValue = null;

        #endregion
    }
}
