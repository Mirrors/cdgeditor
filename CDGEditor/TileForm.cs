﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using Cdg.Chunks;

namespace CDGEditor
{
    public partial class TileForm : Form
    {
        #region Construction

        public TileForm()
        {
            InitializeComponent();
            _OnColour.ReadOnly = false;
            _OffColour.ReadOnly = false;

            _OnColour.ColourChanged += new EventHandler(_Colour_ColourChanged);
            _OffColour.ColourChanged +=new EventHandler(_Colour_ColourChanged);

            _TileGrid.Changed += new EventHandler(_TileGrid_Changed);
        }

        #endregion

        #region Events

        public class TileFormEventArgs : EventArgs
        {
            public TileFormEventArgs(bool typeChanged)
            {
                TypeChanged = typeChanged;
            }

            public bool TypeChanged;
        };

        public event EventHandler Changed;
        void RaiseChanged(bool typeChanged)
        {
            if (Changed != null)
            {
                Changed(this, new TileFormEventArgs(typeChanged));
            }
        }

        public event EventHandler Canceled;
        void RaiseCanceled()
        {
            if (Canceled != null)
            {
                Canceled(this, new EventArgs());
            }
        }

        #endregion

        #region Properties

        public System.Drawing.Imaging.ColorPalette Palette
        {
            set
            {
                _Palette = value;
                _OffColour.Palette = value;
                _OnColour.Palette = value;
            }
        }

        public List<TileBlock> Chunks { get; set; }

        public TileBlock CommonChunk
        {
            get
            {
                return _Chunk;
            }
            set
            {
                _Chunk = value;
                Text = string.Format("Edit Tile Block {0}", _Chunk.Time);

                _TileGrid.Chunk = value;

                if (_Chunk.Column.HasValue)
                {
                    _XNumeric.Value = Math.Min(_Chunk.Column.Value, _XNumeric.Maximum);
                }
                else
                {
                    _XNumeric.Controls[1].Text = "";
                }

                if (_Chunk.Row.HasValue)
                {
                    _YNumeric.Value = Math.Min(_Chunk.Row.Value, _YNumeric.Maximum);
                }
                else
                {
                    _YNumeric.Controls[1].Text = "";
                }

                _OffColour.Index = _Chunk.OffColour;
                _OnColour.Index = _Chunk.OnColour;

                if (_Chunk.TileType == TileBlock.TileBlockType.Normal)
                {
                    _NormalRadioButton.Checked = true;
                }
                else
                {
                    _XorRadioButton.Checked = true;
                }

            }
        }

        #endregion

        #region Event Handlers

        void _Colour_ColourChanged(object sender, EventArgs e)
        {
            if (sender == _OffColour)
            {
                _Chunk.OffColour = _OffColour.Index.Value;
            }
            else
            {
                _Chunk.OnColour = _OnColour.Index.Value;
            }

            RaiseChanged(false);
        }

        void _TileGrid_Changed(object sender, EventArgs e)
        {
            RaiseChanged(false);
        }

        private void _OKButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _CancelButton_Click(object sender, EventArgs e)
        {
            RaiseCanceled();
            Close();
        }

        private void _NormalRadioButton_Click(object sender, EventArgs e)
        {
            _Chunk.TileType = TileBlock.TileBlockType.Normal;
            RaiseChanged(true);
        }

        private void _XorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            _Chunk.TileType = TileBlock.TileBlockType.Xor;
            RaiseChanged(true);
        }

        private void _Numeric_ValueChanged(object sender, EventArgs e)
        {
            if (sender == _XNumeric)
            {
                _Chunk.Column = (int)_XNumeric.Value;
            }
            else
            {
                _Chunk.Row = (int)_YNumeric.Value;
            }
            RaiseChanged(false);
        }

        private void _PatternButton_Click(object sender, EventArgs e)
        {
            int pattern = int.Parse((string)((sender as ToolStripButton).Tag));
            _TileGrid.SetPattern(pattern);
        }
        
        #endregion

        #region Data

        /// <summary>
        /// The chunk being edited
        /// </summary>
        TileBlock _Chunk;

        /// <summary>
        /// The palette to use for the colour selection controls
        /// </summary>
        System.Drawing.Imaging.ColorPalette _Palette;

        #endregion

        #region Persistence

        [Persist]
        public Point TileFormLocation
        {
            get
            {
                return Location;
            }
            set
            {
                Location = value;
            }
        }

        private void TileForm_Load(object sender, EventArgs e)
        {
            Persist.Load(this);
        }

        private void TileForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Persist.Save(this);
        }

        #endregion
    }
}
