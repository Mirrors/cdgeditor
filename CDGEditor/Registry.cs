﻿using System;
using System.Collections;
using System.Reflection;

namespace CDGEditor
{
    class Registry
    {
        #region Construction

        public Registry(string keyName)
        {
            m_Key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(string.Format(@"Software\dbs\CDGEditor\{0}", keyName));
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sets the specified entry
        /// </summary>
        /// <param name="entry">Entry to set</param>
        /// <param name="val">Value to store in the entry</param>
        public void SetValue(string entry, object val)
        {
            if (val != null)
            {
                m_Key.SetValue(entry, val);
            }
        }

        /// <summary>
        /// Gets the value of the entry specified
        /// </summary>
        /// <param name="entry">Entry to get value for from registry</param>
        /// <param name="obj">The object to populate</param>
        public object GetValue(string entry, object obj)
        {
            Type type = obj.GetType();

            if (type.IsPrimitive || obj is string)
            {
                // Value types can be obtained directly
                Object keyObj = m_Key.GetValue(entry);
                if (null != keyObj)
                {
                    if (obj is Boolean)
                    {
                        obj = Convert.ToBoolean(keyObj);
                    }
                    else
                    {
                        obj = keyObj;
                    }
                }
            }
            else
            {
                if (type.IsArray)
                {
                    // We only support arrays of strings (for now)
                    obj = GetStringArray(entry);
                }
                else
                {
                    // Other types have to be obtained by their properties
                    Hashtable regProperties = GetProperties(entry);

                    PropertyInfo[] properties = type.GetProperties();
                    foreach (PropertyInfo property in properties)
                    {
                        if (property.CanWrite)
                        {
                            object propVal = property.GetValue(obj, null);
                            if (propVal.GetType() == typeof(int))
                            {
                                if (regProperties.Contains(property.Name))
                                {
                                    int val = Convert.ToInt32(regProperties[property.Name]);
                                    property.SetValue(obj, val, null);
                                }
                            }
                        }
                    }
                }
            }
            return obj;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Gets the property values for the specified entry from the registry
        /// </summary>
        /// <param name="entry">Name of entry to get property values from</param>
        private Hashtable GetProperties(string entry)
        {
            Hashtable result = new Hashtable();

            string valStr = (string)m_Key.GetValue(entry);

            if (valStr != null)
            {
                valStr = valStr.Trim('{', '}');
                string[] fields = valStr.Split(',');
                foreach (string field in fields)
                {
                    string[] parts = field.Split('=');
                    if (parts.Length == 2)
                    {
                        result.Add(parts[0].Trim(), parts[1].Trim());
                    }
                }
            }
            return result;
        }

        private string[] GetStringArray(string entry)
        {
            string[] result = (string[])m_Key.GetValue(entry);
            return result;
        }

        #endregion

        #region Data

        /// <summary>
        /// Registry key to read/write settings
        /// </summary>
        private Microsoft.Win32.RegistryKey m_Key;

        #endregion
    }
}
