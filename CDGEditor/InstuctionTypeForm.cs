﻿using System.Windows.Forms;

using Cdg.Chunks;

namespace CDGEditor
{
    public partial class InstuctionTypeForm : Form
    {
        class DisplayChunk
        {
            public DisplayChunk(Chunk.InstructionType type)
            {
                _Type = type;
            }

            public Chunk.InstructionType Type
            {
                get
                {
                    return _Type;
                }
            }

            public override string ToString()
            {
                return Chunk.GetTypeDescription(_Type);
            }

            Chunk.InstructionType _Type;
        }

        public InstuctionTypeForm()
        {
            InitializeComponent();

            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.BorderPreset));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.DefTransColor));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.LoadColTableLow));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.LoadColTableHigh));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.MemoryPreset));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.ScrollCopy));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.ScrollPreset));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.TileNormal));
            _TypeComboBox.Items.Add(new DisplayChunk(Chunk.InstructionType.TileXor));

            _TypeComboBox.SelectedIndex = 0;

        }

        public Chunk.InstructionType Type
        {
            get
            {
                return ((DisplayChunk)_TypeComboBox.SelectedItem).Type;
            }
            set
            {
                _TypeComboBox.SelectedIndex = 0;

                int index = 0;
                foreach (DisplayChunk item in _TypeComboBox.Items)
                {
                    if (item.Type == value)
                    {
                        _TypeComboBox.SelectedIndex = index;
                        break;
                    }

                    index++;
                }
            }
        }
    }
}
