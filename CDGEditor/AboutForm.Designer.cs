﻿namespace CDGEditor
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this._OKButton = new System.Windows.Forms.Button();
            this._PictureBox = new System.Windows.Forms.PictureBox();
            this._VersionLabel = new System.Windows.Forms.Label();
            this._BlurbLabel = new System.Windows.Forms.Label();
            this._SourceForgeLinkLabel = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this._PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // _OKButton
            // 
            this._OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OKButton.Location = new System.Drawing.Point(156, 161);
            this._OKButton.Name = "_OKButton";
            this._OKButton.Size = new System.Drawing.Size(75, 23);
            this._OKButton.TabIndex = 4;
            this._OKButton.Text = "&OK";
            this._OKButton.UseVisualStyleBackColor = true;
            // 
            // _PictureBox
            // 
            this._PictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_PictureBox.BackgroundImage")));
            this._PictureBox.Location = new System.Drawing.Point(12, 12);
            this._PictureBox.Name = "_PictureBox";
            this._PictureBox.Size = new System.Drawing.Size(48, 48);
            this._PictureBox.TabIndex = 5;
            this._PictureBox.TabStop = false;
            // 
            // _VersionLabel
            // 
            this._VersionLabel.AutoSize = true;
            this._VersionLabel.Location = new System.Drawing.Point(92, 19);
            this._VersionLabel.Name = "_VersionLabel";
            this._VersionLabel.Size = new System.Drawing.Size(83, 13);
            this._VersionLabel.TabIndex = 6;
            this._VersionLabel.Text = "CDG Editor v{0}";
            // 
            // _BlurbLabel
            // 
            this._BlurbLabel.Location = new System.Drawing.Point(9, 63);
            this._BlurbLabel.Name = "_BlurbLabel";
            this._BlurbLabel.Size = new System.Drawing.Size(366, 58);
            this._BlurbLabel.TabIndex = 7;
            this._BlurbLabel.Text = "Copyright (C) 2009-2016 Darryl Baker\r\n\r\nThis project has been released under the " +
    "GNU General Public License (GPL)";
            this._BlurbLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _SourceForgeLinkLabel
            // 
            this._SourceForgeLinkLabel.AutoSize = true;
            this._SourceForgeLinkLabel.Location = new System.Drawing.Point(128, 132);
            this._SourceForgeLinkLabel.Name = "_SourceForgeLinkLabel";
            this._SourceForgeLinkLabel.Size = new System.Drawing.Size(128, 13);
            this._SourceForgeLinkLabel.TabIndex = 8;
            this._SourceForgeLinkLabel.TabStop = true;
            this._SourceForgeLinkLabel.Text = "cdgeditor.sourceforge.net";
            this._SourceForgeLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._SourceForgeLinkLabel_LinkClicked);
            // 
            // AboutForm
            // 
            this.AcceptButton = this._OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._OKButton;
            this.ClientSize = new System.Drawing.Size(387, 192);
            this.ControlBox = false;
            this.Controls.Add(this._SourceForgeLinkLabel);
            this.Controls.Add(this._BlurbLabel);
            this.Controls.Add(this._VersionLabel);
            this.Controls.Add(this._PictureBox);
            this.Controls.Add(this._OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AboutForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About CDG Editor";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _OKButton;
        private System.Windows.Forms.PictureBox _PictureBox;
        private System.Windows.Forms.Label _VersionLabel;
        private System.Windows.Forms.Label _BlurbLabel;
        private System.Windows.Forms.LinkLabel _SourceForgeLinkLabel;
    }
}