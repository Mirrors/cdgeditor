﻿using System;
using System.Windows.Forms;

namespace CDGEditor
{
    public partial class ScrollForm : Form
    {
        #region Construction

        public ScrollForm()
        {
            InitializeComponent();
            _PresetColour.ColourChanged += new EventHandler(_PresetColour_ColourChanged);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Scroll Chunk Command to Edit
        /// </summary>
        public Cdg.Chunks.ScrollChunk Chunk {get; set; }

        /// <summary>
        /// Sets the palette to display
        /// </summary>
        public System.Drawing.Imaging.ColorPalette Palette
        {
            set
            {
                _Palette = value;
                _PresetColour.Palette = value;
            }
        }
        
        #endregion

        #region Event Handlers

        private void ScrollForm_Load(object sender, EventArgs e)
        {
            if (Chunk is Cdg.Chunks.ScrollPreset)
            {
                Cdg.Chunks.ScrollPreset scrollPreset = (Cdg.Chunks.ScrollPreset)Chunk;
                _PresetColour.Index = scrollPreset.FillColor;
                Size = MaximumSize;
            }

            if (Chunk.HorizontalScrollInstruction.HasValue)
            {
                switch (Chunk.HorizontalScrollInstruction)
                {
                    case Cdg.Chunks.ScrollChunk.HScrollInstruction.None:
                    {
                        _HSNoneRadio.Checked = true;
                        break;
                    }
                    case Cdg.Chunks.ScrollChunk.HScrollInstruction.Left:
                    {
                        _HSLeftRadio.Checked = true;
                        break;
                    }
                    case Cdg.Chunks.ScrollChunk.HScrollInstruction.Right:
                    {
                        _HSRightRadio.Checked = true;
                        break;
                    }
                }
            }

            if (Chunk.VerticalScrollInstruction.HasValue)
            {
                switch (Chunk.VerticalScrollInstruction)
                {
                    case Cdg.Chunks.ScrollChunk.VScrollInstruction.None:
                    {
                        _VSNoneRadio.Checked = true;
                        break;
                    }
                    case Cdg.Chunks.ScrollChunk.VScrollInstruction.Up:
                    {
                        _VSUpRadio.Checked = true;
                        break;
                    }
                    case Cdg.Chunks.ScrollChunk.VScrollInstruction.Down:
                    {
                        _VSDownRadio.Checked = true;
                        break;
                    }
                }
            }

            if (Chunk.HorizontalScrollOffset.HasValue)
            {
                _XOffsetNumeric.Value = Math.Min(5, Math.Max(0, Chunk.HorizontalScrollOffset.Value));
            }
            else
            {
                _XOffsetNumeric.Controls[1].Text = "";
            }

            if (Chunk.VerticalScrollOffset.HasValue)
            {
                _YOffsetNumeric.Value = Math.Min(11, Math.Max(0, Chunk.VerticalScrollOffset.Value));
            }
            else
            {
                _YOffsetNumeric.Controls[1].Text = "";
            }
        }

        private void _CheckedChanged(object sender, EventArgs e)
        {
            if (_HSLeftRadio.Checked)
            {
                Chunk.HorizontalScrollInstruction = Cdg.Chunks.ScrollChunk.HScrollInstruction.Left;
            }
            else if (_HSRightRadio.Checked)
            {
                Chunk.HorizontalScrollInstruction = Cdg.Chunks.ScrollChunk.HScrollInstruction.Right;
            }
            else if (_HSNoneRadio.Checked)
            {
                Chunk.HorizontalScrollInstruction = Cdg.Chunks.ScrollChunk.HScrollInstruction.None;
            }

            if (_VSUpRadio.Checked)
            {
                Chunk.VerticalScrollInstruction = Cdg.Chunks.ScrollChunk.VScrollInstruction.Up;
            }
            else if (_VSDownRadio.Checked)
            {
                Chunk.VerticalScrollInstruction = Cdg.Chunks.ScrollChunk.VScrollInstruction.Down;
            }
            else if (_VSNoneRadio.Checked)
            {
                Chunk.VerticalScrollInstruction = Cdg.Chunks.ScrollChunk.VScrollInstruction.None;
            }
        }

        private void _XOffsetNumeric_ValueChanged(object sender, EventArgs e)
        {
            Chunk.HorizontalScrollOffset = (int)_XOffsetNumeric.Value;
        }

        private void _YOffsetNumeric_ValueChanged(object sender, EventArgs e)
        {
            Chunk.VerticalScrollOffset = (int)_YOffsetNumeric.Value;
        }

        void _PresetColour_ColourChanged(object sender, EventArgs e)
        {
            Cdg.Chunks.ScrollPreset scrollPreset = (Cdg.Chunks.ScrollPreset)Chunk;
            scrollPreset.FillColor = _PresetColour.Index;
        }

        #endregion

        #region Data

        /// <summary>
        /// The palette to use for the colour selection control.
        /// </summary>
        System.Drawing.Imaging.ColorPalette _Palette;

        #endregion
    }
}
