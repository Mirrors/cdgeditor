using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Cdg.Chunks;
using System.IO;

namespace CDGEditor
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		#region Controls

        private System.Windows.Forms.Panel _DisplayPanel;
        private Splitter splitter1;
        private Cdg.Controls.CdgDisplay _Display;
        private ContextMenuStrip _CommandListMenu;
        private ToolStripMenuItem _ContextEditMenu;
        private ToolStripMenuItem _ContextChangeCommandMenu;
        private MenuStrip _MainMenu;
        private ToolStripMenuItem _FileMenu;
        private ToolStripMenuItem _FileSaveAsMenu;
        private ToolStripSeparator _FileMenuSeparator;
        private ToolStripMenuItem _FileExitMenu2;
        private ToolStripMenuItem _EditMenu;
        private ToolStripMenuItem _EditCommandMenu;
        private ToolStripSeparator _EditMenuSeparator1;
        private ToolStripMenuItem _EditFindMenu;
        private ToolStripSeparator toolStripMenuItem3;
        private ToolStripMenuItem _EditChangeMenu;
        private ToolStripMenuItem _EditRecoverMenu;
        private ToolStripMenuItem _EditDeleteMenu;
        private ToolStripMenuItem _EditFindCommandMenu;
        private ToolStripSeparator _EditFindMenuSeparator;
        private ToolStripMenuItem _EditFindNextMenu;
        private ToolStripMenuItem _EditFindPreviousMenu;
        private ToolStripMenuItem _EditGoToMenu;
        private ToolStripMenuItem _ToolsMenu;
        private ToolStripMenuItem _ToolsPropertiesMenu;
        private ToolStripMenuItem _FileOpenMenu;
        private ToolStripMenuItem _FileSaveMenu;
        private ToolStripMenuItem _HelpMenu;
        private BrightIdeasSoftware.VirtualObjectListView _CommandList;
        private BrightIdeasSoftware.OLVColumn _TimeColumn;
        private BrightIdeasSoftware.OLVColumn _InstructionColumn;
        private BrightIdeasSoftware.OLVColumn _IndexColumn;
        private SplitContainer _SplitView;
        private ToolStripMenuItem findErrorsToolStripMenuItem;
        private Button _HideOutputButton;
        private BrightIdeasSoftware.ObjectListView _OutputList;
        private ToolStripMenuItem _ViewMenu;
        private ToolStripMenuItem _ViewTestColorTableMenu;
        private BrightIdeasSoftware.OLVColumn _OutputTimeColumn;
        private BrightIdeasSoftware.OLVColumn _OutputDescriptionColumn;
        private ContextMenuStrip _OutputListMenu;
        private ToolStripMenuItem _OutputGotoMenuItem;
        private ToolStripMenuItem _OutputListRevertMenuItem;
        private BrightIdeasSoftware.OLVColumn _StatusColumn;
        private ToolStripMenuItem _EditInsertTimeGaps;
        private ToolStripMenuItem _ContextDeleteCommands;
        private ToolStripMenuItem _ViewShowBorderTiles;
        private ToolStripMenuItem _EditCutMenuItem;
        private ToolStripMenuItem _EditCopyMenuItem;
        private ToolStripMenuItem _EditPasteMenuItem;
        private ToolStripSeparator toolStripMenuItem1;
        private ClipboardMonitor _ClipboardMonitor;
        private ToolStripMenuItem _HelpAboutMenu;

		#endregion

		#region Construction

        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            DataFormats.Format format = DataFormats.GetFormat(typeof(Chunk).FullName);
            _ClipboardFormat = format.Name;

            _CommandList.RowGetter = delegate(int index)
            {
                return _File.CdgChunks[index];
            };

            _SearchType = Cdg.Chunks.Chunk.InstructionType.MemoryPreset;

            _EditFindNextMenu.Text = string.Format("Find Next {0}",
                Cdg.Chunks.Chunk.GetTypeDescription(_SearchType));

            _EditFindPreviousMenu.Text = string.Format("Find Previous {0}",
                Cdg.Chunks.Chunk.GetTypeDescription(_SearchType));

            _IndexColumn.IsVisible = false;
            _CommandList.RebuildColumns();

            _Display.Clicked += new Cdg.Controls.CdgDisplay.DisplayClickedEventHandler(_Display_Clicked);
            _Display.DoubleClicked += new Cdg.Controls.CdgDisplay.DisplayClickedEventHandler(_Display_DoubleClicked);

            EnableMenus();
        }

        void _Display_DoubleClicked(int x, int y)
        {
            if (_File != null)
            {
                int pos = _File.FindPreviousTileAtLocation(x, y);
                if (pos > 0)
                {
                    Chunk chunk = _File.Chunks[pos];
                    _CommandList.SelectedIndex = _File.CdgChunks.IndexOf(chunk);
                    _CommandList.FocusedItem = _CommandList.SelectedItem;
                    _CommandList.SelectedItem.EnsureVisible();

                    _EditCommandMenu_Click(this, new EventArgs());
                }
            }
        }

        void _Display_Clicked(int x, int y)
        {
            if (_File != null)
            {
                int pos = _File.FindPreviousTileAtLocation(x, y);
                if (pos > 0)
                {
                    Chunk chunk = _File.Chunks[pos];
                    _CommandList.SelectedIndex = _File.CdgChunks.IndexOf(chunk);
                    _CommandList.FocusedItem = _CommandList.SelectedItem;
                    _CommandList.SelectedItem.EnsureVisible();
                }
            }
            //Text = string.Format("X:{0:00} Y:{1:00}", x, y);
        }

		#endregion

		#region Generated code

        private IContainer components;

        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this._DisplayPanel = new System.Windows.Forms.Panel();
            this._SplitView = new System.Windows.Forms.SplitContainer();
            this._OutputListMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._OutputGotoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._OutputListRevertMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._HideOutputButton = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this._CommandListMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._ContextEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._ContextChangeCommandMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._ContextDeleteCommands = new System.Windows.Forms.ToolStripMenuItem();
            this._MainMenu = new System.Windows.Forms.MenuStrip();
            this._FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._FileOpenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._FileSaveMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._FileSaveAsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._FileMenuSeparator = new System.Windows.Forms.ToolStripSeparator();
            this._FileExitMenu2 = new System.Windows.Forms.ToolStripMenuItem();
            this._EditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditCommandMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._EditFindMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditFindCommandMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditFindMenuSeparator = new System.Windows.Forms.ToolStripSeparator();
            this._EditFindNextMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditFindPreviousMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditGoToMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this._EditCutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._EditCopyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._EditPasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this._EditChangeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditRecoverMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditDeleteMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._EditInsertTimeGaps = new System.Windows.Forms.ToolStripMenuItem();
            this._ViewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._ViewTestColorTableMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._ViewShowBorderTiles = new System.Windows.Forms.ToolStripMenuItem();
            this._ToolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._ToolsPropertiesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.findErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._HelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._HelpAboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this._Display = new Cdg.Controls.CdgDisplay();
            this._OutputList = new BrightIdeasSoftware.ObjectListView();
            this._OutputTimeColumn = new BrightIdeasSoftware.OLVColumn();
            this._OutputDescriptionColumn = new BrightIdeasSoftware.OLVColumn();
            this._StatusColumn = new BrightIdeasSoftware.OLVColumn();
            this._CommandList = new BrightIdeasSoftware.VirtualObjectListView();
            this._TimeColumn = new BrightIdeasSoftware.OLVColumn();
            this._InstructionColumn = new BrightIdeasSoftware.OLVColumn();
            this._IndexColumn = new BrightIdeasSoftware.OLVColumn();
            this._ClipboardMonitor = new CDGEditor.ClipboardMonitor();
            this._DisplayPanel.SuspendLayout();
            this._SplitView.Panel1.SuspendLayout();
            this._SplitView.Panel2.SuspendLayout();
            this._SplitView.SuspendLayout();
            this._OutputListMenu.SuspendLayout();
            this._CommandListMenu.SuspendLayout();
            this._MainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._OutputList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._CommandList)).BeginInit();
            this.SuspendLayout();
            // 
            // _DisplayPanel
            // 
            this._DisplayPanel.Controls.Add(this._ClipboardMonitor);
            this._DisplayPanel.Controls.Add(this._SplitView);
            this._DisplayPanel.Controls.Add(this.splitter1);
            this._DisplayPanel.Controls.Add(this._CommandList);
            this._DisplayPanel.Controls.Add(this._MainMenu);
            this._DisplayPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._DisplayPanel.Location = new System.Drawing.Point(0, 0);
            this._DisplayPanel.Name = "_DisplayPanel";
            this._DisplayPanel.Size = new System.Drawing.Size(628, 320);
            this._DisplayPanel.TabIndex = 13;
            // 
            // _SplitView
            // 
            this._SplitView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SplitView.Location = new System.Drawing.Point(206, 24);
            this._SplitView.Name = "_SplitView";
            this._SplitView.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // _SplitView.Panel1
            // 
            this._SplitView.Panel1.Controls.Add(this._Display);
            this._SplitView.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // _SplitView.Panel2
            // 
            this._SplitView.Panel2.Controls.Add(this._OutputList);
            this._SplitView.Panel2.Controls.Add(this._HideOutputButton);
            this._SplitView.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SplitView.Size = new System.Drawing.Size(422, 296);
            this._SplitView.SplitterDistance = 159;
            this._SplitView.TabIndex = 6;
            // 
            // _OutputListMenu
            // 
            this._OutputListMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._OutputGotoMenuItem,
            this._OutputListRevertMenuItem});
            this._OutputListMenu.Name = "_OutputListMenu";
            this._OutputListMenu.Size = new System.Drawing.Size(108, 48);
            // 
            // _OutputGotoMenuItem
            // 
            this._OutputGotoMenuItem.Name = "_OutputGotoMenuItem";
            this._OutputGotoMenuItem.Size = new System.Drawing.Size(107, 22);
            this._OutputGotoMenuItem.Text = "Goto";
            this._OutputGotoMenuItem.Click += new System.EventHandler(this._OutputGotoMenuItem_Click);
            // 
            // _OutputListRevertMenuItem
            // 
            this._OutputListRevertMenuItem.Name = "_OutputListRevertMenuItem";
            this._OutputListRevertMenuItem.Size = new System.Drawing.Size(107, 22);
            this._OutputListRevertMenuItem.Text = "Revert";
            this._OutputListRevertMenuItem.Click += new System.EventHandler(this._OutputListRevertMenuItem_Click);
            // 
            // _HideOutputButton
            // 
            this._HideOutputButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._HideOutputButton.Image = global::CDGEditor.Properties.Resources.x1;
            this._HideOutputButton.Location = new System.Drawing.Point(402, 2);
            this._HideOutputButton.Name = "_HideOutputButton";
            this._HideOutputButton.Size = new System.Drawing.Size(17, 17);
            this._HideOutputButton.TabIndex = 0;
            this._HideOutputButton.UseVisualStyleBackColor = true;
            this._HideOutputButton.Click += new System.EventHandler(this._HideOutputButton_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(203, 24);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 296);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // _CommandListMenu
            // 
            this._CommandListMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ContextEditMenu,
            this._ContextChangeCommandMenu,
            this._ContextDeleteCommands});
            this._CommandListMenu.Name = "_CommandListMenu";
            this._CommandListMenu.Size = new System.Drawing.Size(255, 70);
            this._CommandListMenu.Opening += new System.ComponentModel.CancelEventHandler(this._CommandListMenu_Opening);
            // 
            // _ContextEditMenu
            // 
            this._ContextEditMenu.DoubleClickEnabled = true;
            this._ContextEditMenu.Name = "_ContextEditMenu";
            this._ContextEditMenu.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this._ContextEditMenu.Size = new System.Drawing.Size(254, 22);
            this._ContextEditMenu.Text = "Edit...";
            this._ContextEditMenu.Click += new System.EventHandler(this._EditCommandMenu_Click);
            // 
            // _ContextChangeCommandMenu
            // 
            this._ContextChangeCommandMenu.Name = "_ContextChangeCommandMenu";
            this._ContextChangeCommandMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this._ContextChangeCommandMenu.Size = new System.Drawing.Size(254, 22);
            this._ContextChangeCommandMenu.Text = "Change Command Type...";
            this._ContextChangeCommandMenu.Click += new System.EventHandler(this._EditChangeCommandTypeMenu_Click);
            // 
            // _ContextDeleteCommands
            // 
            this._ContextDeleteCommands.Name = "_ContextDeleteCommands";
            this._ContextDeleteCommands.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this._ContextDeleteCommands.Size = new System.Drawing.Size(254, 22);
            this._ContextDeleteCommands.Text = "Delete Command(s)";
            this._ContextDeleteCommands.Click += new System.EventHandler(this._EditDeleteCommandMenu_Click);
            // 
            // _MainMenu
            // 
            this._MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._FileMenu,
            this._EditMenu,
            this._ViewMenu,
            this._ToolsMenu,
            this._HelpMenu});
            this._MainMenu.Location = new System.Drawing.Point(0, 0);
            this._MainMenu.Name = "_MainMenu";
            this._MainMenu.Size = new System.Drawing.Size(628, 24);
            this._MainMenu.TabIndex = 5;
            this._MainMenu.Text = "Main Menu";
            // 
            // _FileMenu
            // 
            this._FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._FileOpenMenu,
            this._FileSaveMenu,
            this._FileSaveAsMenu,
            this._FileMenuSeparator,
            this._FileExitMenu2});
            this._FileMenu.Name = "_FileMenu";
            this._FileMenu.Size = new System.Drawing.Size(37, 20);
            this._FileMenu.Text = "&File";
            // 
            // _FileOpenMenu
            // 
            this._FileOpenMenu.Image = ((System.Drawing.Image)(resources.GetObject("_FileOpenMenu.Image")));
            this._FileOpenMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._FileOpenMenu.Name = "_FileOpenMenu";
            this._FileOpenMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._FileOpenMenu.Size = new System.Drawing.Size(146, 22);
            this._FileOpenMenu.Text = "&Open";
            this._FileOpenMenu.Click += new System.EventHandler(this._FileOpenMenu_Click);
            // 
            // _FileSaveMenu
            // 
            this._FileSaveMenu.Image = ((System.Drawing.Image)(resources.GetObject("_FileSaveMenu.Image")));
            this._FileSaveMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._FileSaveMenu.Name = "_FileSaveMenu";
            this._FileSaveMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._FileSaveMenu.Size = new System.Drawing.Size(146, 22);
            this._FileSaveMenu.Text = "&Save";
            this._FileSaveMenu.Click += new System.EventHandler(this._FileSaveMenu_Click);
            // 
            // _FileSaveAsMenu
            // 
            this._FileSaveAsMenu.Name = "_FileSaveAsMenu";
            this._FileSaveAsMenu.Size = new System.Drawing.Size(146, 22);
            this._FileSaveAsMenu.Text = "Save &As...";
            this._FileSaveAsMenu.Click += new System.EventHandler(this._FileSaveAsMenu_Click);
            // 
            // _FileMenuSeparator
            // 
            this._FileMenuSeparator.Name = "_FileMenuSeparator";
            this._FileMenuSeparator.Size = new System.Drawing.Size(143, 6);
            // 
            // _FileExitMenu2
            // 
            this._FileExitMenu2.Name = "_FileExitMenu2";
            this._FileExitMenu2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._FileExitMenu2.Size = new System.Drawing.Size(146, 22);
            this._FileExitMenu2.Text = "E&xit";
            this._FileExitMenu2.Click += new System.EventHandler(this._FileExitMenu_Click);
            // 
            // _EditMenu
            // 
            this._EditMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._EditCommandMenu,
            this._EditMenuSeparator1,
            this._EditFindMenu,
            this._EditGoToMenu,
            this.toolStripMenuItem3,
            this._EditCutMenuItem,
            this._EditCopyMenuItem,
            this._EditPasteMenuItem,
            this.toolStripMenuItem1,
            this._EditChangeMenu,
            this._EditRecoverMenu,
            this._EditDeleteMenu,
            this._EditInsertTimeGaps});
            this._EditMenu.Name = "_EditMenu";
            this._EditMenu.Size = new System.Drawing.Size(39, 20);
            this._EditMenu.Text = "&Edit";
            // 
            // _EditCommandMenu
            // 
            this._EditCommandMenu.Enabled = false;
            this._EditCommandMenu.Name = "_EditCommandMenu";
            this._EditCommandMenu.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this._EditCommandMenu.Size = new System.Drawing.Size(254, 22);
            this._EditCommandMenu.Text = "&Edit Command...";
            this._EditCommandMenu.Click += new System.EventHandler(this._EditCommandMenu_Click);
            // 
            // _EditMenuSeparator1
            // 
            this._EditMenuSeparator1.Name = "_EditMenuSeparator1";
            this._EditMenuSeparator1.Size = new System.Drawing.Size(251, 6);
            // 
            // _EditFindMenu
            // 
            this._EditFindMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._EditFindCommandMenu,
            this._EditFindMenuSeparator,
            this._EditFindNextMenu,
            this._EditFindPreviousMenu});
            this._EditFindMenu.Image = global::CDGEditor.Properties.Resources.BINOCULR;
            this._EditFindMenu.Name = "_EditFindMenu";
            this._EditFindMenu.Size = new System.Drawing.Size(254, 22);
            this._EditFindMenu.Text = "&Find";
            // 
            // _EditFindCommandMenu
            // 
            this._EditFindCommandMenu.Image = global::CDGEditor.Properties.Resources.BINOCULR;
            this._EditFindCommandMenu.Name = "_EditFindCommandMenu";
            this._EditFindCommandMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._EditFindCommandMenu.Size = new System.Drawing.Size(206, 22);
            this._EditFindCommandMenu.Text = "Find &Command...";
            this._EditFindCommandMenu.Click += new System.EventHandler(this._EditFindCommandMenu_Click);
            // 
            // _EditFindMenuSeparator
            // 
            this._EditFindMenuSeparator.Name = "_EditFindMenuSeparator";
            this._EditFindMenuSeparator.Size = new System.Drawing.Size(203, 6);
            // 
            // _EditFindNextMenu
            // 
            this._EditFindNextMenu.Name = "_EditFindNextMenu";
            this._EditFindNextMenu.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this._EditFindNextMenu.Size = new System.Drawing.Size(206, 22);
            this._EditFindNextMenu.Text = "Find &Next";
            this._EditFindNextMenu.Click += new System.EventHandler(this._EditFindNextMenu_Click);
            // 
            // _EditFindPreviousMenu
            // 
            this._EditFindPreviousMenu.Name = "_EditFindPreviousMenu";
            this._EditFindPreviousMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this._EditFindPreviousMenu.Size = new System.Drawing.Size(206, 22);
            this._EditFindPreviousMenu.Text = "Find &Previous";
            this._EditFindPreviousMenu.Click += new System.EventHandler(this._EditFindPreviousMenu_Click);
            // 
            // _EditGoToMenu
            // 
            this._EditGoToMenu.Name = "_EditGoToMenu";
            this._EditGoToMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this._EditGoToMenu.Size = new System.Drawing.Size(254, 22);
            this._EditGoToMenu.Text = "Go To...";
            this._EditGoToMenu.Click += new System.EventHandler(this._EditGoToMenu_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(251, 6);
            // 
            // _EditCutMenuItem
            // 
            this._EditCutMenuItem.Enabled = false;
            this._EditCutMenuItem.Name = "_EditCutMenuItem";
            this._EditCutMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._EditCutMenuItem.Size = new System.Drawing.Size(254, 22);
            this._EditCutMenuItem.Text = "Cu&t";
            this._EditCutMenuItem.Click += new System.EventHandler(this._EditCutMenuItem_Click);
            // 
            // _EditCopyMenuItem
            // 
            this._EditCopyMenuItem.Enabled = false;
            this._EditCopyMenuItem.Name = "_EditCopyMenuItem";
            this._EditCopyMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._EditCopyMenuItem.Size = new System.Drawing.Size(254, 22);
            this._EditCopyMenuItem.Text = "&Copy";
            this._EditCopyMenuItem.Click += new System.EventHandler(this._EditCopyMenuItem_Click);
            // 
            // _EditPasteMenuItem
            // 
            this._EditPasteMenuItem.Enabled = false;
            this._EditPasteMenuItem.Name = "_EditPasteMenuItem";
            this._EditPasteMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._EditPasteMenuItem.Size = new System.Drawing.Size(254, 22);
            this._EditPasteMenuItem.Text = "&Paste";
            this._EditPasteMenuItem.Click += new System.EventHandler(this._EditPasteMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(251, 6);
            // 
            // _EditChangeMenu
            // 
            this._EditChangeMenu.Name = "_EditChangeMenu";
            this._EditChangeMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this._EditChangeMenu.Size = new System.Drawing.Size(254, 22);
            this._EditChangeMenu.Text = "Change Command &Type...";
            this._EditChangeMenu.Click += new System.EventHandler(this._EditChangeCommandTypeMenu_Click);
            // 
            // _EditRecoverMenu
            // 
            this._EditRecoverMenu.Name = "_EditRecoverMenu";
            this._EditRecoverMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this._EditRecoverMenu.Size = new System.Drawing.Size(254, 22);
            this._EditRecoverMenu.Text = "&Recover Command...";
            this._EditRecoverMenu.Click += new System.EventHandler(this._EditRecoverCommandMenu_Click);
            // 
            // _EditDeleteMenu
            // 
            this._EditDeleteMenu.Name = "_EditDeleteMenu";
            this._EditDeleteMenu.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this._EditDeleteMenu.Size = new System.Drawing.Size(254, 22);
            this._EditDeleteMenu.Text = "&Delete Command(s)";
            this._EditDeleteMenu.Click += new System.EventHandler(this._EditDeleteCommandMenu_Click);
            // 
            // _EditInsertTimeGaps
            // 
            this._EditInsertTimeGaps.Name = "_EditInsertTimeGaps";
            this._EditInsertTimeGaps.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this._EditInsertTimeGaps.Size = new System.Drawing.Size(254, 22);
            this._EditInsertTimeGaps.Text = "Insert Time Gaps...";
            this._EditInsertTimeGaps.Click += new System.EventHandler(this._EditInsertTimeGaps_Click);
            // 
            // _ViewMenu
            // 
            this._ViewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ViewTestColorTableMenu,
            this._ViewShowBorderTiles});
            this._ViewMenu.Name = "_ViewMenu";
            this._ViewMenu.Size = new System.Drawing.Size(44, 20);
            this._ViewMenu.Text = "&View";
            // 
            // _ViewTestColorTableMenu
            // 
            this._ViewTestColorTableMenu.Name = "_ViewTestColorTableMenu";
            this._ViewTestColorTableMenu.Size = new System.Drawing.Size(214, 22);
            this._ViewTestColorTableMenu.Text = "Use Test &Color Table";
            this._ViewTestColorTableMenu.Click += new System.EventHandler(this._ViewTestColorTableMenu_Click);
            // 
            // _ViewShowBorderTiles
            // 
            this._ViewShowBorderTiles.Name = "_ViewShowBorderTiles";
            this._ViewShowBorderTiles.Size = new System.Drawing.Size(214, 22);
            this._ViewShowBorderTiles.Text = "Show Border Tile Contents";
            this._ViewShowBorderTiles.Click += new System.EventHandler(this._ViewShowBorderTiles_Click);
            // 
            // _ToolsMenu
            // 
            this._ToolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ToolsPropertiesMenu,
            this.findErrorsToolStripMenuItem});
            this._ToolsMenu.Name = "_ToolsMenu";
            this._ToolsMenu.Size = new System.Drawing.Size(48, 20);
            this._ToolsMenu.Text = "Tools";
            // 
            // _ToolsPropertiesMenu
            // 
            this._ToolsPropertiesMenu.Name = "_ToolsPropertiesMenu";
            this._ToolsPropertiesMenu.Size = new System.Drawing.Size(136, 22);
            this._ToolsPropertiesMenu.Text = "Properties...";
            this._ToolsPropertiesMenu.Click += new System.EventHandler(this._ToolsPropertiesMenu_Click);
            // 
            // findErrorsToolStripMenuItem
            // 
            this.findErrorsToolStripMenuItem.Name = "findErrorsToolStripMenuItem";
            this.findErrorsToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.findErrorsToolStripMenuItem.Text = "&Validate";
            this.findErrorsToolStripMenuItem.Click += new System.EventHandler(this.findErrorsToolStripMenuItem_Click);
            // 
            // _HelpMenu
            // 
            this._HelpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._HelpAboutMenu});
            this._HelpMenu.Name = "_HelpMenu";
            this._HelpMenu.Size = new System.Drawing.Size(44, 20);
            this._HelpMenu.Text = "He&lp";
            // 
            // _HelpAboutMenu
            // 
            this._HelpAboutMenu.Name = "_HelpAboutMenu";
            this._HelpAboutMenu.Size = new System.Drawing.Size(116, 22);
            this._HelpAboutMenu.Text = "&About...";
            this._HelpAboutMenu.Click += new System.EventHandler(this._HelpAboutMenu_Click);
            // 
            // _Display
            // 
            this._Display.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Display.HorizontalScrollOffset = 0;
            this._Display.Location = new System.Drawing.Point(0, 0);
            this._Display.Name = "_Display";
            this._Display.Size = new System.Drawing.Size(422, 159);
            this._Display.TabIndex = 4;
            this._Display.TileLocation = new System.Drawing.Point(0, 0);
            this._Display.UseTestColourTable = false;
            this._Display.VerticalScrollOffset = 0;
            this._Display.VisibleBorderTiles = false;
            // 
            // _OutputList
            // 
            this._OutputList.AllColumns.Add(this._OutputTimeColumn);
            this._OutputList.AllColumns.Add(this._OutputDescriptionColumn);
            this._OutputList.AllColumns.Add(this._StatusColumn);
            this._OutputList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this._OutputList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._OutputTimeColumn,
            this._OutputDescriptionColumn,
            this._StatusColumn});
            this._OutputList.ContextMenuStrip = this._OutputListMenu;
            this._OutputList.FullRowSelect = true;
            this._OutputList.GridLines = true;
            this._OutputList.HideSelection = false;
            this._OutputList.Location = new System.Drawing.Point(3, 3);
            this._OutputList.Name = "_OutputList";
            this._OutputList.ShowGroups = false;
            this._OutputList.Size = new System.Drawing.Size(393, 127);
            this._OutputList.TabIndex = 1;
            this._OutputList.UseCompatibleStateImageBehavior = false;
            this._OutputList.View = System.Windows.Forms.View.Details;
            this._OutputList.DoubleClick += new System.EventHandler(this._OutputGotoMenuItem_Click);
            // 
            // _OutputTimeColumn
            // 
            this._OutputTimeColumn.AspectName = "Time";
            this._OutputTimeColumn.Text = "Time";
            this._OutputTimeColumn.Width = 68;
            // 
            // _OutputDescriptionColumn
            // 
            this._OutputDescriptionColumn.AspectName = "Description";
            this._OutputDescriptionColumn.FillsFreeSpace = true;
            this._OutputDescriptionColumn.Text = "Description";
            // 
            // _StatusColumn
            // 
            this._StatusColumn.AspectName = "Status";
            this._StatusColumn.Text = "Status";
            // 
            // _CommandList
            // 
            this._CommandList.AllColumns.Add(this._TimeColumn);
            this._CommandList.AllColumns.Add(this._InstructionColumn);
            this._CommandList.AllColumns.Add(this._IndexColumn);
            this._CommandList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._TimeColumn,
            this._InstructionColumn,
            this._IndexColumn});
            this._CommandList.ContextMenuStrip = this._CommandListMenu;
            this._CommandList.Dock = System.Windows.Forms.DockStyle.Left;
            this._CommandList.FullRowSelect = true;
            this._CommandList.GridLines = true;
            this._CommandList.HideSelection = false;
            this._CommandList.Location = new System.Drawing.Point(0, 24);
            this._CommandList.Name = "_CommandList";
            this._CommandList.ShowGroups = false;
            this._CommandList.Size = new System.Drawing.Size(203, 296);
            this._CommandList.TabIndex = 0;
            this._CommandList.UseCompatibleStateImageBehavior = false;
            this._CommandList.View = System.Windows.Forms.View.Details;
            this._CommandList.VirtualMode = true;
            this._CommandList.SelectionChanged += new System.EventHandler(this._CommandList_SelectedIndexChanged);
            this._CommandList.VirtualItemsSelectionRangeChanged += new System.Windows.Forms.ListViewVirtualItemsSelectionRangeChangedEventHandler(this._CommandList_VirtualItemsSelectionRangeChanged);
            this._CommandList.DoubleClick += new System.EventHandler(this._EditCommandMenu_Click);
            // 
            // _TimeColumn
            // 
            this._TimeColumn.AspectName = "Time";
            this._TimeColumn.Text = "Time";
            // 
            // _InstructionColumn
            // 
            this._InstructionColumn.AspectName = "Instruction";
            this._InstructionColumn.FillsFreeSpace = true;
            this._InstructionColumn.Text = "Instruction";
            // 
            // _IndexColumn
            // 
            this._IndexColumn.AspectName = "Index";
            this._IndexColumn.Text = "Index";
            // 
            // _ClipboardMonitor
            // 
            this._ClipboardMonitor.BackColor = System.Drawing.Color.Red;
            this._ClipboardMonitor.Location = new System.Drawing.Point(599, 3);
            this._ClipboardMonitor.Name = "_ClipboardMonitor";
            this._ClipboardMonitor.Size = new System.Drawing.Size(26, 25);
            this._ClipboardMonitor.TabIndex = 5;
            this._ClipboardMonitor.Visible = false;
            this._ClipboardMonitor.ClipboardChanged += new System.EventHandler<CDGEditor.ClipboardChangedEventArgs>(this._ClipboardMonitor_ClipboardChanged);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(628, 320);
            this.Controls.Add(this._DisplayPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MainMenu;
            this.Name = "MainForm";
            this.Text = "Darryl\'s CDG Editor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this._DisplayPanel.ResumeLayout(false);
            this._DisplayPanel.PerformLayout();
            this._SplitView.Panel1.ResumeLayout(false);
            this._SplitView.Panel2.ResumeLayout(false);
            this._SplitView.ResumeLayout(false);
            this._OutputListMenu.ResumeLayout(false);
            this._CommandListMenu.ResumeLayout(false);
            this._MainMenu.ResumeLayout(false);
            this._MainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._OutputList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._CommandList)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		#endregion

        #region Properties

        /// <summary>
        /// Arguments for single instance application
        /// </summary>
        public string[] Args
        {
            get
            {
                return _Args;
            }
            set
            {
                _Args = value;
            }
        }

        /// <summary>
        /// Whether the file being edited has unsaved changes
        /// </summary>
        bool FileChanged
        {
            get
            {
                return _FileChanged;
            }
            set
            {
                _FileChanged = value;
                if (_File == null)
                {
                    Text = "Darryl's CDG Editor";
                }
                else
                {
                    Text = string.Format("Darryl's CDG Editor - {0}{1}",
                        _File.Info.Name, _FileChanged ? "*" : "");
                }
            }
        }

        #region Persistence

        [Persist]
        new public Point Location
        {
            get
            {
                return base.Location;
            }
            set
            {
                base.Location = value;
            }
        }

        [Persist]
        new public Size Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
            }
        }

        [Persist]
        public int SplitLocation
        {
            get
            {
                return _SplitView.SplitterDistance;
            }
            set
            {
                _SplitView.SplitterDistance = value;
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        #region Menus

        #region Menu Enabling

        private void _CommandListMenu_Opening(object sender, CancelEventArgs e)
        {
            EnableMenus();
        }

        void EnableMenus()
        {
            bool enable = _File != null;

            _FileSaveMenu.Enabled = enable;
            _FileSaveAsMenu.Enabled = enable;
            _EditFindNextMenu.Enabled = enable;
            _EditFindPreviousMenu.Enabled = enable;
            _EditRecoverMenu.Enabled = enable;
            _EditFindMenu.Enabled = enable;
            _EditGoToMenu.Enabled = enable;
            _EditFindCommandMenu.Enabled = enable;
            _EditFindNextMenu.Enabled = enable;
            _EditFindPreviousMenu.Enabled = enable;
            _ToolsPropertiesMenu.Enabled = enable;

            if (enable)
            {
                enable = _CommandList.SelectedObject != null;
            }

            _EditChangeMenu.Enabled = enable;
            _ContextEditMenu.Enabled = enable;
            _ContextChangeCommandMenu.Enabled = enable;

            // Allow multiple selection for delete/copy/cut
            enable = _CommandList.SelectedObjects.Count > 0;

            _EditCommandMenu.Enabled = enable;  // Version 1.5: Allow editing of multiple commands. 
            _EditDeleteMenu.Enabled = enable;
            _ContextDeleteCommands.Enabled = enable;
            _EditCopyMenuItem.Enabled = enable;
            _EditCutMenuItem.Enabled = enable;
        }

        #endregion

        #region File

        private void _FileOpenMenu_Click(object sender, System.EventArgs e)
		{
            if (PromptForSave())
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "CDG Files (*.cdg)|*.cdg";
                dialog.Title = "Open File";
                dialog.CheckFileExists = true;
                dialog.CheckPathExists = true;

                if (DialogResult.OK == dialog.ShowDialog(this))
                {
                    OpenFile(dialog.FileName);
                }
            }
        }

        private void _FileSaveMenu_Click(object sender, EventArgs e)
        {
            try
            {
                _File.Save();
                FileChanged = false;
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void _FileSaveAsMenu_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "CDG Files (*.cdg)|*.cdg";
            dialog.AddExtension = true;
            dialog.FileName = _File.Info.FullName;

            if (DialogResult.OK == dialog.ShowDialog(this))
            {
                try
                {
                    _File.Save(dialog.FileName);
                    OpenFile(dialog.FileName);
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
            }
        }

        private void _FileExitMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region Edit

        private void _EditCommandMenu_Click(object sender, EventArgs e)
        {
            if (_CommandList.SelectedObjects.Count == 0) return;

            // Allow editing of multiple items find common values
            Chunk commonChunk = FindCommonSelectedValues();
            if (commonChunk != null)
            {
                Form editForm = CreateEditForm(commonChunk);

                if (editForm != null)
                {
                    if (!(editForm is TileForm ) && DialogResult.OK == editForm.ShowDialog())
                    {
                        // Colour Selection form doesn't work on a chunk directly
                        if (editForm is ColourSelectionForm)
                        {
                            ColourSelectionForm colourForm = (ColourSelectionForm)editForm;
                            switch (commonChunk.Type)
                            {
                                case Chunk.InstructionType.BorderPreset:
                                {
                                    (commonChunk as BorderPreset).Colour = colourForm.ColourIndex;
                                    break;
                                }
                                case Chunk.InstructionType.MemoryPreset:
                                {
                                    (commonChunk as MemoryPreset).ColourIndex = colourForm.ColourIndex;
                                    break;
                                }
                                case Chunk.InstructionType.LoadColTableHigh:
                                case Chunk.InstructionType.LoadColTableLow:
                                {
                                    (commonChunk as LoadColourTable).SetColours(colourForm.Palette);
                                    break;
                                }
                            }
                        }
                    }

                    foreach (Chunk chunk in _CommandList.SelectedObjects)
                    {
                        ApplyValues(chunk, commonChunk);
                    }

                    FileChanged = true;
                    RefreshCDG(false);
                }
            }
        }
        
        /// <summary>
        /// Handles event raised when a change is made to a Tile chunk in the modeless
        /// Tile Block edit form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TileForm_Changed(object sender, EventArgs e)
        {
            // Apply common changes to all selected tile block chunks backing up the original data in case the
            // user cancels the edit form.
            TileForm form = (TileForm)sender;
            TileBlock commonTile = form.CommonChunk;

            foreach (TileBlock tileBlock in form.Chunks)
            {
                TileBlock editTile = (TileBlock)_File.Chunks[tileBlock.Index.Value];
                if (commonTile.OffColour.HasValue)
                {
                    editTile.OffColour = commonTile.OffColour;
                }
                if (commonTile.OnColour.HasValue)
                {
                    editTile.OnColour = commonTile.OnColour;
                }
                if (commonTile.Row.HasValue)
                {
                    editTile.Row = commonTile.Row;
                }
                if (commonTile.Column.HasValue)
                {
                    editTile.Column = commonTile.Column;
                }
                if (commonTile.TileType.HasValue)
                {
                    editTile.TileType = commonTile.TileType;
                }
                if (!commonTile.NullPixels)
                {
                    editTile.SetPixels(commonTile);
                }
            }

            FileChanged = true;
            RefreshCDG(false);
        }

        void TileForm_Canceled(object sender, EventArgs e)
        {
            TileForm form = (TileForm)sender;
            foreach (TileBlock tileBlock in form.Chunks)
            {
                _File.Chunks[tileBlock.Index.Value] = tileBlock;
            }

            FileChanged = true;
            RefreshCDG(false);
        }


        private void _EditFindCommandMenu_Click(object sender, EventArgs e)
        {
            InstuctionTypeForm form = new InstuctionTypeForm();
            form.Type = _SearchType;
            form.Text = "Find Command";

            if (DialogResult.OK == form.ShowDialog(this))
            {
                _SearchType = form.Type;
                _EditFindNextMenu_Click(this, new EventArgs());

                _EditFindNextMenu.Text = string.Format("Find Next {0}",
                    Cdg.Chunks.Chunk.GetTypeDescription(_SearchType));
                
                _EditFindPreviousMenu.Text = string.Format("Find Previous {0}",
                    Cdg.Chunks.Chunk.GetTypeDescription(_SearchType));
            }
        }

        private void _EditFindNextMenu_Click(object sender, EventArgs e)
        {
            Cdg.Chunks.Chunk chunk;

            // Move to next command
            int nextCommandIndex = _CommandList.SelectedIndex + 1;

            if (_SearchType == Cdg.Chunks.Chunk.InstructionType.MemoryPreset)
            {
                // Memory preset search navigates to the instruction before...
                nextCommandIndex++;
                if (nextCommandIndex < _File.CdgChunks.Count)
                {
                    _File.GotoChunk(_File.CdgChunks[nextCommandIndex].Index.Value, _Display.Bitmap);
                }
            }

            int chunkIndex = _File.FindNext(_SearchType);
            if (chunkIndex > 0)
            {
                chunk = _File.Chunks[chunkIndex];
                int newSelection = _File.CdgChunks.IndexOf(chunk);

                if (_SearchType == Cdg.Chunks.Chunk.InstructionType.MemoryPreset)
                {
                    // More useful to see the page when searching for Memory Presets
                    newSelection--;
                }

                _CommandList.SelectedIndex = newSelection;
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                if (_CommandList.SelectedItem != null)
                {
                    _CommandList.SelectedItem.EnsureVisible();
                }
            }
            else
            {
                _CommandList.SelectedIndex = _CommandList.Items.Count - 1;
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                _CommandList.SelectedItem.EnsureVisible();
            }
        }

        private void _EditFindPreviousMenu_Click(object sender, EventArgs e)
        {
            Cdg.Chunks.Chunk chunk;

            // If the current command is a the same type, move back 1
            if (_CommandList.SelectedIndex > 1)
            {
                chunk = _File.CdgChunks[_CommandList.SelectedIndex];
                if (chunk.Type == _SearchType)
                {
                    _CommandList.SelectedIndex -= 1;
                    _CommandList.FocusedItem = _CommandList.SelectedItem;
                    _CommandList.SelectedItem.EnsureVisible();
                }
            }
            if (_CommandList.SelectedIndex > 0)
            {
                int chunkIndex = _File.FindPrevious(_SearchType, _File.CdgChunks[_CommandList.SelectedIndex].Index.Value);
                chunk = _File.Chunks[chunkIndex];
                _CommandList.SelectedIndex = _File.CdgChunks.IndexOf(chunk);
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                if (_CommandList.SelectedItem != null)
                {
                    _CommandList.SelectedItem.EnsureVisible();
                }
            }
            else
            {
                _CommandList.SelectedIndex = 0;
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                _CommandList.SelectedItem.EnsureVisible();
            }
        }

        private void _EditChangeCommandTypeMenu_Click(object sender, EventArgs e)
        {
            InstuctionTypeForm form = new InstuctionTypeForm();
            form.Type = _File.CdgChunks[_CommandList.SelectedIndex].Type;
            if (DialogResult.OK == form.ShowDialog(this))
            {
                _File.ChangeChunkType(_CommandList.SelectedIndex, form.Type);

                FileChanged = true;
                RefreshCDG(true);
            }
        }

        private void _EditRecoverCommandMenu_Click(object sender, EventArgs e)
        {
            RecoverForm form = new RecoverForm(_File, _CommandList.SelectedIndex);
            if (DialogResult.OK == form.ShowDialog(this))
            {
                foreach (Chunk chunk in form.SelectedChunks)
                {
                    _File.RecoverChunk(chunk, form.ChunkType);
                }
                RefreshCDG(true);
                FileChanged = true;
            }
        }

        private void _EditDeleteCommandMenu_Click(object sender, EventArgs e)
        {
            Chunk firstChunk = (Chunk)_CommandList.SelectedObjects[0];

            if (DialogResult.Yes == MessageBox.Show(this,
                string.Format("Delete {1} Chunk{2} at {0}\nAre you sure?",
                firstChunk.Time, _CommandList.SelectedObjects.Count,
                (_CommandList.SelectedObjects.Count > 1 ? "s" : "")),
                "Delete Chunk", MessageBoxButtons.YesNo))
            {
                DeleteSelection();
            }
        }

        private void DeleteSelection()
        {
            int lastSelectedIndex = _CommandList.SelectedIndices[_CommandList.SelectedIndices.Count - 1];
            Chunk nextChunk = null;
            if (lastSelectedIndex < _File.CdgChunks.Count - 1)
            {
                nextChunk = _File.CdgChunks[lastSelectedIndex + 1];
            }

            foreach (Chunk chunk in _CommandList.SelectedObjects)
            {
                _File.DeleteChunk(chunk.Index.Value);
            }

            RefreshCDG(true);

            if (nextChunk != null)
            {
                _CommandList.SelectedIndex = _File.CdgChunks.IndexOf(nextChunk);
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                _CommandList.SelectedItem.EnsureVisible();

                Chunk selectedChunk = (Chunk)_CommandList.SelectedObject;
                _File.GotoChunk(selectedChunk.Index.Value, _Display.Bitmap, true);
            }
            else
            {
                _CommandList.SelectedIndex = -1;
            }

            FileChanged = true;
        }

        private void _EditGoToMenu_Click(object sender, EventArgs e)
        {
            GotoForm form = new GotoForm(string.Format("Time ({0}-{1})",
                _File.CdgChunks[0].Time, _File.CdgChunks[_File.CdgChunks.Count - 1].Time));
            if (DialogResult.OK == form.ShowDialog(this))
            {
                if (form.ChunkIndex >= 0 && form.ChunkIndex < _File.Chunks.Count)
                {
                    Cdg.Chunks.Chunk chunk = _File.Chunks[form.ChunkIndex];
                    _CommandList.SelectedIndex = _File.CdgChunks.IndexOf(chunk);
                    _CommandList.FocusedItem = _CommandList.SelectedItem;
                    _CommandList.SelectedItem.EnsureVisible();
                }
            }
        }

        private void _EditInsertTimeGaps_Click(object sender, EventArgs e)
        {
            InsertForm form = new InsertForm();
            if (DialogResult.OK == form.ShowDialog())
            {
                int insertIndex = 0;
                Chunk selectedChunk = (Chunk)_CommandList.SelectedObject;
                if (selectedChunk != null)
                {
                    insertIndex = selectedChunk.Index.Value;
                }

                _File.InsertChunks(insertIndex, form.ChunkCount);
                RefreshCDG(true);
            }
        }

        #region Cut/Copy/Paste

        private void PutSelectionInClipboard()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(_CommandList.SelectedObjects.Count.ToString());
            foreach (Chunk chunk in _CommandList.SelectedObjects)
            {
                builder.AppendLine(chunk.DataString);
            }

            DataObject dataObject = new DataObject(_ClipboardFormat, builder.ToString());
            Clipboard.SetDataObject(dataObject, true);
        }

        private void _EditCutMenuItem_Click(object sender, EventArgs e)
        {
            PutSelectionInClipboard();
            DeleteSelection();
        }

        private void _EditCopyMenuItem_Click(object sender, EventArgs e)
        {
            PutSelectionInClipboard();
        }

        private void _EditPasteMenuItem_Click(object sender, EventArgs e)
        {
            //TODO: What if paste range overwrites existing commands?
            // For first implementation - just overwrite whatever is there.

            IDataObject dataObject = Clipboard.GetDataObject();
            string data = (string)dataObject.GetData(_ClipboardFormat);

            int pasteIndex = 0;
            if (_CommandList.SelectedObjects.Count > 0)
            {
                if (_CommandList.SelectedIndices[0] > 0)
                {
                    // Find the index (in the command list view) of the first selected item
                    pasteIndex = _CommandList.SelectedIndices[0];
                    if (pasteIndex > 0)
                    {
                        Chunk previousChunk = _File.CdgChunks[pasteIndex - 1];
                        // Paste after that chunk.
                        pasteIndex = previousChunk.Index.Value + 1;
                    }
                    else
                    {
                        // Paste at the start
                        pasteIndex = 0;
                    }
                }
            }

            byte[] byteArray = Encoding.ASCII.GetBytes(data);
            MemoryStream stream = new MemoryStream(byteArray);
            StreamReader reader = new StreamReader(stream);
            int numItems = int.Parse(reader.ReadLine());

            for (int index = 0; index < numItems; index++ )
            {
                string chunkDataString = reader.ReadLine();
                _File.Chunks[pasteIndex] = Chunk.FromDataString(chunkDataString);
                _File.Chunks[pasteIndex].Index = pasteIndex;
                pasteIndex++;
            }

            _File.RebuildCdgChunkList();
            RefreshCDG(true);
        }
        
        private void _ClipboardMonitor_ClipboardChanged(object sender, ClipboardChangedEventArgs e)
        {
            _EditPasteMenuItem.Enabled = Clipboard.ContainsData(_ClipboardFormat);
        }

        #endregion

        #endregion

        #region Tools

        private void _ToolsPropertiesMenu_Click(object sender, EventArgs e)
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.Append(_File.Info.FullName);
            builder.Append(":\n");

            builder.Append(string.Format("Raw chunks: {0}\n", _File.Chunks.Count));
            builder.Append(string.Format("CDG chunks: {0}\n", _File.CdgChunks.Count));

            MessageBox.Show(builder.ToString());
        }

        #endregion

        #region TODO

        private void findErrorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileChanged = _File.Validate();
            if (FileChanged)
            {
                RefreshCDG(true);
            }

            _SplitView.Panel2Collapsed = false;
        }

        private void _HideOutputButton_Click(object sender, EventArgs e)
        {
            _SplitView.Panel2Collapsed = true;
        }

        #endregion

        #region View

        private void _ViewTestColorTableMenu_Click(object sender, EventArgs e)
        {
            _ViewTestColorTableMenu.Checked = !_ViewTestColorTableMenu.Checked;
            _Display.UseTestColourTable = _ViewTestColorTableMenu.Checked;
        }

        private void _ViewShowBorderTiles_Click(object sender, EventArgs e)
        {
            _ViewShowBorderTiles.Checked = !_ViewShowBorderTiles.Checked;
            _Display.VisibleBorderTiles = _ViewShowBorderTiles.Checked;
        }

        #endregion

        #region Output List context menu

        private void _OutputGotoMenuItem_Click(object sender, EventArgs e)
        {
            Cdg.Validation.Result error = (Cdg.Validation.Result)_OutputList.SelectedObject;
            if (error != null && error is Cdg.Validation.ChunkResult)
            {
                Cdg.Chunks.Chunk chunk = (error as Cdg.Validation.ChunkResult).Chunk;
                _CommandList.SelectedIndex = _File.CdgChunks.IndexOf(chunk);
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                _CommandList.FocusedItem.EnsureVisible();
            }
        }
        
        private void _OutputListRevertMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Cdg.Validation.Result result in _OutputList.SelectedObjects)
            {
                if (result is Cdg.Validation.ChunkRepair)
                {
                    (result as Cdg.Validation.ChunkRepair).Revert();
                }
            }

            RefreshCDG(false);
            FileChanged = true;
        }
        #endregion

        #region Help

        private void _HelpAboutMenu_Click(object sender, EventArgs e)
        {
            AboutForm form = new AboutForm();
            form.ShowDialog(this);
        }

        #endregion

        #endregion

        #region Command List

        private void _CommandList_VirtualItemsSelectionRangeChanged(object sender, ListViewVirtualItemsSelectionRangeChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                Cdg.Chunks.Chunk chunk = _File.CdgChunks[e.EndIndex + 1];

                if (chunk != null)
                {
                    _File.GotoChunk(chunk.Index.Value, _Display.Bitmap);
                    _FilePostion = chunk.Index.Value;

                }

                EnableMenus();

                if (chunk is TileBlock)
                {
                    TileBlock tile = chunk as TileBlock;
                    _Display.TileLocation = new Point(tile.Column.Value, tile.Row.Value);
                }
                else
                {
                    _Display.TileLocation = Point.Empty;
                }
            }
        }


        private void _CommandList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Execute instructions up to and including the one clicked
            Cdg.Chunks.Chunk chunk = _CommandList.SelectedObject as Cdg.Chunks.Chunk;

            if (chunk != null)
            {
                _File.GotoChunk(chunk.Index.Value, _Display.Bitmap);
                _FilePostion = chunk.Index.Value;
            }

            EnableMenus();

            if (chunk is TileBlock)
            {
                TileBlock tile = chunk as TileBlock;
                _Display.TileLocation = new Point(tile.Column.Value, tile.Row.Value);
            }
            else
            {
                _Display.TileLocation = Point.Empty;
            }
        }

        #endregion

        #region Form

        private void MainForm_Load(object sender, EventArgs e)
        {
            Persist.Load(this);

            Focus();

            // The single-instance code is going to save the command line 
            // arguments in this member variable before opening the first instance
            // of the app.
            if (this.Args != null && this.Args.Length > 0)
            {
                ProcessParameters(null, this.Args);
                this.Args = null;
            }
            else
            {
                NewFile();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!PromptForSave())
            {
                e.Cancel = true;
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Persist.Save(this);
        }

        #endregion

        #endregion

        #region Private Methods

        void NewFile()
        {
            _File = new Cdg.CdgFile();
            _CommandList.VirtualListSize = _File.CdgChunks.Count;

            EnableMenus();
        }

        void OpenFile(string path)
        {
            try
            {
                _File = new Cdg.CdgFile();
                FileChanged = _File.Load(path);
                _CommandList.VirtualListSize = _File.CdgChunks.Count;

                _CommandList.SelectedIndex = 0;
                _CommandList.FocusedItem = _CommandList.SelectedItem;
                if (_CommandList.SelectedItem != null)
                {
                    _CommandList.SelectedItem.EnsureVisible();
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(this, x.Message);
            }

            RefreshCDG(true);

            EnableMenus();
        }

        void RefreshCDG(bool refreshList)
        {
            if (refreshList)
            {
                _CommandList.VirtualListSize = _File.CdgChunks.Count;
                _CommandList.Refresh();
            }

            Chunk selectedChunk = _File.Chunks[_FilePostion];
            if (selectedChunk != null)
            {
                _File.GotoChunk(selectedChunk.Index.Value, _Display.Bitmap);

                if (selectedChunk is TileBlock)
                {
                    TileBlock tile = selectedChunk as TileBlock;
                    _Display.TileLocation = new Point(tile.Column.Value, tile.Row.Value);
                }
                else
                {
                    _Display.TileLocation = Point.Empty;
                }
            }

            _OutputList.Objects = _File.Errors;
        }

        /// <summary>
        /// Prompt to save changes.  If yes is chosen, file is saved.
        /// </summary>
        /// <returns>True if user chooses yes or no, false if user cancels.</returns>
        bool PromptForSave()
        {
            bool result = true;

            if (FileChanged)
            {
                DialogResult dialogResult = MessageBox.Show("The file has changes that have not been saved.\nSave changes?", "Darryl's CDG Editor", MessageBoxButtons.YesNoCancel);

                if (dialogResult == DialogResult.Yes)
                {
                    _File.Save();
                    FileChanged = false;
                }

                if (dialogResult == DialogResult.Cancel)
                {
                    result = false;
                }
            }
            return result;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Step 1 of multiple chunk editing - Create the appropriate edit dialog
        /// </summary>
        /// <param name="commonChunk"></param>
        /// <returns></returns>
        private Form CreateEditForm(Chunk commonChunk)
        {
            Form result = null;

            switch (commonChunk.Type)
            {
                default:
                {
                    MessageBox.Show(string.Format("Editing of the {0} command is not supported",
                       commonChunk.Instruction));
                    break;
                }

                case Chunk.InstructionType.ScrollPreset:
                case Chunk.InstructionType.ScrollCopy:
                {
                    ScrollForm form = new ScrollForm();
                    form.Chunk = (ScrollChunk)commonChunk;
                    form.Palette = _Display.Bitmap.Palette;
                    result = form;
                    break;
                }

                case Chunk.InstructionType.BorderPreset:
                {

                    BorderPreset borderChunk = commonChunk as BorderPreset;
                    result = new ColourSelectionForm(_Display.Bitmap.Palette, borderChunk.Colour);
                    break;
                }

                case Chunk.InstructionType.DefTransColor:
                {
                    DefineTransparentColour colourChunk = commonChunk as DefineTransparentColour;
                    result = new ColourSelectionForm(_Display.Bitmap.Palette, colourChunk.ColourIndex);
                    break;
                }

                case Chunk.InstructionType.MemoryPreset:
                {
                    MemoryPreset presetChunk = commonChunk as MemoryPreset;
                    result = new ColourSelectionForm(
                        _Display.Bitmap.Palette, presetChunk.ColourIndex);
                    ((ColourSelectionForm)result).RepeatValue = presetChunk.Repeat;
                    break;
                }

                case Chunk.InstructionType.LoadColTableLow:
                case Chunk.InstructionType.LoadColTableHigh:
                {
                    LoadColourTable loadChunk = commonChunk as LoadColourTable;

                    System.Drawing.Imaging.ColorPalette palette = _Display.Bitmap.Palette;
                    ColourSelectionForm form = new ColourSelectionForm(
                        palette, 0);
                    form.EditColours = true;
                    form.HalfSize = true;
                    form.ShowFirstHalf = loadChunk.Type == Chunk.InstructionType.LoadColTableLow;
                    form.Text = string.Format("Edit {0}", commonChunk.Instruction);
                    result = form;
                    break;
                }

                case Chunk.InstructionType.TileNormal:
                case Chunk.InstructionType.TileXor:
                {
                    TileBlock tileChunk = commonChunk as TileBlock;

                    TileForm form = new TileForm();
                    form.Owner = this;
                    form.Palette = _Display.Bitmap.Palette;
                    form.CommonChunk = tileChunk;
                    form.Chunks = new List<TileBlock>();
                    foreach (Chunk chunk in _CommandList.SelectedObjects)
                    {
                        TileBlock clonedChunk = (TileBlock)chunk.Clone();
                        clonedChunk.Index = chunk.Index;
                        form.Chunks.Add(clonedChunk);
                    }
                    form.Changed += new EventHandler(TileForm_Changed);
                    form.Canceled += new EventHandler(TileForm_Canceled);
                    form.Show();
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Step 2 of multiple chunk editing - find values that are common to all of the chunks
        /// (and check all of the selected chunks are of the same type)
        /// </summary>
        private Chunk FindCommonSelectedValues()
        {
            Chunk result = null;

            Chunk.InstructionType type = Chunk.InstructionType.Unknown;
            for (int index = 0; index < _CommandList.SelectedObjects.Count; index++)
            {
                Chunk chunk = (Chunk)_CommandList.SelectedObjects[index];
                if (chunk.Type == Chunk.InstructionType.LoadColTableHigh ||
                    chunk.Type == Chunk.InstructionType.LoadColTableLow)
                {
                    if (_CommandList.SelectedObjects.Count == 1)
                    {
                        result = chunk;
                    }
                    else
                    {
                        result = null;
                    }
                    break;
                }

                if (index == 0)
                {
                    type = chunk.Type;
                    result = (Chunk)chunk.Clone();
                    result.Index = chunk.Index;
                }
                else if (type != chunk.Type)
                {
                    MessageBox.Show("Editing of multiple CD+G Command types is not supported.");
                    result = null;
                    break;
                }

                GetCommonValues(result, chunk);
            }
            return result;
        }

        /// <summary>
        /// Helper function to set any values to null in the commonChunk that 
        /// are different to the chunk passed in.
        /// </summary>
        /// <param name="commonChunk"></param>
        /// <param name="chunk"></param>
        private void GetCommonValues(Chunk commonChunk, Chunk chunk)
        {
            if (commonChunk.Type != chunk.Type)
            {
                throw new InvalidOperationException();
            }

            if (commonChunk.Index != chunk.Index)
            {
                commonChunk.Index = null;
            }

            switch (chunk.Type)
            {
                case Chunk.InstructionType.ScrollPreset:
                case Chunk.InstructionType.ScrollCopy:
                {
                    if (chunk.Type == Chunk.InstructionType.ScrollPreset)
                    {
                        ScrollPreset scrollPreset = (ScrollPreset)chunk;
                        ScrollPreset commonScrollPreset = (ScrollPreset)commonChunk;

                        if (scrollPreset.FillColor != commonScrollPreset.FillColor)
                        {
                            commonScrollPreset.FillColor = null;
                        }
                    }

                    ScrollChunk scrollChunk = (ScrollChunk)chunk;
                    ScrollChunk commonScrollChunk = (ScrollChunk)commonChunk;

                    if (scrollChunk.HorizontalScrollInstruction != commonScrollChunk.HorizontalScrollInstruction)
                    {
                        commonScrollChunk.HorizontalScrollInstruction = null;
                    }

                    if (scrollChunk.VerticalScrollInstruction != commonScrollChunk.VerticalScrollInstruction)
                    {
                        commonScrollChunk.VerticalScrollInstruction = null;
                    }

                    if (scrollChunk.HorizontalScrollOffset != commonScrollChunk.HorizontalScrollOffset)
                    {
                        commonScrollChunk.HorizontalScrollOffset = null;
                    }

                    if (scrollChunk.VerticalScrollOffset != commonScrollChunk.VerticalScrollOffset)
                    {
                        commonScrollChunk.VerticalScrollOffset = null;
                    }

                    break;
                }

                case Chunk.InstructionType.BorderPreset:
                {
                    BorderPreset borderPreset = (BorderPreset)chunk;
                    BorderPreset commonBorderPreset = (BorderPreset)commonChunk;
                    if (borderPreset.Colour != commonBorderPreset.Colour)
                    {
                        commonBorderPreset.Colour = null;
                    }
                    break;
                }

                case Chunk.InstructionType.MemoryPreset:
                {
                    MemoryPreset memoryPreset = (MemoryPreset)chunk;
                    MemoryPreset commonMemoryPreset = (MemoryPreset)commonChunk;
                    if (commonMemoryPreset.ColourIndex != memoryPreset.ColourIndex)
                    {
                        commonMemoryPreset.ColourIndex = null;
                    }
                    if (commonMemoryPreset.Repeat != memoryPreset.Repeat)
                    {
                        commonMemoryPreset.Repeat = null;
                    }
                    break;
                }

                case Chunk.InstructionType.TileNormal:
                case Chunk.InstructionType.TileXor:
                {
                    TileBlock tileBlock = (TileBlock)chunk;
                    TileBlock commonTileBlock = (TileBlock)commonChunk;

                    if (tileBlock.OffColour != commonTileBlock.OffColour)
                    {
                        commonTileBlock.OffColour = null;
                    }

                    if (tileBlock.OnColour != commonTileBlock.OnColour)
                    {
                        commonTileBlock.OnColour = null;
                    }

                    if (tileBlock.Row != commonTileBlock.Row)
                    {
                        commonTileBlock.Row = null;
                    }

                    if (tileBlock.Column != commonTileBlock.Column)
                    {
                        commonTileBlock.Column = null;
                    }

                    if (!tileBlock.ComparePixels(commonTileBlock))
                    {
                        commonTileBlock.NullPixels = true;
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Step 3 - Apply only the non-null values from the common chunk.
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="commonChunk"></param>
        private void ApplyValues(Chunk chunk, Chunk commonChunk)
        {
            switch (commonChunk.Type)
            {
                case Chunk.InstructionType.ScrollPreset:
                case Chunk.InstructionType.ScrollCopy:
                {
                    if (chunk is ScrollPreset)
                    {
                        ScrollPreset formPresetChunk = (ScrollPreset)commonChunk;
                        if (formPresetChunk.FillColor.HasValue)
                        {
                            (chunk as ScrollPreset).FillColor = formPresetChunk.FillColor;
                        }
                    }

                    ScrollChunk scrollChunk = (ScrollChunk)chunk;
                    ScrollChunk commonScrollChunk = (ScrollChunk)commonChunk;

                    if (commonScrollChunk.HorizontalScrollInstruction.HasValue)
                    {
                        scrollChunk.HorizontalScrollInstruction = commonScrollChunk.HorizontalScrollInstruction;
                    }
                    if (commonScrollChunk.VerticalScrollInstruction.HasValue)
                    {
                        scrollChunk.VerticalScrollInstruction = commonScrollChunk.VerticalScrollInstruction;
                    }
                    if (commonScrollChunk.HorizontalScrollOffset.HasValue)
                    {
                        scrollChunk.HorizontalScrollOffset = commonScrollChunk.HorizontalScrollOffset;
                    }
                    if (commonScrollChunk.VerticalScrollOffset.HasValue)
                    {
                        scrollChunk.VerticalScrollOffset = commonScrollChunk.VerticalScrollOffset;
                    }

                    break;
                }

                case Chunk.InstructionType.BorderPreset:
                {
                    BorderPreset commonBorderChunk = (BorderPreset)commonChunk;
                    if (commonBorderChunk.Colour.HasValue)
                    {
                        (chunk as BorderPreset).Colour = commonBorderChunk.Colour;
                    }
                    break;
                }

                case Chunk.InstructionType.MemoryPreset:
                {
                    MemoryPreset commonMemoryPreset = (MemoryPreset)commonChunk;
                    if (commonMemoryPreset.Repeat.HasValue)
                    {
                        (chunk as MemoryPreset).Repeat = commonMemoryPreset.Repeat;
                    }
                    if (commonMemoryPreset.ColourIndex.HasValue)
                    {
                        (chunk as MemoryPreset).ColourIndex = commonMemoryPreset.ColourIndex;
                    }
                    break;
                }
            }
        }

        #endregion

        #region Single Instance

        public delegate void ProcessParametersDelegate(object sender, string[] args);
        public void ProcessParameters(object sender, string[] args)
        {
            // The form has loaded, and initialization will have been be done.

            if (args.Length == 1)
            {
                if (PromptForSave())
                {
                    OpenFile(args[0]);
                }
            }
        }
        #endregion

        #region Data

        /// <summary>
        /// The current file 
        /// </summary>
        Cdg.CdgFile _File;

        /// <summary>
        /// Type of instruction to search for.
        /// </summary>
        Cdg.Chunks.Chunk.InstructionType _SearchType;

        /// <summary>
        /// Whether the current file has been changed.
        /// </summary>
        bool _FileChanged;

        /// <summary>
        /// Command line arguments
        /// </summary>
        string[] _Args;

        /// <summary>
        /// Original tile blocks before editing
        /// </summary>
        Dictionary<int, TileBlock> _OriginalTileBlocks = new Dictionary<int, TileBlock>();

        int _FilePostion = 0;

        /// <summary>
        /// Clipboard format string.
        /// </summary>
        string _ClipboardFormat;

        #endregion               
    }
}
