﻿namespace CDGEditor
{
    partial class ScrollForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._CancelButton = new System.Windows.Forms.Button();
            this._OKButton = new System.Windows.Forms.Button();
            this._PresetColourLabel = new System.Windows.Forms.Label();
            this._XOffsetNumeric = new System.Windows.Forms.NumericUpDown();
            this._XOffsetLabel = new System.Windows.Forms.Label();
            this._YOffsetLabel = new System.Windows.Forms.Label();
            this._YOffsetNumeric = new System.Windows.Forms.NumericUpDown();
            this._ScrollGroupBox = new System.Windows.Forms.GroupBox();
            this._VerticalScrollLabel = new System.Windows.Forms.Label();
            this._HorizontalScrollLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this._VSDownRadio = new System.Windows.Forms.RadioButton();
            this._VSNoneRadio = new System.Windows.Forms.RadioButton();
            this._VSUpRadio = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this._HSNoneRadio = new System.Windows.Forms.RadioButton();
            this._HSLeftRadio = new System.Windows.Forms.RadioButton();
            this._HSRightRadio = new System.Windows.Forms.RadioButton();
            this._OffsetGroupBox = new System.Windows.Forms.GroupBox();
            this._PresetColour = new Cdg.Controls.ColourControl();
            ((System.ComponentModel.ISupportInitialize)(this._XOffsetNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._YOffsetNumeric)).BeginInit();
            this._ScrollGroupBox.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this._OffsetGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _CancelButton
            // 
            this._CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._CancelButton.Location = new System.Drawing.Point(254, 41);
            this._CancelButton.Name = "_CancelButton";
            this._CancelButton.Size = new System.Drawing.Size(75, 23);
            this._CancelButton.TabIndex = 8;
            this._CancelButton.Text = "&Cancel";
            this._CancelButton.UseVisualStyleBackColor = true;
            // 
            // _OKButton
            // 
            this._OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OKButton.Location = new System.Drawing.Point(254, 12);
            this._OKButton.Name = "_OKButton";
            this._OKButton.Size = new System.Drawing.Size(75, 23);
            this._OKButton.TabIndex = 7;
            this._OKButton.Text = "&OK";
            this._OKButton.UseVisualStyleBackColor = true;
            // 
            // _PresetColourLabel
            // 
            this._PresetColourLabel.AutoSize = true;
            this._PresetColourLabel.Location = new System.Drawing.Point(12, 167);
            this._PresetColourLabel.Name = "_PresetColourLabel";
            this._PresetColourLabel.Size = new System.Drawing.Size(73, 13);
            this._PresetColourLabel.TabIndex = 11;
            this._PresetColourLabel.Text = "Preset Colour:";
            // 
            // _XOffsetNumeric
            // 
            this._XOffsetNumeric.Location = new System.Drawing.Point(71, 19);
            this._XOffsetNumeric.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this._XOffsetNumeric.Name = "_XOffsetNumeric";
            this._XOffsetNumeric.Size = new System.Drawing.Size(47, 20);
            this._XOffsetNumeric.TabIndex = 14;
            this._XOffsetNumeric.ValueChanged += new System.EventHandler(this._XOffsetNumeric_ValueChanged);
            // 
            // _XOffsetLabel
            // 
            this._XOffsetLabel.AutoSize = true;
            this._XOffsetLabel.Location = new System.Drawing.Point(16, 21);
            this._XOffsetLabel.Name = "_XOffsetLabel";
            this._XOffsetLabel.Size = new System.Drawing.Size(57, 13);
            this._XOffsetLabel.TabIndex = 15;
            this._XOffsetLabel.Text = "Horizontal:";
            // 
            // _YOffsetLabel
            // 
            this._YOffsetLabel.AutoSize = true;
            this._YOffsetLabel.Location = new System.Drawing.Point(124, 21);
            this._YOffsetLabel.Name = "_YOffsetLabel";
            this._YOffsetLabel.Size = new System.Drawing.Size(45, 13);
            this._YOffsetLabel.TabIndex = 16;
            this._YOffsetLabel.Text = "Vertical:";
            // 
            // _YOffsetNumeric
            // 
            this._YOffsetNumeric.Location = new System.Drawing.Point(179, 19);
            this._YOffsetNumeric.Maximum = new decimal(new int[] {
            11,
            0,
            0,
            0});
            this._YOffsetNumeric.Name = "_YOffsetNumeric";
            this._YOffsetNumeric.Size = new System.Drawing.Size(47, 20);
            this._YOffsetNumeric.TabIndex = 13;
            this._YOffsetNumeric.ValueChanged += new System.EventHandler(this._YOffsetNumeric_ValueChanged);
            // 
            // _ScrollGroupBox
            // 
            this._ScrollGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ScrollGroupBox.Controls.Add(this._VerticalScrollLabel);
            this._ScrollGroupBox.Controls.Add(this._HorizontalScrollLabel);
            this._ScrollGroupBox.Controls.Add(this.panel2);
            this._ScrollGroupBox.Controls.Add(this.panel1);
            this._ScrollGroupBox.Location = new System.Drawing.Point(12, 12);
            this._ScrollGroupBox.Name = "_ScrollGroupBox";
            this._ScrollGroupBox.Size = new System.Drawing.Size(236, 75);
            this._ScrollGroupBox.TabIndex = 17;
            this._ScrollGroupBox.TabStop = false;
            this._ScrollGroupBox.Text = "Scroll";
            // 
            // _VerticalScrollLabel
            // 
            this._VerticalScrollLabel.AutoSize = true;
            this._VerticalScrollLabel.Location = new System.Drawing.Point(16, 54);
            this._VerticalScrollLabel.Name = "_VerticalScrollLabel";
            this._VerticalScrollLabel.Size = new System.Drawing.Size(45, 13);
            this._VerticalScrollLabel.TabIndex = 0;
            this._VerticalScrollLabel.Text = "Vertical:";
            // 
            // _HorizontalScrollLabel
            // 
            this._HorizontalScrollLabel.AutoSize = true;
            this._HorizontalScrollLabel.Location = new System.Drawing.Point(11, 26);
            this._HorizontalScrollLabel.Name = "_HorizontalScrollLabel";
            this._HorizontalScrollLabel.Size = new System.Drawing.Size(57, 13);
            this._HorizontalScrollLabel.TabIndex = 0;
            this._HorizontalScrollLabel.Text = "Horizontal:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this._VSDownRadio);
            this.panel2.Controls.Add(this._VSNoneRadio);
            this.panel2.Controls.Add(this._VSUpRadio);
            this.panel2.Location = new System.Drawing.Point(79, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(175, 28);
            this.panel2.TabIndex = 2;
            // 
            // _VSDownRadio
            // 
            this._VSDownRadio.AutoSize = true;
            this._VSDownRadio.Location = new System.Drawing.Point(108, 5);
            this._VSDownRadio.Name = "_VSDownRadio";
            this._VSDownRadio.Size = new System.Drawing.Size(53, 17);
            this._VSDownRadio.TabIndex = 1;
            this._VSDownRadio.Text = "Down";
            this._VSDownRadio.UseVisualStyleBackColor = true;
            this._VSDownRadio.CheckedChanged += new System.EventHandler(this._CheckedChanged);
            // 
            // _VSNoneRadio
            // 
            this._VSNoneRadio.AutoSize = true;
            this._VSNoneRadio.Location = new System.Drawing.Point(3, 5);
            this._VSNoneRadio.Name = "_VSNoneRadio";
            this._VSNoneRadio.Size = new System.Drawing.Size(51, 17);
            this._VSNoneRadio.TabIndex = 1;
            this._VSNoneRadio.Text = "None";
            this._VSNoneRadio.UseVisualStyleBackColor = true;
            this._VSNoneRadio.CheckedChanged += new System.EventHandler(this._CheckedChanged);
            // 
            // _VSUpRadio
            // 
            this._VSUpRadio.AutoSize = true;
            this._VSUpRadio.Location = new System.Drawing.Point(60, 5);
            this._VSUpRadio.Name = "_VSUpRadio";
            this._VSUpRadio.Size = new System.Drawing.Size(39, 17);
            this._VSUpRadio.TabIndex = 1;
            this._VSUpRadio.Text = "Up";
            this._VSUpRadio.UseVisualStyleBackColor = true;
            this._VSUpRadio.CheckedChanged += new System.EventHandler(this._CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._HSNoneRadio);
            this.panel1.Controls.Add(this._HSLeftRadio);
            this.panel1.Controls.Add(this._HSRightRadio);
            this.panel1.Location = new System.Drawing.Point(79, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 25);
            this.panel1.TabIndex = 2;
            // 
            // _HSNoneRadio
            // 
            this._HSNoneRadio.AutoSize = true;
            this._HSNoneRadio.Location = new System.Drawing.Point(3, 5);
            this._HSNoneRadio.Name = "_HSNoneRadio";
            this._HSNoneRadio.Size = new System.Drawing.Size(51, 17);
            this._HSNoneRadio.TabIndex = 1;
            this._HSNoneRadio.Text = "None";
            this._HSNoneRadio.UseVisualStyleBackColor = true;
            this._HSNoneRadio.CheckedChanged += new System.EventHandler(this._CheckedChanged);
            // 
            // _HSLeftRadio
            // 
            this._HSLeftRadio.AutoSize = true;
            this._HSLeftRadio.Location = new System.Drawing.Point(60, 5);
            this._HSLeftRadio.Name = "_HSLeftRadio";
            this._HSLeftRadio.Size = new System.Drawing.Size(43, 17);
            this._HSLeftRadio.TabIndex = 1;
            this._HSLeftRadio.Text = "Left";
            this._HSLeftRadio.UseVisualStyleBackColor = true;
            this._HSLeftRadio.CheckedChanged += new System.EventHandler(this._CheckedChanged);
            // 
            // _HSRightRadio
            // 
            this._HSRightRadio.AutoSize = true;
            this._HSRightRadio.Location = new System.Drawing.Point(108, 5);
            this._HSRightRadio.Name = "_HSRightRadio";
            this._HSRightRadio.Size = new System.Drawing.Size(50, 17);
            this._HSRightRadio.TabIndex = 1;
            this._HSRightRadio.Text = "Right";
            this._HSRightRadio.UseVisualStyleBackColor = true;
            this._HSRightRadio.CheckedChanged += new System.EventHandler(this._CheckedChanged);
            // 
            // _OffsetGroupBox
            // 
            this._OffsetGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._OffsetGroupBox.Controls.Add(this._XOffsetNumeric);
            this._OffsetGroupBox.Controls.Add(this._YOffsetNumeric);
            this._OffsetGroupBox.Controls.Add(this._YOffsetLabel);
            this._OffsetGroupBox.Controls.Add(this._XOffsetLabel);
            this._OffsetGroupBox.Location = new System.Drawing.Point(12, 93);
            this._OffsetGroupBox.Name = "_OffsetGroupBox";
            this._OffsetGroupBox.Size = new System.Drawing.Size(236, 48);
            this._OffsetGroupBox.TabIndex = 17;
            this._OffsetGroupBox.TabStop = false;
            this._OffsetGroupBox.Text = "Offset";
            // 
            // _PresetColour
            // 
            this._PresetColour.BackColor = System.Drawing.SystemColors.Control;
            this._PresetColour.Index = 0;
            this._PresetColour.Location = new System.Drawing.Point(88, 150);
            this._PresetColour.Name = "_PresetColour";
            this._PresetColour.Palette = null;
            this._PresetColour.ReadOnly = false;
            this._PresetColour.Selected = false;
            this._PresetColour.Size = new System.Drawing.Size(27, 33);
            this._PresetColour.TabIndex = 10;
            // 
            // ScrollForm
            // 
            this.AcceptButton = this._OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._CancelButton;
            this.ClientSize = new System.Drawing.Size(355, 179);
            this.ControlBox = false;
            this.Controls.Add(this._OffsetGroupBox);
            this.Controls.Add(this._ScrollGroupBox);
            this.Controls.Add(this._PresetColourLabel);
            this.Controls.Add(this._PresetColour);
            this.Controls.Add(this._CancelButton);
            this.Controls.Add(this._OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(371, 213);
            this.MinimumSize = new System.Drawing.Size(371, 176);
            this.Name = "ScrollForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Scroll";
            this.Load += new System.EventHandler(this.ScrollForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._XOffsetNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._YOffsetNumeric)).EndInit();
            this._ScrollGroupBox.ResumeLayout(false);
            this._ScrollGroupBox.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this._OffsetGroupBox.ResumeLayout(false);
            this._OffsetGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _OKButton;
        private System.Windows.Forms.Label _PresetColourLabel;
        private Cdg.Controls.ColourControl _PresetColour;
        private System.Windows.Forms.NumericUpDown _XOffsetNumeric;
        private System.Windows.Forms.Label _XOffsetLabel;
        private System.Windows.Forms.Label _YOffsetLabel;
        private System.Windows.Forms.NumericUpDown _YOffsetNumeric;
        private System.Windows.Forms.GroupBox _ScrollGroupBox;
        private System.Windows.Forms.RadioButton _VSDownRadio;
        private System.Windows.Forms.RadioButton _HSRightRadio;
        private System.Windows.Forms.RadioButton _VSUpRadio;
        private System.Windows.Forms.RadioButton _HSLeftRadio;
        private System.Windows.Forms.RadioButton _VSNoneRadio;
        private System.Windows.Forms.RadioButton _HSNoneRadio;
        private System.Windows.Forms.GroupBox _OffsetGroupBox;
        private System.Windows.Forms.Label _VerticalScrollLabel;
        private System.Windows.Forms.Label _HorizontalScrollLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
    }
}