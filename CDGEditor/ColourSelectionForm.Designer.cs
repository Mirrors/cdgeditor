﻿namespace CDGEditor
{
    partial class ColourSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._ColourNumeric = new System.Windows.Forms.NumericUpDown();
            this._ColourLabel = new System.Windows.Forms.Label();
            this._CancelButton = new System.Windows.Forms.Button();
            this._OKButton = new System.Windows.Forms.Button();
            this._RepeatNumeric = new System.Windows.Forms.NumericUpDown();
            this._RepeatLabel = new System.Windows.Forms.Label();
            this._ColourControl1 = new Cdg.Controls.ColourControl();
            this._ColourControl2 = new Cdg.Controls.ColourControl();
            this._ColourControl3 = new Cdg.Controls.ColourControl();
            this._ColourControl4 = new Cdg.Controls.ColourControl();
            this._ColourControl5 = new Cdg.Controls.ColourControl();
            this._ColourControl6 = new Cdg.Controls.ColourControl();
            this._ColourControl7 = new Cdg.Controls.ColourControl();
            this._ColourControl8 = new Cdg.Controls.ColourControl();
            this._ColourControl9 = new Cdg.Controls.ColourControl();
            this._ColourControl10 = new Cdg.Controls.ColourControl();
            this._ColourControl11 = new Cdg.Controls.ColourControl();
            this._ColourControl12 = new Cdg.Controls.ColourControl();
            this._ColourControl13 = new Cdg.Controls.ColourControl();
            this._ColourControl14 = new Cdg.Controls.ColourControl();
            this._ColourControl15 = new Cdg.Controls.ColourControl();
            this._ColourControl16 = new Cdg.Controls.ColourControl();
            ((System.ComponentModel.ISupportInitialize)(this._ColourNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._RepeatNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // _ColourNumeric
            // 
            this._ColourNumeric.Location = new System.Drawing.Point(355, 152);
            this._ColourNumeric.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this._ColourNumeric.Name = "_ColourNumeric";
            this._ColourNumeric.Size = new System.Drawing.Size(43, 20);
            this._ColourNumeric.TabIndex = 2;
            this._ColourNumeric.ValueChanged += new System.EventHandler(this._ColourNumeric_ValueChanged);
            // 
            // _ColourLabel
            // 
            this._ColourLabel.AutoSize = true;
            this._ColourLabel.Location = new System.Drawing.Point(309, 154);
            this._ColourLabel.Name = "_ColourLabel";
            this._ColourLabel.Size = new System.Drawing.Size(40, 13);
            this._ColourLabel.TabIndex = 1;
            this._ColourLabel.Text = "Colour:";
            // 
            // _CancelButton
            // 
            this._CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._CancelButton.Location = new System.Drawing.Point(403, 41);
            this._CancelButton.Name = "_CancelButton";
            this._CancelButton.Size = new System.Drawing.Size(75, 23);
            this._CancelButton.TabIndex = 4;
            this._CancelButton.Text = "&Cancel";
            this._CancelButton.UseVisualStyleBackColor = true;
            // 
            // _OKButton
            // 
            this._OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OKButton.Location = new System.Drawing.Point(403, 12);
            this._OKButton.Name = "_OKButton";
            this._OKButton.Size = new System.Drawing.Size(75, 23);
            this._OKButton.TabIndex = 3;
            this._OKButton.Text = "&OK";
            this._OKButton.UseVisualStyleBackColor = true;
            // 
            // _RepeatNumeric
            // 
            this._RepeatNumeric.Location = new System.Drawing.Point(260, 152);
            this._RepeatNumeric.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this._RepeatNumeric.Name = "_RepeatNumeric";
            this._RepeatNumeric.Size = new System.Drawing.Size(43, 20);
            this._RepeatNumeric.TabIndex = 2;
            this._RepeatNumeric.Visible = false;
            this._RepeatNumeric.ValueChanged += new System.EventHandler(this._RepeatNumeric_ValueChanged);
            // 
            // _RepeatLabel
            // 
            this._RepeatLabel.AutoSize = true;
            this._RepeatLabel.Location = new System.Drawing.Point(209, 154);
            this._RepeatLabel.Name = "_RepeatLabel";
            this._RepeatLabel.Size = new System.Drawing.Size(45, 13);
            this._RepeatLabel.TabIndex = 1;
            this._RepeatLabel.Text = "Repeat:";
            this._RepeatLabel.Visible = false;
            // 
            // _ColourControl1
            // 
            this._ColourControl1.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl1.Index = 0;
            this._ColourControl1.Location = new System.Drawing.Point(12, 12);
            this._ColourControl1.Name = "_ColourControl1";
            this._ColourControl1.Palette = null;
            this._ColourControl1.ReadOnly = true;
            this._ColourControl1.Selected = false;
            this._ColourControl1.Size = new System.Drawing.Size(43, 60);
            this._ColourControl1.TabIndex = 0;
            this._ColourControl1.TabStop = false;
            // 
            // _ColourControl2
            // 
            this._ColourControl2.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl2.Index = 0;
            this._ColourControl2.Location = new System.Drawing.Point(61, 12);
            this._ColourControl2.Name = "_ColourControl2";
            this._ColourControl2.Palette = null;
            this._ColourControl2.ReadOnly = true;
            this._ColourControl2.Selected = false;
            this._ColourControl2.Size = new System.Drawing.Size(43, 60);
            this._ColourControl2.TabIndex = 0;
            this._ColourControl2.TabStop = false;
            // 
            // _ColourControl3
            // 
            this._ColourControl3.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl3.Index = 0;
            this._ColourControl3.Location = new System.Drawing.Point(110, 12);
            this._ColourControl3.Name = "_ColourControl3";
            this._ColourControl3.Palette = null;
            this._ColourControl3.ReadOnly = true;
            this._ColourControl3.Selected = false;
            this._ColourControl3.Size = new System.Drawing.Size(43, 60);
            this._ColourControl3.TabIndex = 0;
            this._ColourControl3.TabStop = false;
            // 
            // _ColourControl4
            // 
            this._ColourControl4.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl4.Index = 0;
            this._ColourControl4.Location = new System.Drawing.Point(159, 12);
            this._ColourControl4.Name = "_ColourControl4";
            this._ColourControl4.Palette = null;
            this._ColourControl4.ReadOnly = true;
            this._ColourControl4.Selected = false;
            this._ColourControl4.Size = new System.Drawing.Size(43, 60);
            this._ColourControl4.TabIndex = 2;
            this._ColourControl4.TabStop = false;
            // 
            // _ColourControl5
            // 
            this._ColourControl5.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl5.Index = 0;
            this._ColourControl5.Location = new System.Drawing.Point(208, 12);
            this._ColourControl5.Name = "_ColourControl5";
            this._ColourControl5.Palette = null;
            this._ColourControl5.ReadOnly = true;
            this._ColourControl5.Selected = false;
            this._ColourControl5.Size = new System.Drawing.Size(43, 60);
            this._ColourControl5.TabIndex = 4;
            this._ColourControl5.TabStop = false;
            // 
            // _ColourControl6
            // 
            this._ColourControl6.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl6.Index = 0;
            this._ColourControl6.Location = new System.Drawing.Point(257, 12);
            this._ColourControl6.Name = "_ColourControl6";
            this._ColourControl6.Palette = null;
            this._ColourControl6.ReadOnly = true;
            this._ColourControl6.Selected = false;
            this._ColourControl6.Size = new System.Drawing.Size(43, 60);
            this._ColourControl6.TabIndex = 5;
            this._ColourControl6.TabStop = false;
            // 
            // _ColourControl7
            // 
            this._ColourControl7.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl7.Index = 0;
            this._ColourControl7.Location = new System.Drawing.Point(306, 12);
            this._ColourControl7.Name = "_ColourControl7";
            this._ColourControl7.Palette = null;
            this._ColourControl7.ReadOnly = true;
            this._ColourControl7.Selected = false;
            this._ColourControl7.Size = new System.Drawing.Size(43, 60);
            this._ColourControl7.TabIndex = 3;
            this._ColourControl7.TabStop = false;
            // 
            // _ColourControl8
            // 
            this._ColourControl8.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl8.Index = 0;
            this._ColourControl8.Location = new System.Drawing.Point(355, 12);
            this._ColourControl8.Name = "_ColourControl8";
            this._ColourControl8.Palette = null;
            this._ColourControl8.ReadOnly = true;
            this._ColourControl8.Selected = false;
            this._ColourControl8.Size = new System.Drawing.Size(43, 60);
            this._ColourControl8.TabIndex = 6;
            this._ColourControl8.TabStop = false;
            // 
            // _ColourControl9
            // 
            this._ColourControl9.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl9.Index = 0;
            this._ColourControl9.Location = new System.Drawing.Point(12, 78);
            this._ColourControl9.Name = "_ColourControl9";
            this._ColourControl9.Palette = null;
            this._ColourControl9.ReadOnly = true;
            this._ColourControl9.Selected = false;
            this._ColourControl9.Size = new System.Drawing.Size(43, 60);
            this._ColourControl9.TabIndex = 9;
            this._ColourControl9.TabStop = false;
            // 
            // _ColourControl10
            // 
            this._ColourControl10.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl10.Index = 0;
            this._ColourControl10.Location = new System.Drawing.Point(61, 78);
            this._ColourControl10.Name = "_ColourControl10";
            this._ColourControl10.Palette = null;
            this._ColourControl10.ReadOnly = true;
            this._ColourControl10.Selected = false;
            this._ColourControl10.Size = new System.Drawing.Size(43, 60);
            this._ColourControl10.TabIndex = 7;
            this._ColourControl10.TabStop = false;
            // 
            // _ColourControl11
            // 
            this._ColourControl11.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl11.Index = 0;
            this._ColourControl11.Location = new System.Drawing.Point(110, 78);
            this._ColourControl11.Name = "_ColourControl11";
            this._ColourControl11.Palette = null;
            this._ColourControl11.ReadOnly = true;
            this._ColourControl11.Selected = false;
            this._ColourControl11.Size = new System.Drawing.Size(43, 60);
            this._ColourControl11.TabIndex = 8;
            this._ColourControl11.TabStop = false;
            // 
            // _ColourControl12
            // 
            this._ColourControl12.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl12.Index = 0;
            this._ColourControl12.Location = new System.Drawing.Point(159, 78);
            this._ColourControl12.Name = "_ColourControl12";
            this._ColourControl12.Palette = null;
            this._ColourControl12.ReadOnly = true;
            this._ColourControl12.Selected = false;
            this._ColourControl12.Size = new System.Drawing.Size(43, 60);
            this._ColourControl12.TabIndex = 10;
            this._ColourControl12.TabStop = false;
            // 
            // _ColourControl13
            // 
            this._ColourControl13.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl13.Index = 0;
            this._ColourControl13.Location = new System.Drawing.Point(208, 78);
            this._ColourControl13.Name = "_ColourControl13";
            this._ColourControl13.Palette = null;
            this._ColourControl13.ReadOnly = true;
            this._ColourControl13.Selected = false;
            this._ColourControl13.Size = new System.Drawing.Size(43, 60);
            this._ColourControl13.TabIndex = 12;
            this._ColourControl13.TabStop = false;
            // 
            // _ColourControl14
            // 
            this._ColourControl14.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl14.Index = 0;
            this._ColourControl14.Location = new System.Drawing.Point(257, 78);
            this._ColourControl14.Name = "_ColourControl14";
            this._ColourControl14.Palette = null;
            this._ColourControl14.ReadOnly = true;
            this._ColourControl14.Selected = false;
            this._ColourControl14.Size = new System.Drawing.Size(43, 60);
            this._ColourControl14.TabIndex = 13;
            this._ColourControl14.TabStop = false;
            // 
            // _ColourControl15
            // 
            this._ColourControl15.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl15.Index = 0;
            this._ColourControl15.Location = new System.Drawing.Point(306, 78);
            this._ColourControl15.Name = "_ColourControl15";
            this._ColourControl15.Palette = null;
            this._ColourControl15.ReadOnly = true;
            this._ColourControl15.Selected = false;
            this._ColourControl15.Size = new System.Drawing.Size(43, 60);
            this._ColourControl15.TabIndex = 11;
            this._ColourControl15.TabStop = false;
            // 
            // _ColourControl16
            // 
            this._ColourControl16.BackColor = System.Drawing.SystemColors.Control;
            this._ColourControl16.Index = 0;
            this._ColourControl16.Location = new System.Drawing.Point(355, 78);
            this._ColourControl16.Name = "_ColourControl16";
            this._ColourControl16.Palette = null;
            this._ColourControl16.ReadOnly = true;
            this._ColourControl16.Selected = false;
            this._ColourControl16.Size = new System.Drawing.Size(43, 60);
            this._ColourControl16.TabIndex = 14;
            this._ColourControl16.TabStop = false;
            // 
            // ColourSelectionForm
            // 
            this.AcceptButton = this._OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._CancelButton;
            this.ClientSize = new System.Drawing.Size(490, 167);
            this.ControlBox = false;
            this.Controls.Add(this._ColourControl1);
            this.Controls.Add(this._ColourControl2);
            this.Controls.Add(this._ColourControl3);
            this.Controls.Add(this._ColourControl4);
            this.Controls.Add(this._ColourControl5);
            this.Controls.Add(this._ColourControl6);
            this.Controls.Add(this._ColourControl7);
            this.Controls.Add(this._ColourControl8);
            this.Controls.Add(this._ColourControl9);
            this.Controls.Add(this._ColourControl10);
            this.Controls.Add(this._ColourControl11);
            this.Controls.Add(this._ColourControl12);
            this.Controls.Add(this._ColourControl13);
            this.Controls.Add(this._ColourControl14);
            this.Controls.Add(this._ColourControl15);
            this.Controls.Add(this._ColourControl16);
            this.Controls.Add(this._CancelButton);
            this.Controls.Add(this._OKButton);
            this.Controls.Add(this._RepeatLabel);
            this.Controls.Add(this._ColourLabel);
            this.Controls.Add(this._RepeatNumeric);
            this.Controls.Add(this._ColourNumeric);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(506, 205);
            this.MinimumSize = new System.Drawing.Size(506, 104);
            this.Name = "ColourSelectionForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Colour";
            ((System.ComponentModel.ISupportInitialize)(this._ColourNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._RepeatNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Cdg.Controls.ColourControl _ColourControl1;
        private System.Windows.Forms.NumericUpDown _ColourNumeric;
        private Cdg.Controls.ColourControl _ColourControl3;
        private Cdg.Controls.ColourControl _ColourControl2;
        private Cdg.Controls.ColourControl _ColourControl4;
        private Cdg.Controls.ColourControl _ColourControl8;
        private Cdg.Controls.ColourControl _ColourControl6;
        private Cdg.Controls.ColourControl _ColourControl7;
        private Cdg.Controls.ColourControl _ColourControl5;
        private Cdg.Controls.ColourControl _ColourControl16;
        private Cdg.Controls.ColourControl _ColourControl14;
        private Cdg.Controls.ColourControl _ColourControl15;
        private Cdg.Controls.ColourControl _ColourControl13;
        private Cdg.Controls.ColourControl _ColourControl12;
        private Cdg.Controls.ColourControl _ColourControl10;
        private Cdg.Controls.ColourControl _ColourControl11;
        private Cdg.Controls.ColourControl _ColourControl9;
        private System.Windows.Forms.Label _ColourLabel;
        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _OKButton;
        private System.Windows.Forms.NumericUpDown _RepeatNumeric;
        private System.Windows.Forms.Label _RepeatLabel;
    }
}