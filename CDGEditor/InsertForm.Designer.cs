﻿namespace CDGEditor
{
    partial class InsertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._CancelButton = new System.Windows.Forms.Button();
            this._OKButton = new System.Windows.Forms.Button();
            this._ChunkModeRadioButton = new System.Windows.Forms.RadioButton();
            this._TimeModeRadioButton = new System.Windows.Forms.RadioButton();
            this._AmountNumeric = new System.Windows.Forms.NumericUpDown();
            this._ModeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._AmountNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // _CancelButton
            // 
            this._CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._CancelButton.Location = new System.Drawing.Point(271, 41);
            this._CancelButton.Name = "_CancelButton";
            this._CancelButton.Size = new System.Drawing.Size(75, 23);
            this._CancelButton.TabIndex = 10;
            this._CancelButton.Text = "&Cancel";
            this._CancelButton.UseVisualStyleBackColor = true;
            // 
            // _OKButton
            // 
            this._OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OKButton.Location = new System.Drawing.Point(271, 12);
            this._OKButton.Name = "_OKButton";
            this._OKButton.Size = new System.Drawing.Size(75, 23);
            this._OKButton.TabIndex = 9;
            this._OKButton.Text = "&OK";
            this._OKButton.UseVisualStyleBackColor = true;
            // 
            // _ChunkModeRadioButton
            // 
            this._ChunkModeRadioButton.AutoSize = true;
            this._ChunkModeRadioButton.Checked = true;
            this._ChunkModeRadioButton.Location = new System.Drawing.Point(12, 15);
            this._ChunkModeRadioButton.Name = "_ChunkModeRadioButton";
            this._ChunkModeRadioButton.Size = new System.Drawing.Size(87, 17);
            this._ChunkModeRadioButton.TabIndex = 11;
            this._ChunkModeRadioButton.TabStop = true;
            this._ChunkModeRadioButton.Text = "Chunk Count";
            this._ChunkModeRadioButton.UseVisualStyleBackColor = true;
            this._ChunkModeRadioButton.CheckedChanged += new System.EventHandler(this._ChunkModeRadioButton_CheckedChanged);
            // 
            // _TimeModeRadioButton
            // 
            this._TimeModeRadioButton.AutoSize = true;
            this._TimeModeRadioButton.Location = new System.Drawing.Point(12, 38);
            this._TimeModeRadioButton.Name = "_TimeModeRadioButton";
            this._TimeModeRadioButton.Size = new System.Drawing.Size(76, 17);
            this._TimeModeRadioButton.TabIndex = 11;
            this._TimeModeRadioButton.Text = "Time Span";
            this._TimeModeRadioButton.UseVisualStyleBackColor = true;
            this._TimeModeRadioButton.CheckedChanged += new System.EventHandler(this._TimeModeRadioButton_CheckedChanged);
            // 
            // _AmountNumeric
            // 
            this._AmountNumeric.Location = new System.Drawing.Point(105, 15);
            this._AmountNumeric.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this._AmountNumeric.Name = "_AmountNumeric";
            this._AmountNumeric.Size = new System.Drawing.Size(99, 20);
            this._AmountNumeric.TabIndex = 12;
            this._AmountNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._AmountNumeric.ValueChanged += new System.EventHandler(this._AmountNumeric_ValueChanged);
            // 
            // _ModeLabel
            // 
            this._ModeLabel.AutoSize = true;
            this._ModeLabel.Location = new System.Drawing.Point(210, 17);
            this._ModeLabel.Name = "_ModeLabel";
            this._ModeLabel.Size = new System.Drawing.Size(43, 13);
            this._ModeLabel.TabIndex = 13;
            this._ModeLabel.Text = "Chunks";
            // 
            // InsertForm
            // 
            this.AcceptButton = this._OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._CancelButton;
            this.ClientSize = new System.Drawing.Size(358, 82);
            this.ControlBox = false;
            this.Controls.Add(this._ModeLabel);
            this.Controls.Add(this._AmountNumeric);
            this.Controls.Add(this._TimeModeRadioButton);
            this.Controls.Add(this._ChunkModeRadioButton);
            this.Controls.Add(this._CancelButton);
            this.Controls.Add(this._OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "InsertForm";
            this.ShowInTaskbar = false;
            this.Text = "Insert Time Gaps";
            ((System.ComponentModel.ISupportInitialize)(this._AmountNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _OKButton;
        private System.Windows.Forms.RadioButton _ChunkModeRadioButton;
        private System.Windows.Forms.RadioButton _TimeModeRadioButton;
        private System.Windows.Forms.NumericUpDown _AmountNumeric;
        private System.Windows.Forms.Label _ModeLabel;
    }
}