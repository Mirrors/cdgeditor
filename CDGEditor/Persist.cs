﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace CDGEditor
{
    #region Attributes

    /// <summary>
    /// Attribute indicating that a property is to be persisted in the registry
    /// </summary>
    public class PersistAttribute : Attribute
    {
        public PersistAttribute()
        {
        }
    }

    #endregion

    /// <summary>
    /// Encapsulates persistant storage of UI settings
    /// </summary>
    public class Persist
    {
        #region Public Methods

        /// <summary>
        /// Saves the properties of the form marked as persistent
        /// </summary>
        /// <param name="form"></param>
        static public void Save(System.Windows.Forms.Control control)
        {
            Registry reg = new Registry(control.Name);

            Type type = control.GetType();
            PropertyInfo[] propertyInfos = type.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                // Get attributes for the property
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(control);
                if (properties != null)
                {
                    PropertyDescriptor descriptor = properties[propertyInfo.Name];
                    if (descriptor != null)
                    {
                        // Get attributes for the property
                        AttributeCollection attributes = descriptor.Attributes;

                        // Check to see if the property is a one we want to persist
                        PersistAttribute persistAttribute = (PersistAttribute)attributes[typeof(PersistAttribute)];
                        if (persistAttribute != null)
                        {
                            object obj = propertyInfo.GetValue(control, null);
                            reg.SetValue(propertyInfo.Name, obj);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the properties of the form marked as persistent
        /// </summary>
        /// <param name="form"></param>
        static public void Load(System.Windows.Forms.Control control)
        {
            Registry reg = new Registry(control.Name);

            Type type = control.GetType();
            PropertyInfo[] propertyInfos = type.GetProperties();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(control);
                if (properties != null)
                {
                    PropertyDescriptor descriptor = properties[propertyInfo.Name];
                    if (descriptor != null)
                    {
                        // Get attributes for the property
                        AttributeCollection attributes = descriptor.Attributes;

                        // Check to see if the property is a one we want to persist
                        PersistAttribute persistAttribute = (PersistAttribute)attributes[typeof(PersistAttribute)];
                        if (persistAttribute != null)
                        {
                            if (type.IsArray)
                            {
                            }
                            else
                            {
                                object obj = propertyInfo.GetValue(control, null);
                                obj = reg.GetValue(propertyInfo.Name, obj);
                                try
                                {
                                    propertyInfo.SetValue(control, obj, null);
                                }
                                catch (Exception)
                                {
                                    // If we try to load an invalid value (e.g. the spliiter
                                    // bar position), crashing out isn't very helpful.
                                    System.Windows.Forms.MessageBox.Show("Failed to load last window postions from registry.\n" + 
                                        "Maybe you've added/removed a display or moved them around.\n" + 
                                        "If the Window isn't visible, right click the task bar, choose Move then press a cursor key on the keyboard and wiggle the mouse.",
                                        "CDG Editor");
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}
