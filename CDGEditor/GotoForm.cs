﻿using System.Windows.Forms;

namespace CDGEditor
{
    public partial class GotoForm : Form
    {
        public GotoForm(string prompt)
        {
            InitializeComponent();
            _TimeLabel.Text = prompt;
        }

        public int ChunkIndex
        {
            set
            {
                int minutes = value / 18000;
                int seconds = (value - (minutes * 18000)) / 300;
                int ms = ((value % 300) * 10) / 3;

                Text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, ms);
            }
            get
            {
                string[] parts = _TimeTextBox.Text.Split(':');
                int minutes = int.Parse(parts[0]);
                int seconds = int.Parse(parts[1]);
                int ms = int.Parse(parts[2]);

                int result = minutes * 18000 +
                    seconds * 300 +
                    (int)(ms * 0.3);

                return result;
            }
        }
    }
}
