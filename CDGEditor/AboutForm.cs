﻿using System;
using System.Windows.Forms;

namespace CDGEditor
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            _VersionLabel.Text = string.Format(_VersionLabel.Text, Application.ProductVersion);
        }

        private void _SourceForgeLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://cdgeditor.sourceforge.net");

        }
    }
}
