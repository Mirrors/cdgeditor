﻿namespace CDGEditor
{
    partial class TileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TileForm));
            this._CancelButton = new System.Windows.Forms.Button();
            this._OKButton = new System.Windows.Forms.Button();
            this._OffColourLabel = new System.Windows.Forms.Label();
            this._OnColourLabel = new System.Windows.Forms.Label();
            this._XNumeric = new System.Windows.Forms.NumericUpDown();
            this._YNumeric = new System.Windows.Forms.NumericUpDown();
            this._XLabel = new System.Windows.Forms.Label();
            this._YLabel = new System.Windows.Forms.Label();
            this._NormalRadioButton = new System.Windows.Forms.RadioButton();
            this._XorRadioButton = new System.Windows.Forms.RadioButton();
            this._ToolStrip = new System.Windows.Forms.ToolStrip();
            this._Pattern0Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern1Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern2Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern3Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern4Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern5Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern6Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern8Button = new System.Windows.Forms.ToolStripButton();
            this._Pattern7Button = new System.Windows.Forms.ToolStripButton();
            this._TileGrid = new Cdg.Controls.TileGrid();
            this._OnColour = new Cdg.Controls.ColourControl();
            this._OffColour = new Cdg.Controls.ColourControl();
            ((System.ComponentModel.ISupportInitialize)(this._XNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._YNumeric)).BeginInit();
            this._ToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _CancelButton
            // 
            this._CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._CancelButton.Location = new System.Drawing.Point(268, 41);
            this._CancelButton.Name = "_CancelButton";
            this._CancelButton.Size = new System.Drawing.Size(75, 23);
            this._CancelButton.TabIndex = 6;
            this._CancelButton.Text = "&Cancel";
            this._CancelButton.UseVisualStyleBackColor = true;
            this._CancelButton.Click += new System.EventHandler(this._CancelButton_Click);
            // 
            // _OKButton
            // 
            this._OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OKButton.Location = new System.Drawing.Point(268, 12);
            this._OKButton.Name = "_OKButton";
            this._OKButton.Size = new System.Drawing.Size(75, 23);
            this._OKButton.TabIndex = 5;
            this._OKButton.Text = "&OK";
            this._OKButton.UseVisualStyleBackColor = true;
            this._OKButton.Click += new System.EventHandler(this._OKButton_Click);
            // 
            // _OffColourLabel
            // 
            this._OffColourLabel.AutoSize = true;
            this._OffColourLabel.Location = new System.Drawing.Point(154, 22);
            this._OffColourLabel.Name = "_OffColourLabel";
            this._OffColourLabel.Size = new System.Drawing.Size(57, 13);
            this._OffColourLabel.TabIndex = 9;
            this._OffColourLabel.Text = "Off Colour:";
            // 
            // _OnColourLabel
            // 
            this._OnColourLabel.AutoSize = true;
            this._OnColourLabel.Location = new System.Drawing.Point(154, 51);
            this._OnColourLabel.Name = "_OnColourLabel";
            this._OnColourLabel.Size = new System.Drawing.Size(57, 13);
            this._OnColourLabel.TabIndex = 9;
            this._OnColourLabel.Text = "On Colour:";
            // 
            // _XNumeric
            // 
            this._XNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._XNumeric.Location = new System.Drawing.Point(197, 169);
            this._XNumeric.Maximum = new decimal(new int[] {
            49,
            0,
            0,
            0});
            this._XNumeric.Name = "_XNumeric";
            this._XNumeric.Size = new System.Drawing.Size(47, 20);
            this._XNumeric.TabIndex = 11;
            this._XNumeric.ValueChanged += new System.EventHandler(this._Numeric_ValueChanged);
            // 
            // _YNumeric
            // 
            this._YNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._YNumeric.Location = new System.Drawing.Point(197, 198);
            this._YNumeric.Maximum = new decimal(new int[] {
            17,
            0,
            0,
            0});
            this._YNumeric.Name = "_YNumeric";
            this._YNumeric.Size = new System.Drawing.Size(47, 20);
            this._YNumeric.TabIndex = 11;
            this._YNumeric.ValueChanged += new System.EventHandler(this._Numeric_ValueChanged);
            // 
            // _XLabel
            // 
            this._XLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._XLabel.AutoSize = true;
            this._XLabel.Location = new System.Drawing.Point(174, 171);
            this._XLabel.Name = "_XLabel";
            this._XLabel.Size = new System.Drawing.Size(17, 13);
            this._XLabel.TabIndex = 12;
            this._XLabel.Text = "X:";
            // 
            // _YLabel
            // 
            this._YLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._YLabel.AutoSize = true;
            this._YLabel.Location = new System.Drawing.Point(174, 200);
            this._YLabel.Name = "_YLabel";
            this._YLabel.Size = new System.Drawing.Size(17, 13);
            this._YLabel.TabIndex = 12;
            this._YLabel.Text = "Y:";
            // 
            // _NormalRadioButton
            // 
            this._NormalRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._NormalRadioButton.AutoSize = true;
            this._NormalRadioButton.Location = new System.Drawing.Point(186, 95);
            this._NormalRadioButton.Name = "_NormalRadioButton";
            this._NormalRadioButton.Size = new System.Drawing.Size(58, 17);
            this._NormalRadioButton.TabIndex = 13;
            this._NormalRadioButton.TabStop = true;
            this._NormalRadioButton.Text = "Normal";
            this._NormalRadioButton.UseVisualStyleBackColor = true;
            this._NormalRadioButton.Click += new System.EventHandler(this._NormalRadioButton_Click);
            // 
            // _XorRadioButton
            // 
            this._XorRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._XorRadioButton.AutoSize = true;
            this._XorRadioButton.Location = new System.Drawing.Point(186, 121);
            this._XorRadioButton.Name = "_XorRadioButton";
            this._XorRadioButton.Size = new System.Drawing.Size(41, 17);
            this._XorRadioButton.TabIndex = 13;
            this._XorRadioButton.TabStop = true;
            this._XorRadioButton.Text = "Xor";
            this._XorRadioButton.UseVisualStyleBackColor = true;
            this._XorRadioButton.CheckedChanged += new System.EventHandler(this._XorRadioButton_CheckedChanged);
            // 
            // _ToolStrip
            // 
            this._ToolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._Pattern0Button,
            this._Pattern1Button,
            this._Pattern2Button,
            this._Pattern3Button,
            this._Pattern4Button,
            this._Pattern5Button,
            this._Pattern6Button,
            this._Pattern8Button,
            this._Pattern7Button});
            this._ToolStrip.Location = new System.Drawing.Point(0, 0);
            this._ToolStrip.Name = "_ToolStrip";
            this._ToolStrip.Size = new System.Drawing.Size(24, 243);
            this._ToolStrip.TabIndex = 15;
            this._ToolStrip.Text = "toolStrip1";
            // 
            // _Pattern0Button
            // 
            this._Pattern0Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern0Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern0Button.Image")));
            this._Pattern0Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern0Button.Name = "_Pattern0Button";
            this._Pattern0Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern0Button.Tag = "0";
            this._Pattern0Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern1Button
            // 
            this._Pattern1Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern1Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern1Button.Image")));
            this._Pattern1Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern1Button.Name = "_Pattern1Button";
            this._Pattern1Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern1Button.Tag = "1";
            this._Pattern1Button.Text = "toolStripButton2";
            this._Pattern1Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern2Button
            // 
            this._Pattern2Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern2Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern2Button.Image")));
            this._Pattern2Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern2Button.Name = "_Pattern2Button";
            this._Pattern2Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern2Button.Tag = "2";
            this._Pattern2Button.Text = "toolStripButton3";
            this._Pattern2Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern3Button
            // 
            this._Pattern3Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern3Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern3Button.Image")));
            this._Pattern3Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern3Button.Name = "_Pattern3Button";
            this._Pattern3Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern3Button.Tag = "3";
            this._Pattern3Button.Text = "toolStripButton4";
            this._Pattern3Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern4Button
            // 
            this._Pattern4Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern4Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern4Button.Image")));
            this._Pattern4Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern4Button.Name = "_Pattern4Button";
            this._Pattern4Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern4Button.Tag = "4";
            this._Pattern4Button.Text = "toolStripButton5";
            this._Pattern4Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern5Button
            // 
            this._Pattern5Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern5Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern5Button.Image")));
            this._Pattern5Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern5Button.Name = "_Pattern5Button";
            this._Pattern5Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern5Button.Tag = "5";
            this._Pattern5Button.Text = "toolStripButton6";
            this._Pattern5Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern6Button
            // 
            this._Pattern6Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern6Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern6Button.Image")));
            this._Pattern6Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern6Button.Name = "_Pattern6Button";
            this._Pattern6Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern6Button.Tag = "6";
            this._Pattern6Button.Text = "toolStripButton7";
            this._Pattern6Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern8Button
            // 
            this._Pattern8Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern8Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern8Button.Image")));
            this._Pattern8Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern8Button.Name = "_Pattern8Button";
            this._Pattern8Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern8Button.Tag = "7";
            this._Pattern8Button.Text = "toolStripButton9";
            this._Pattern8Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _Pattern7Button
            // 
            this._Pattern7Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._Pattern7Button.Image = ((System.Drawing.Image)(resources.GetObject("_Pattern7Button.Image")));
            this._Pattern7Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._Pattern7Button.Name = "_Pattern7Button";
            this._Pattern7Button.Size = new System.Drawing.Size(21, 20);
            this._Pattern7Button.Tag = "8";
            this._Pattern7Button.Text = "toolStripButton8";
            this._Pattern7Button.Click += new System.EventHandler(this._PatternButton_Click);
            // 
            // _TileGrid
            // 
            this._TileGrid.BackColor = System.Drawing.Color.White;
            this._TileGrid.CellSize = 18;
            this._TileGrid.Chunk = null;
            this._TileGrid.GridHeight = 12;
            this._TileGrid.GridWidth = 6;
            this._TileGrid.Location = new System.Drawing.Point(27, 12);
            this._TileGrid.Name = "_TileGrid";
            this._TileGrid.Selection = new System.Drawing.Rectangle(0, 0, 0, 0);
            this._TileGrid.SelectionMode = false;
            this._TileGrid.Size = new System.Drawing.Size(109, 217);
            this._TileGrid.TabIndex = 14;
            // 
            // _OnColour
            // 
            this._OnColour.BackColor = System.Drawing.SystemColors.Control;
            this._OnColour.Index = 0;
            this._OnColour.Location = new System.Drawing.Point(217, 35);
            this._OnColour.Name = "_OnColour";
            this._OnColour.Palette = null;
            this._OnColour.ReadOnly = true;
            this._OnColour.Selected = false;
            this._OnColour.Size = new System.Drawing.Size(27, 33);
            this._OnColour.TabIndex = 8;
            // 
            // _OffColour
            // 
            this._OffColour.BackColor = System.Drawing.SystemColors.Control;
            this._OffColour.Index = 0;
            this._OffColour.Location = new System.Drawing.Point(217, 6);
            this._OffColour.Name = "_OffColour";
            this._OffColour.Palette = null;
            this._OffColour.ReadOnly = true;
            this._OffColour.Selected = false;
            this._OffColour.Size = new System.Drawing.Size(27, 33);
            this._OffColour.TabIndex = 8;
            // 
            // TileForm
            // 
            this.AcceptButton = this._OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._CancelButton;
            this.ClientSize = new System.Drawing.Size(355, 243);
            this.ControlBox = false;
            this.Controls.Add(this._NormalRadioButton);
            this.Controls.Add(this._XorRadioButton);
            this.Controls.Add(this._TileGrid);
            this.Controls.Add(this._OnColourLabel);
            this.Controls.Add(this._XNumeric);
            this.Controls.Add(this._XLabel);
            this.Controls.Add(this._YLabel);
            this.Controls.Add(this._YNumeric);
            this.Controls.Add(this._OffColourLabel);
            this.Controls.Add(this._OnColour);
            this.Controls.Add(this._OffColour);
            this.Controls.Add(this._CancelButton);
            this.Controls.Add(this._OKButton);
            this.Controls.Add(this._ToolStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TileForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Tile";
            this.Load += new System.EventHandler(this.TileForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TileForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this._XNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._YNumeric)).EndInit();
            this._ToolStrip.ResumeLayout(false);
            this._ToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _OKButton;
        private Cdg.Controls.ColourControl _OffColour;
        private Cdg.Controls.ColourControl _OnColour;
        private System.Windows.Forms.Label _OffColourLabel;
        private System.Windows.Forms.Label _OnColourLabel;
        private System.Windows.Forms.NumericUpDown _XNumeric;
        private System.Windows.Forms.NumericUpDown _YNumeric;
        private System.Windows.Forms.Label _XLabel;
        private System.Windows.Forms.Label _YLabel;
        private System.Windows.Forms.RadioButton _NormalRadioButton;
        private System.Windows.Forms.RadioButton _XorRadioButton;
        private Cdg.Controls.TileGrid _TileGrid;
        private System.Windows.Forms.ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripButton _Pattern0Button;
        private System.Windows.Forms.ToolStripButton _Pattern1Button;
        private System.Windows.Forms.ToolStripButton _Pattern2Button;
        private System.Windows.Forms.ToolStripButton _Pattern3Button;
        private System.Windows.Forms.ToolStripButton _Pattern4Button;
        private System.Windows.Forms.ToolStripButton _Pattern5Button;
        private System.Windows.Forms.ToolStripButton _Pattern6Button;
        private System.Windows.Forms.ToolStripButton _Pattern7Button;
        private System.Windows.Forms.ToolStripButton _Pattern8Button;
    }
}