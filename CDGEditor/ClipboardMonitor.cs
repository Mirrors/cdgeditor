﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;

namespace CDGEditor
{
    /// <summary>
    /// Clipboard monitor control (based on code from http://stackoverflow.com/questions/621577/clipboard-event-c)
    /// </summary>
    [DefaultEvent("ClipboardChanged")]
    public partial class ClipboardMonitor : UserControl
    {
        #region Construction

        public ClipboardMonitor()
        {
            InitializeComponent();

            this.BackColor = Color.Red;
            this.Visible = false;

            _NextClipboardViewer = (IntPtr)SetClipboardViewer((int)this.Handle);
        }

        #endregion

        #region Events

        /// <summary>
        /// Clipboard contents changed.
        /// </summary>
        public event EventHandler<ClipboardChangedEventArgs> ClipboardChanged;

        #endregion

        #region Overrides

        /// <summary>
        /// Overridden to sniff the clipboard windows event messages.
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            // defined in winuser.h
            const int WM_DRAWCLIPBOARD = 0x308;
            const int WM_CHANGECBCHAIN = 0x030D;

            switch (m.Msg)
            {
                case WM_DRAWCLIPBOARD:
                RaiseClipboardChanged();
                SendMessage(_NextClipboardViewer, m.Msg, m.WParam, m.LParam);
                break;

                case WM_CHANGECBCHAIN:
                if (m.WParam == _NextClipboardViewer)
                    _NextClipboardViewer = m.LParam;
                else
                    SendMessage(_NextClipboardViewer, m.Msg, m.WParam, m.LParam);
                break;

                default:
                base.WndProc(ref m);
                break;
            }
        }

        #endregion

        #region Data

        IntPtr _NextClipboardViewer;

        #endregion

        #region User32.dll P/Invoke Declarations

        [DllImport("User32.dll")]
        protected static extern int SetClipboardViewer(int hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

        #endregion

        #region Private Methods

        void RaiseClipboardChanged()
        {
            try
            {
                IDataObject iData = Clipboard.GetDataObject();
                if (ClipboardChanged != null)
                {
                    ClipboardChanged(this, new ClipboardChangedEventArgs(iData));
                }

            }
            catch (Exception e)
            {
                // Swallow or pop-up, not sure
                // Trace.Write(e.ToString());
                MessageBox.Show(e.ToString());
            }
        }

        #endregion
    }

    /// <summary>
    /// Event arguments for the clipboard changed event.
    /// </summary>
    public class ClipboardChangedEventArgs : EventArgs
    {
        public readonly IDataObject DataObject;

        public ClipboardChangedEventArgs(IDataObject dataObject)
        {
            DataObject = dataObject;
        }
    }
}

