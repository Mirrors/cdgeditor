﻿namespace CDGEditor
{
    partial class RecoverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._CancelButton = new System.Windows.Forms.Button();
            this._OKButton = new System.Windows.Forms.Button();
            this._InstructionLabel = new System.Windows.Forms.Label();
            this._InstructionComboBox = new System.Windows.Forms.ComboBox();
            this._ShowingLabel = new System.Windows.Forms.Label();
            this._TimeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._GotoButton = new System.Windows.Forms.Button();
            this._ChunkList = new BrightIdeasSoftware.ObjectListView();
            this._TimeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._CommandColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._IndexColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this._ChunkList)).BeginInit();
            this.SuspendLayout();
            // 
            // _CancelButton
            // 
            this._CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._CancelButton.Location = new System.Drawing.Point(324, 41);
            this._CancelButton.Name = "_CancelButton";
            this._CancelButton.Size = new System.Drawing.Size(75, 23);
            this._CancelButton.TabIndex = 5;
            this._CancelButton.Text = "&Cancel";
            this._CancelButton.UseVisualStyleBackColor = true;
            // 
            // _OKButton
            // 
            this._OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._OKButton.Location = new System.Drawing.Point(324, 12);
            this._OKButton.Name = "_OKButton";
            this._OKButton.Size = new System.Drawing.Size(75, 23);
            this._OKButton.TabIndex = 4;
            this._OKButton.Text = "&OK";
            this._OKButton.UseVisualStyleBackColor = true;
            // 
            // _InstructionLabel
            // 
            this._InstructionLabel.AutoSize = true;
            this._InstructionLabel.Location = new System.Drawing.Point(12, 254);
            this._InstructionLabel.Name = "_InstructionLabel";
            this._InstructionLabel.Size = new System.Drawing.Size(92, 13);
            this._InstructionLabel.TabIndex = 2;
            this._InstructionLabel.Text = "Select Instruction:";
            // 
            // _InstructionComboBox
            // 
            this._InstructionComboBox.FormattingEnabled = true;
            this._InstructionComboBox.Location = new System.Drawing.Point(110, 253);
            this._InstructionComboBox.Name = "_InstructionComboBox";
            this._InstructionComboBox.Size = new System.Drawing.Size(208, 21);
            this._InstructionComboBox.TabIndex = 3;
            // 
            // _ShowingLabel
            // 
            this._ShowingLabel.AutoSize = true;
            this._ShowingLabel.Location = new System.Drawing.Point(12, 17);
            this._ShowingLabel.Name = "_ShowingLabel";
            this._ShowingLabel.Size = new System.Drawing.Size(179, 13);
            this._ShowingLabel.TabIndex = 6;
            this._ShowingLabel.Text = "Showing: n chunks before and after:";
            // 
            // _TimeTextBox
            // 
            this._TimeTextBox.Location = new System.Drawing.Point(197, 15);
            this._TimeTextBox.Mask = "##:##:###";
            this._TimeTextBox.Name = "_TimeTextBox";
            this._TimeTextBox.Size = new System.Drawing.Size(61, 20);
            this._TimeTextBox.TabIndex = 7;
            this._TimeTextBox.Text = "0000000";
            // 
            // _GotoButton
            // 
            this._GotoButton.Location = new System.Drawing.Point(264, 12);
            this._GotoButton.Name = "_GotoButton";
            this._GotoButton.Size = new System.Drawing.Size(40, 23);
            this._GotoButton.TabIndex = 5;
            this._GotoButton.Text = "&Goto";
            this._GotoButton.UseVisualStyleBackColor = true;
            this._GotoButton.Click += new System.EventHandler(this._GotoButton_Click);
            // 
            // _ChunkList
            // 
            this._ChunkList.AllColumns.Add(this._TimeColumn);
            this._ChunkList.AllColumns.Add(this._CommandColumn);
            this._ChunkList.AllColumns.Add(this._IndexColumn);
            this._ChunkList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._ChunkList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._TimeColumn,
            this._CommandColumn,
            this._IndexColumn});
            this._ChunkList.FullRowSelect = true;
            this._ChunkList.Location = new System.Drawing.Point(4, 41);
            this._ChunkList.Name = "_ChunkList";
            this._ChunkList.ShowGroups = false;
            this._ChunkList.Size = new System.Drawing.Size(314, 201);
            this._ChunkList.TabIndex = 1;
            this._ChunkList.UseCompatibleStateImageBehavior = false;
            this._ChunkList.View = System.Windows.Forms.View.Details;
            this._ChunkList.SelectionChanged += new System.EventHandler(this._ChunkList_SelectionChanged);
            this._ChunkList.DoubleClick += new System.EventHandler(this._ChunkList_DoubleClick);
            // 
            // _TimeColumn
            // 
            this._TimeColumn.AspectName = "Time";
            this._TimeColumn.Text = "Time";
            // 
            // _CommandColumn
            // 
            this._CommandColumn.AspectName = "Instruction";
            this._CommandColumn.FillsFreeSpace = true;
            this._CommandColumn.Text = "Command";
            // 
            // _IndexColumn
            // 
            this._IndexColumn.AspectName = "Index";
            this._IndexColumn.Text = "Index";
            // 
            // RecoverForm
            // 
            this.AcceptButton = this._OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._CancelButton;
            this.ClientSize = new System.Drawing.Size(401, 282);
            this.ControlBox = false;
            this.Controls.Add(this._TimeTextBox);
            this.Controls.Add(this._ShowingLabel);
            this.Controls.Add(this._InstructionComboBox);
            this.Controls.Add(this._InstructionLabel);
            this.Controls.Add(this._GotoButton);
            this.Controls.Add(this._CancelButton);
            this.Controls.Add(this._OKButton);
            this.Controls.Add(this._ChunkList);
            this.Name = "RecoverForm";
            this.ShowInTaskbar = false;
            this.Text = "Recover Chunk";
            ((System.ComponentModel.ISupportInitialize)(this._ChunkList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView _ChunkList;
        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _OKButton;
        private System.Windows.Forms.Label _InstructionLabel;
        private System.Windows.Forms.ComboBox _InstructionComboBox;
        private BrightIdeasSoftware.OLVColumn _TimeColumn;
        private BrightIdeasSoftware.OLVColumn _CommandColumn;
        private BrightIdeasSoftware.OLVColumn _IndexColumn;
        private System.Windows.Forms.Label _ShowingLabel;
        private System.Windows.Forms.MaskedTextBox _TimeTextBox;
        private System.Windows.Forms.Button _GotoButton;
    }
}