﻿using System.Collections.Generic;
using Cdg.Chunks;

namespace Cdg
{
    public class CdgChunkEnumerator : IEnumerator<Chunk>
    {
        public CdgChunkEnumerator(List<Chunk> chunks)
        {
            _chunks = chunks;            
        }

        private readonly List<Chunk> _chunks;
        int _Index;

        int FindCDGChunk(int start)
        {
            int index = start;
            while (index < _chunks.Count)
            {
                if (_chunks[index].CDG)
                {
                    break;
                }
                index++;
            }

            return index;
        }

        #region IEnumerator Members

        public object Current
        {
            get
            {
                return _chunks[_Index];
            }
        }

        public bool MoveNext()
        {
            _Index = FindCDGChunk(_Index + 1);
            return _Index < _chunks.Count;
        }

        public void Reset()
        {
            _Index = FindCDGChunk(0);
        }

        #endregion

        #region IEnumerator<Chunk> Members

        Chunk IEnumerator<Chunk>.Current
        {
            get 
            {
                return _chunks[_Index];
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}
