﻿using System.Collections.Generic;

namespace Cdg
{
    /// <summary>
    /// Class to encapsulate a collection of pixels.
    /// </summary>
    public class Pixels
    {
        #region Construction

        public Pixels(System.Drawing.Size size)
        {
            NullPixels = false;
            CreatePixels(size);
        }

        #endregion

        #region Properties

        public System.Drawing.Size Size
        {
            get
            {
                return new System.Drawing.Size(_rows[0].Count, _rows.Count);
            }
            set
            {
                if (value.Width != _rows[0].Count || value.Height != _rows.Count)
                {
                    CreatePixels(value);
                }
            }
        }

        public bool NullPixels { get; set; }

        #endregion

        #region Public Methods

        public bool Get(System.Drawing.Point location)
        {
            return _rows[location.Y].Get(location.X);
        }

        public void Set(System.Drawing.Point location, bool on)
        {
            _rows[location.Y].Set(location.X, on);
        }

        public System.Collections.BitArray GetRow(int row)
        {
            return _rows[row];
        }

        #endregion

        #region Private Methods

        void CreatePixels(System.Drawing.Size size)
        {
            // Keep original rows
            var originalRows = _rows;

            // Create new pixel storage
            _rows = new List<System.Collections.BitArray>(size.Height);
            for (var row = 0; row < size.Height; row++)
            {
                if (originalRows != null && size.Width == originalRows[0].Count
                    && row < originalRows.Count)
                {
                    _rows.Add(originalRows[row]);
                }
                else
                {
                    _rows.Add(new System.Collections.BitArray(size.Width));

                    if (originalRows != null && row < originalRows.Count)
                    {
                        // Copy pixels from original
                        for (var x = 0; x < originalRows[0].Count
                            && x < size.Width; x++)
                        {
                            _rows[row].Set(x, originalRows[row].Get(x));
                        }
                    }
                }
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// List of BitArray objects containing the pixel data.
        /// Each element in the list is a row of pixels.
        /// </summary>
        private List<System.Collections.BitArray> _rows;

        #endregion
    }
}
