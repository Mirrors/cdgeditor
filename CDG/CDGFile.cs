using System;
using System.Collections.Generic;
using System.IO;
using Cdg.Chunks;
using Cdg.Validation;

namespace Cdg
{
	/// <summary>
	/// Encapsulates a CDG file
	/// </summary>
	public class CdgFile
    {
        #region Constants

        private const byte CdgCommand = 9;

        // CDs played at standard speed are played at 75 sectors per second.
        private const int SectorsPerSecond = 75;

        // Each sector is 96 bytes long, containing 4 packets of 24 CDG data bytes
        // that this application refers to as Chunks.
        private const int ChunksPerSector = 4;
        public const int ChunksPerSecond = SectorsPerSecond * ChunksPerSector;
        public const int ChunksPerMinute = ChunksPerSecond * 60;

        #endregion

        #region Data

        private readonly Rules _rules = new Rules();

		#endregion

		#region Construction

		public CdgFile()
		{
			_chunks = new List<Chunk>();
            _cdgChunks = new List<Chunk>();

            _rules.File = this;

            _fileInfo = new FileInfo("Untitled.cdg");

            // Insert 10 seconds worth of Chunks
            InsertChunks(0, TimeSpanToChunkCount(new TimeSpan(0, 0, 10)));
		}        

		#endregion

        #region Public Properties

        /// <summary>
        /// Gets access to all the sub-channel chunks including non CDG chunks
        /// </summary>
        public List<Chunk> Chunks
        {
            get
            {
                return _chunks;
            }
        }

        /// <summary>
        /// Gets access to all the CDG sub-channel chunks
        /// </summary>
        public List<Chunk> CdgChunks
        {
            get
            {
                return _cdgChunks;
            }
        }

        /// <summary>
        /// Gets the file path information.
        /// </summary>
        public FileInfo Info
        {
            get
            {
                return _fileInfo;
            }
        }

        /// <summary>
        /// Gets the errors detected.
        /// </summary>
        public IEnumerable<Result> Errors
        {
            get
            {
                return _errors;
            }
        }

        /// <summary>
        /// Gets the full path to the repair file.
        /// </summary>
        private string RepairPath
        {
            get
            {
                return _fileInfo != null ? string.Format("{0}.repair", _fileInfo.FullName) : null;
            }
        }      

        #endregion

        #region Time to Chunk Conversions

        /// <summary>
        /// Converts a time span into a number of chunks required to fill it.
        /// </summary>
        /// <param name="span">The span to convert.</param>
        /// <returns>The number of chunks to fill the specified time span.</returns>
        static int TimeSpanToChunkCount(TimeSpan span)
        {
            var totalSeconds = span.TotalSeconds;
            var result = (int)(totalSeconds * ChunksPerSecond);
            return result;
        }

        #endregion

        #region Public Methods

        #region I/O

        /// <summary>
        /// Loads the file from disk.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>True if validation has chaged the file.</returns>
        public bool Load(string path)
		{
			_chunks.Clear();
            _cdgChunks.Clear();

            using (var stream = File.Open(path,
                FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                _fileInfo = new FileInfo(path);

                var index = 0;
                var chunk = Chunk.Read(stream);
                chunk.Index = index++;

                while (chunk != null)
                {
                    _chunks.Add(chunk);
                    if (chunk.Cdg)
                    {
                        _cdgChunks.Add(chunk);
                    }

                    chunk = Chunk.Read(stream);
                    if (chunk != null)
                    {
                        chunk.Index = index++;
                    }
                }

                _position = -1;

                stream.Close();
            }

		    if (!File.Exists(RepairPath)) return false;
		    var resultsFile = new ResultsFile();
		    resultsFile.Load(RepairPath, this);

		    _errors = resultsFile.Results;

		    return false;
        }

        public void Save()
        {
            var resultsFile = new ResultsFile(_errors);
            resultsFile.Save(RepairPath);

            Save(_fileInfo.FullName);
        }

        public void Save(string path)
        {            
            File.Delete(path);

            using (var stream = File.Open(path,
                FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                // Write all the chunks
                foreach (var chunk in _chunks)
                {
                    chunk.Write(stream);
                }
            }
        }

        #endregion

        #region Navigation

        public void GotoChunk(int position, CdgBitmap bitmap, bool forceRedraw = false)
        {
            if (position < 0 || position > _chunks.Count)
            {
                throw new IndexOutOfRangeException();
            }

            // If requested position is the next CDG instruction - just play it

            // Otherwise, scan back from requested position to the first Memory Preset
            // or the beginning of the file, or the current playback position (whichever 
            // is found first) and execute from that point up to and including the requested 
            // chunk.

            // If we went all the way to the beginning without finding a memory preset,
            // clear the bitmap.

            if (!forceRedraw && position == NextCdgInstruction())
            {
                _position = position;
                var chunk = _chunks[_position];
                if (chunk.Cdg)
                {
                    chunk.Execute(bitmap);
                }
            }
            else
            {
                var firstMemoryPreset = FindPrevious(Chunk.InstructionType.MemoryPreset, position - 1);

                if (firstMemoryPreset == 0)
                {
                    bitmap.Clear();
                }
                
                for (var index = firstMemoryPreset; index <= position; index++)
                {
                    var chunk = _chunks[index];
                    if (chunk.Cdg)
                    {
                        chunk.Execute(bitmap);
                    }
                }
                _position = position;
            }      

            // Re-apply border and palette too
            var border = FindPrevious(Chunk.InstructionType.BorderPreset, _position);
            if (border > 0)
            {
                _chunks[border].Execute(bitmap);
            }
            else
            {
                bitmap.BorderColour = 0;
            }

            var palette = FindPrevious(Chunk.InstructionType.LoadColTableLow, _position);
            if (palette > 0)
            {
                _chunks[palette].Execute(bitmap);
            }

            palette = FindPrevious(Chunk.InstructionType.LoadColTableHigh, _position);
            if (palette > 0)
            {
                _chunks[palette].Execute(bitmap);
            }
        }

        /// <summary>
        /// Finds the next memory preset CDG chunk
        /// </summary>
        /// <returns>The chunk index where it was found.</returns>
        public int FindNext(Chunk.InstructionType type)
        {
            var found = false;
            
            var result = _position;
            if (_position < 0)
            {
                result = 0;
            }

            while (result < _chunks.Count - 1)
            {
                var chunk = _chunks[result];
                if (chunk.Type == type)
                {
                    if (chunk.Type == Chunk.InstructionType.MemoryPreset)
                    {
                        var memoryChunk = (chunk as MemoryPreset);
                        if (memoryChunk != null && memoryChunk.Repeat == 0)
                        {
                            found = true;
                            break;
                        }
                    }
                    else
                    {
                        found = true;
                        break;
                    }
                }
                result++;
            }

            if (!found)
            {
                result = -1;
            }

            return result;
        }

        public int FindPrevious(Chunk.InstructionType type, int startPosition)
        {
            var result = startPosition;
            if (startPosition > 0)
            {
                while (result > 0)
                {
                    var chunk = _chunks[result];
                    if (chunk.Type == type)
                    {
                        if (chunk.Type == Chunk.InstructionType.MemoryPreset)
                        {
                            var memoryChunk = (chunk as MemoryPreset);
                            if (memoryChunk != null && memoryChunk.Repeat == 0)
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    result--;
                }
            }
            else
            {
                result = 0;
            }
            return result;
        }

        public int FindPreviousTileAtLocation(int x, int y)
        {
            var result = -1;
            // Search back for the last tile block which matches the
            // coordinates supplied until we find one or a memory preset.

            var searchPos = _position - 1;
            
            while(searchPos > 0)
            {
                var chunk = _chunks[searchPos];
                if (chunk.Cdg && chunk.Type == Chunk.InstructionType.MemoryPreset)
                {
                    break;
                }
                if (chunk.Cdg && chunk is TileBlock)
                {
                    var tile = chunk as TileBlock;
                    if (tile.Column == x && tile.Row == y)
                    {
                        result = searchPos;
                        break;
                    }
                }

                searchPos--;
            }
            return result;
        }

        #endregion

        #region Chunk Manipulation

        /// <summary>
        /// Inserts chunks into the file.
        /// </summary>
        /// <param name="startIndex">Index to insert chunks at.</param>
        /// <param name="count">Number of chunks to insert.</param>
        public void InsertChunks(int startIndex, int count)
        {
            for (var index = 0; index < count; index++)
            {
                _chunks.Insert(startIndex + index, new Chunk(startIndex + index));
            }

            // Re-index chunks after
            for (var index = startIndex + count; index < _chunks.Count; index++ )
            {
                _chunks[index].Index = index;
            }
        }

        /// <summary>
        /// Changes the type of the chunk at the specified index in the _CDGChunks
        /// array.
        /// </summary>
        /// <param name="cdgIndex">Display index of the chunk to change</param>
        /// <param name="type">New type to create</param>
        public void ChangeChunkType(int cdgIndex, Chunk.InstructionType type)
        {
            var existingChunk = _cdgChunks[cdgIndex];

            if (!existingChunk.Cdg)
            {
                throw new InvalidOperationException(
                    "Use RecoverChunk to change to/from an unknown chunk type");
            }
 
            if (existingChunk.Type != type && existingChunk.Index.HasValue)
            {
                var newChunk = Chunk.Create(type, existingChunk.Data);

                newChunk.Data[1] = (byte)type;
                newChunk.Index = existingChunk.Index;
                _cdgChunks[cdgIndex] = newChunk;
                _chunks[existingChunk.Index.Value] = newChunk;
            }
        }
    
        /// <summary>
        /// Changes a TimeGap chunk into a CDG chunk or vice-versa
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="newType"></param>
        public void RecoverChunk(Chunk chunk, Chunk.InstructionType newType)
        {
            if (chunk.Cdg)
            {
                if (chunk.Type == Chunk.InstructionType.Unknown)
                {
                    ChangeChunkType(_cdgChunks.IndexOf(chunk), newType);
                }
                else
                {
                    throw new InvalidOperationException(
                        "Use ChangeChunkType to change a CDG instruction");
                }
            }

            if (chunk.Cdg || chunk.Type == newType || !chunk.Index.HasValue) return;
            
            // Create a new type of chunk
            var newChunk = Chunk.Create(newType, chunk.Data);

            // Replace the original chunk with the chunk of the new type
            newChunk.Data[0] = CdgCommand;
            newChunk.Data[1] = (byte)newType;
            newChunk.Index = chunk.Index;
            _chunks[chunk.Index.Value] = newChunk;

            // Insert the chunk into the CDG Chunk list
                
            // Find next CDG Chunk
            int index;
            for (index = newChunk.Index.Value + 1; index < _chunks.Count; index++)
            {
                var searchChunk = _chunks[index];
                if (searchChunk.Cdg)
                {
                    break;
                }
            }

            if (index < _chunks.Count)
            {
                var cdgIndex = _cdgChunks.IndexOf(_chunks[index]);

                _cdgChunks.Insert(cdgIndex, newChunk);
                
                if (newChunk is TileBlock)
                {
                    var nextChunk = _chunks[index];
                    if (nextChunk is TileBlock)
                    {
                        (newChunk as TileBlock).Row = (nextChunk as TileBlock).Row + 1;
                        (newChunk as TileBlock).Column = (nextChunk as TileBlock).Column;
                    }
                }
            }
            else
            {
                _cdgChunks.Add(newChunk);
            }
        }

        public void DeleteChunk(int index)
        {
            var chunk = _chunks[index];
            chunk.Data[Chunk.CdgCommandOffset] = 0x00;
            _cdgChunks.Remove(chunk);
        }

        #endregion
        
        #region Validation

        /// <summary>
        /// Validates the CDG file.
        /// </summary>    
        /// <returns>True if file is modified.</returns>
        public bool Validate()
        {
            var modified = false;

            //TODO: Validation may take a while - provide events to allow UI to show progress.

            // Create a bitmap to render to
            var bitmap = new CdgBitmap();

            _errors = new List<Result>();

            // Get each chunk to validate itself (invalid values etc)
            foreach (var chunk in CdgChunks)
            {
                var errors = chunk.Validate(_rules);
                if (errors != null && errors.Count > 0)
                {
                    _errors.AddRange(errors);
                    modified = true;
                }

                if (chunk.Type == Chunk.InstructionType.MemoryPreset)
                {
                }

                chunk.Execute(bitmap);
            }
            return modified;
        }

        #endregion

        #endregion

        #region Private Methods

        /// <summary>
        /// Locates the next CDG instruction chunk.
        /// </summary>
        /// <returns>The index of the next CDG instruction chunk.</returns>
        private int NextCdgInstruction()
        {
            var result = 0;

            var position = _position;
            while (position < _chunks.Count - 1)
            {
                if (_chunks[++position].Cdg)
                {
                    result = position;
                    break;
                }
            }

            return result;
        }

        #endregion

        #region Data

        /// <summary>
        /// Storage of the all subchannel data chunks.
        /// </summary>
        private readonly List<Chunk> _chunks;

        /// <summary>
        /// Storage of the CDG subchannel data chunks (subset of _Chunks)
        /// </summary>
        private readonly List<Chunk> _cdgChunks;

        /// <summary>
        /// Storage for errors found.
        /// </summary>
        private List<Result> _errors;

        /// <summary>
        /// Current playback position
        /// </summary>
        private int _position;

        /// <summary>
        /// Current file.
        /// </summary>
        private FileInfo _fileInfo;       

		#endregion

        public void RebuildCdgChunkList()
        {
            _cdgChunks.Clear();
            foreach(var chunk in _chunks)
            {
                if (chunk.Cdg)
                {
                    _cdgChunks.Add(chunk);
                }
            }
        }
    }
}
