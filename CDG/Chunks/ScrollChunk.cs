﻿namespace Cdg.Chunks
{
    /// <summary>
    /// CDG Instruction to scroll the screen.
    /// </summary>
    /// <remarks>
    /// The CDG specification allows the entire screen to be shifted, up to 
    /// 5 pixels right and 11 pixels down.  This shift is persistent
    /// until it is reset to a different value.  In practice, this is used in 
    /// conjunction with scrolling (which always jumps in integer blocks of 6x12 
    /// pixels) to perform one-pixel-at-a-time scrolls.
    /// </remarks>
    public class ScrollChunk : Chunk
    {
        #region Constants

        // Byte offset to the HScroll value in the CDG Data bytes
        private const int ScrollHscroll = 1;

        // Byte offset to the VScroll value in the CDG Data bytes
        private const int ScrollVscroll = 2;

        /// <summary>
        /// The Horizontal Scroll instruction (or SCmd which is the
        /// hScroll byte & 0x30 >> 4)
        /// </summary>
        public enum HScrollInstruction
        {
            None,
            Right,  // 6 Pixels right
            Left    // 6 Pixels left
        }

        /// <summary>
        /// The Vertical Scroll instruction (or SCmd which is the
        /// hScroll byte & 0x30 >> 4)
        /// </summary>
        public enum VScrollInstruction
        {
            None,
            Down,   // 12 pixels down
            Up     // 12 pixels up
        }

        #endregion

        #region Construction

        protected ScrollChunk(byte[] data)
            : base(data)
        {
            var hScrollByte = GetDataByte(ScrollHscroll);
            var vScrollByte = GetDataByte(ScrollVscroll);

            // Scroll command is XXnn bits of the first nibble
            _horizontalScrollInstruction = (HScrollInstruction)((hScrollByte & 0x30) >> 4);
            _verticalScrollInstruction = (VScrollInstruction)((vScrollByte & 0x30) >> 4);

            // Scroll offset is the second nibble
            // Horizontal scroll offset is always pixels to the left.
            _horizontalScrollOffset = hScrollByte & 0x0F;
            _verticalScrollOffset = vScrollByte & 0x0F;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Horizontal display offset.  Valid values are 0 to 5, with values > 0
        /// shifting the display left by that many CDG pixels.
        /// </summary>
        public int? HorizontalScrollOffset
        {
            get
            {
                return _horizontalScrollOffset;
            }
            set
            {
                _horizontalScrollOffset = value;

                if (_horizontalScrollOffset.HasValue)
                {
                    var dataByte = GetDataByte(ScrollHscroll);

                    // Keep the instruction in the top nibble (only care about bottom two bits
                    // though) and place the horizontal offset in the bottom nibble (this time
                    // we only care about the bottom 3 bits of the value).
                    dataByte = (byte)((dataByte & 0x30) | (_horizontalScrollOffset & 0x07));
                    SetDataByte(ScrollHscroll, dataByte);
                }
            }
        }

        /// <summary>
        /// Vertical display offset.  Valid values are 0 to 11, with values > 0
        /// shifting the display up by that many CDG pixels.
        /// </summary>
        public int? VerticalScrollOffset
        {
            get
            {
                return _verticalScrollOffset;
            }
            set
            {
                _verticalScrollOffset = value;

                if (_verticalScrollOffset.HasValue)
                {
                    // Keep the instruction in the top nibble (only care about bottom two bits
                    // though) and place the horizontal offset in the bottom nibble (this time
                    // we only care about the bottom 4 bits of the value).
                    var dataByte = GetDataByte(ScrollVscroll);
                    dataByte = (byte)((dataByte & 0x30) | (_verticalScrollOffset & 0x0F));
                    SetDataByte(ScrollVscroll, dataByte);
                }
            }
        }      

        /// <summary>
        /// Horizontal scroll instruction - moves the CDG pixel data horizontally
        /// by 6 pixels (one tile) left or right.
        /// </summary>
        public HScrollInstruction? HorizontalScrollInstruction
        {
            get
            {
                return _horizontalScrollInstruction;
            }
            set
            {
                _horizontalScrollInstruction = value;

                if (_horizontalScrollInstruction.HasValue)
                {
                    // Place the horizontal instruction in the top nibble (we only care 
                    // about the bottom 2 bits of the value) and keep the offset in the 
                    // bottom nibble (only care about bottom three bits though).
                    var dataByte = GetDataByte(ScrollHscroll);
                    dataByte = (byte)((((byte)_horizontalScrollInstruction & 0x03) << 4) | dataByte & 0x07);
                    SetDataByte(ScrollHscroll, dataByte);
                }
            }
        }

        /// <summary>
        /// Vertical scroll instruction - moves the CDG pixel data vertically
        /// by 12 pixels (one tile) up or down.
        /// </summary>
        public VScrollInstruction? VerticalScrollInstruction
        {
            get
            {
                return _verticalScrollInstruction;
            }
            set
            {
                _verticalScrollInstruction = value;

                if (_verticalScrollInstruction.HasValue)
                {
                    // Place the vertical instruction in the top nibble (we only care 
                    // about the bottom 2 bits of the value) and keep the offset in the 
                    // bottom nibble (only care about bottom four bits though).
                    var dataByte = GetDataByte(ScrollVscroll);
                    dataByte = (byte)((((byte)_verticalScrollInstruction & 0x03) << 4) | dataByte & 0x0F);
                    SetDataByte(ScrollVscroll, dataByte);
                }
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Horizontal display offset.  Valid values are 0 to 5, with values > 0
        /// shifting the display left by that many CDG pixels.
        /// </summary>
        private int? _horizontalScrollOffset;

        /// <summary>
        /// Vertical display offset.  Valid values are 0 to 11, with values > 0
        /// shifting the display up by that many CDG pixels.
        /// </summary>
        private int? _verticalScrollOffset;

        /// <summary>
        /// Instruction to scroll the pixel data horizontally.
        /// </summary>
        private HScrollInstruction? _horizontalScrollInstruction;
        
        /// <summary>
        /// Instruction to scroll the pixel data vertically.
        /// </summary>
        private VScrollInstruction? _verticalScrollInstruction;

        #endregion
    }
}
