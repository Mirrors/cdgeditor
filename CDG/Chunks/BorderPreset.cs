namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a  Border Preset CDG Chunk..
	/// </summary>
	public class BorderPreset : Chunk
	{
	    private const int ColourByteOffset = 0;

		#region Construction

        public BorderPreset(byte[] data)
            : base(data)
		{
            _colour = GetDataByte(ColourByteOffset) & 0xF;
		}

		#endregion

        #region Public Properties

        public int? Colour
        {
            get
            {
                return _colour;
            }
            set
            {
                _colour = value;
                if (value.HasValue)
                {
                    SetDataByte(ColourByteOffset, (byte)value);
                }
                //RaiseChanged();
            }
        }

        #endregion

        #region Public Methods

        public override void Execute(CdgBitmap bitmap)
        {
            base.Execute(bitmap);

            int colourIndex = GetDataByte(ColourByteOffset) & 0xF;

            bitmap.BorderColour = colourIndex;
        }

        #endregion

		#region Object Overrides

		public override string ToString()
		{
			return "Border Preset";
		}

		#endregion

        #region Data

        /// <summary>
        /// Border command's colour index value.
        /// </summary>
        int? _colour;

        #endregion
    }
}
