using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a CDG data chunk - 24 bytes of raw data
	/// </summary>
	public class Chunk : ICloneable
	{
		#region Constants

	    private const int MillisecondsPerSecond = 1000;

        #region CDG

	    private const int CdgCommand = 0x09;               // Command byte to identify a CDG subcode
	    private const int CdgChunkSize = 24;               // Size of a chunk
	    private const int CdgDataSize = 16;                // Size of the data section

        public const int CdgCommandOffset = 0;             // Command byte
	    private const int CdgInstructionOffset = 1;        // Instruction byte
	    private const int CdgDataOffset = 4;               // Start of data bytes (16)

        #endregion


        public enum InstructionType
		{
			Unknown = 0,            // 0x00
			MemoryPreset = 1,       // 0x01
			BorderPreset = 2,       // 0x02
			TileNormal = 6,         // 0x06
			ScrollPreset = 20,      // 0x14
			ScrollCopy = 24,        // 0x18
			DefTransColor = 28,     // 0x1C
			LoadColTableLow = 30,   // 0x1E
			LoadColTableHigh = 31,  // 0x1F
			TileXor = 38,           // 0x26
		};

		#endregion       

		#region Construction

		/// <summary>
		/// Initialises a new instance of <see cref="Chunk"/>.
		/// </summary>
        /// <param name="data">The data read from the file (in raw CDG format)</param>
        /// chunk.
		protected Chunk(byte[] data)
		{
			Data = data;
            System.Diagnostics.Debug.Assert(data.Length == CdgChunkSize);
        }

        public Chunk(int index)
        {
            Data = new Byte[CdgChunkSize];
            Index = index;
        }

		#endregion

        #region Events

        /*public event EventHandler Changed;
        protected void RaiseChanged()
        {
            if (Changed != null)
            {
                Changed(this, new EventArgs());
            }
        }*/

        #endregion

        #region Public Properties

        public string DataString
        {
            get
            {
                var builder = new StringBuilder();
                foreach (var b in Data)
                {
                    builder.AppendFormat("{0:X2}", b);
                }
                return builder.ToString();
            }
        }

        /// <summary>
        /// Gets the instruction type for the chunk.
        /// </summary>
        public string Instruction
        {
            get
            {
                return ToString();
            }
        }

        /// <summary>
        /// Gets the time position of the chunk
        /// </summary>
        public string Time
        {
            get
            {
                var result = "";
                if (Index.HasValue)
                {
                    var minutePart = Index.Value / CdgFile.ChunksPerMinute;
                    var secondPart = (Index.Value - (minutePart * CdgFile.ChunksPerMinute))
                        / CdgFile.ChunksPerSecond;
                    var msPart = ((Index.Value % CdgFile.ChunksPerSecond) * MillisecondsPerSecond) /
                        CdgFile.ChunksPerSecond;

                    //int ms = ((_Index % 300) * 10) / 3;

                    result = string.Format("{0:00}:{1:00}:{2:000}", minutePart, secondPart, msPart);
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the type of chunk
        /// </summary>
        public InstructionType Type
        {
            get
            {
                var instruction = InstructionType.Unknown;
                if (Cdg && Enum.IsDefined(typeof(InstructionType), Data[CdgInstructionOffset] & 0x3F))
                {
                    instruction = (InstructionType)(Data[1] & 0x3F);
                }
                return instruction;
            }
        }

        /// <summary>
        /// Gets whether the chunk is a CD+G chunk
        /// </summary>
        public bool Cdg
        {
            get
            {
                return Data[CdgCommandOffset] == CdgCommand;
            }
        }

        /// <summary>
        /// Gets the chunk data bytes
        /// </summary>
        internal byte[] Data { get; private set; }

	    /// <summary>
        /// Gets the index of the chunk.
        /// </summary>
        public int? Index { get; set; }

	    #endregion

        #region Public Methods

        #region Display Text

        public static string GetTypeDescription(InstructionType type)
        {
            var data = new byte[CdgChunkSize];
            data[0] = CdgCommand;
            data[1] = (byte)type;
            var chunk = Create(type, data);
            return chunk.Instruction;
        }

        #endregion

        #region Object Factory

        /// <summary>
        /// Creates a chunk of the specified type.
        /// </summary>
        /// <param name="type">Type of chunk to create.</param>
        /// <param name="data">The chunk data (Raw CDG format).</param>
        /// <returns></returns>
        public static Chunk Create(InstructionType type, byte[] data)
        {
            Chunk result = null;

            // Object factory - create an instance of the correct type
            switch (type)
            {
                case InstructionType.Unknown:
                    {
                        result = new Chunk(data);
                        break;
                    }
                case InstructionType.MemoryPreset:
                    {
                        result = new MemoryPreset(data);
                        break;
                    }
                case InstructionType.BorderPreset:
                    {
                        result = new BorderPreset(data);
                        break;
                    }
                case InstructionType.TileNormal:
                case InstructionType.TileXor:
                    {
                        result = new TileBlock(data);
                        break;
                    }
                case InstructionType.ScrollPreset:
                    {
                        result = new ScrollPreset(data);
                        break;
                    }
                case InstructionType.ScrollCopy:
                    {
                        result = new ScrollCopy(data);
                        break;
                    }
                case InstructionType.DefTransColor:
                    {
                        result = new DefineTransparentColour(data);
                        break;
                    }
                case InstructionType.LoadColTableLow:
                case InstructionType.LoadColTableHigh:
                    {
                        data[CdgInstructionOffset] = (byte)type;
                        result = new LoadColourTable(data);
                        break;
                    }
            }

            return result;
        }

        #endregion

        #region I/O

        /// <summary>
		/// Reads a <see cref="Chunk"/> from a file.
		/// </summary>
		/// <param name="stream">FileStream to read the chunk from.</param>
		public static Chunk Read(FileStream stream)
		{
			var result = ReadCdg(stream);
            return result;
        }

        public static Chunk FromDataString(string dataString)
        {
            var data = new byte[CdgChunkSize];
            for (var index = 0; index < CdgChunkSize; index++)
            {
                var subString = dataString.Substring(index * 2, 2);
                data[index] = byte.Parse(subString, System.Globalization.NumberStyles.HexNumber);
            }

            Chunk result;
            if ((data[CdgCommandOffset] & 0x3F) == CdgCommand)
            {
                // Chunk contains CDG data

                // Extract the CDG instruction (note that only the bottom 6 bits
                // are CDG data - the others will be masked off whenever they are used)
                var instruction = InstructionType.Unknown;
                if (Enum.IsDefined(typeof(InstructionType), data[CdgInstructionOffset] & 0x3F))
                {
                    instruction = (InstructionType)(data[1] & 0x3F);
                }

                result = Create(instruction, data);
            }
            else
            {
                // Chunk is a Time Gap (other sub-channel data on the CD)
                result = new Chunk(data);
            }

            return result;
        }

        public void Write(FileStream stream)
        {
            WriteCdg(stream);
        }

        #endregion

        #region Execution

        /// <summary>
        /// Executes the chunk command 
        /// </summary>
        /// <param name="bitmap">The CDG Bitmap to draw on</param>
        public virtual void Execute(CdgBitmap bitmap)
        {
            // Since this base class represents a Time Gap there
            // is nothing to do.
        }

        #endregion

        #region Validation

        public virtual List<Validation.Result> Validate(Validation.Rules rules)
        {
            return null;
        }

        #endregion

        #endregion

        #region Protected Methods

        protected byte GetDataByte(int index)
        {
            if (index < 0 || index >= CdgDataSize)
            {
                throw new IndexOutOfRangeException("Invalid data index - valid values are 0 to 15");
            }

            return (byte)(Data[CdgDataOffset + index] & 0x3F);
        }

        protected void SetDataByte(int index, byte value)
        {
            Data[CdgDataOffset + index] = value;
        }

	    #endregion

        #region Private Methods

        #region CDG I/O

        #region Read

        private static Chunk ReadCdg(Stream stream)
        {
            Chunk result;

   			var data = new byte[CdgChunkSize];
            var bytesRead = stream.Read(data, 0, 1);
            if (1 != bytesRead) return null;
            
            bytesRead = stream.Read(data, 1, CdgChunkSize - 1);
            if (bytesRead == CdgChunkSize - 1)
            {
                if ((data[CdgCommandOffset] & 0x3F) == CdgCommand)
                {
                    // Chunk contains CDG data

                    // Extract the CDG instruction (note that only the bottom 6 bits
                    // are CDG data - the others will be masked off whenever they are used)
                    var instruction = InstructionType.Unknown;
                    if (Enum.IsDefined(typeof(InstructionType), data[CdgInstructionOffset] & 0x3F))
                    {
                        instruction = (InstructionType)(data[1] & 0x3F);
                    }

                    result = Create(instruction, data);
                }
                else
                {
                    // Chunk is a Time Gap (other sub-channel data on the CD)
                    result = new Chunk(data);
                }
            }
            else
            {
                throw new IOException("Unexpected end of file");
            }

            return result;
        }

        #endregion

        #region Write

	    private void WriteCdg(FileStream stream)
        {
            stream.Write(Data, 0, CdgChunkSize);
        }

        #endregion

        #endregion

        #endregion

        #region Object Overrides

        public override string ToString()
        {
            return "Time Gap";
        }

        #endregion

        #region Protected Properties

        protected byte InstructionByte
        {
            get
            {
                return (byte)(Data[CdgInstructionOffset] & 0x3F);
            }
            set
            {
                Data[CdgInstructionOffset] = value;
            }
        }
        
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return Create(Type, (byte[])Data.Clone()); 
        }

        #endregion
    }
}
