using System;
using System.Collections.Generic;
using System.Drawing;
using Cdg.Validation;

namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a Tile Block CDG Chunk
	/// </summary>
    public class TileBlock : Chunk
	{
		#region Constants

		/// <summary>
		/// Normal tile block replaces the pixels directly, XOR tile block 
		/// replaces a given pixel with the XOR of its current colour index
		/// and the foreground/background colours in the command.
		/// </summary>
		public enum TileBlockType
		{
			Normal,
			Xor
		}

	    private const int Colour0Index = 0;
        private const int Colour1Index = 1;
        private const int RowIndex = 2;
        private const int ColumnIndex = 3;
        private const int PixelsIndex = 4;

        public const int TileWidth = 6;
        public const int TileHeight = 12;

        // Tile Validation constants
        private const int MaxColumn = 49;
        private const int MaxRow = 17;
        private const int MaxColour = 15;

        #endregion

        #region Construction

        public TileBlock(byte[] data)
            : base(data)
		{
            _row = GetDataByte(RowIndex);
            _column = GetDataByte(ColumnIndex);
            _offColour  = GetDataByte(Colour0Index) & 0x0F;
            _onColour = GetDataByte(Colour1Index) & 0x0F;

            var instruction = InstructionByte;
            _tileType = instruction == (byte)InstructionType.TileNormal ? 
                TileBlockType.Normal : TileBlockType.Xor;
        }

		#endregion

		#region Public Properties

		public TileBlockType? TileType
		{
			get
			{
                return _tileType;
			}
            set
            {
                _tileType = value;
                if (value == TileBlockType.Xor)
                {
                    InstructionByte = (byte)InstructionType.TileXor;
                }
                else if (value == TileBlockType.Normal)
                {
                    InstructionByte = (byte)InstructionType.TileNormal;
                }
            }
		}

        /// <summary>
        /// Gets or sets whether the pixel data is null - for editing of 
        /// multiple tile block chunks.
        /// </summary>
        public bool NullPixels { get; set; }

	    public int? Row
        {
            get
            {
                return _row;
            }
            set
            {
                _row = value;
                if (value.HasValue)
                {
                    SetDataByte(RowIndex, (byte)value);
                }
            }
        }

        public int? Column
        {
            get
            {
                return _column;
            }
            set
            {
                _column = value;
                if (_column.HasValue)
                {
                    SetDataByte(ColumnIndex, (byte)_column);
                }
            }
        }

        public int? OffColour
        {
            get
            {
                return _offColour;
            }
            set
            {
                _offColour = value;
                if (value.HasValue)
                {
                    SetDataByte(Colour0Index, (byte)(value & 0x0F));
                }
            }
        }

        public int? OnColour
        {
            get
            {
                return _onColour;
            }
            set
            {
                _onColour = value;
                if (value.HasValue)
                {
                    SetDataByte(Colour1Index, (byte)(value & 0x0F));
                }
            }
        }

		#endregion

        #region Public Methods

        #region Data Access

        /// <summary>
        /// Sets the pixel data from another tile block.
        /// </summary>
        /// <param name="from"></param>
        public void SetPixels(TileBlock from)
        {
            for (var rowIndex = 0; rowIndex < TileHeight; rowIndex++)
            {
                SetDataByte(PixelsIndex + rowIndex, from.GetDataByte(PixelsIndex + rowIndex));
            }
        }

        public bool GetPixel(int x, int y)
        {
            if (x < 0 || x > TileWidth - 1 || y < 0 || y > TileHeight - 1)
            {
                throw new IndexOutOfRangeException();
            }

            var rowPixels = GetDataByte(PixelsIndex + y);
            var bitTest = 0x20 >> x;

            return (rowPixels & bitTest) == bitTest;
        }

        public void SetPixel(int x, int y, bool on)
        {
            if (x < 0 || x > TileWidth - 1 || y < 0 || y > TileHeight - 1)
            {
                throw new IndexOutOfRangeException();
            }

            var rowPixels = GetDataByte(PixelsIndex + y);
            var bit = 0x20 >> x;

            if (on)
            {
                rowPixels = (byte)(rowPixels | bit);
            }
            else
            {
                rowPixels = (byte)(rowPixels & ~bit);
            }

            SetDataByte(PixelsIndex + y, rowPixels);
        }

        #endregion

        #region Chunk Execution

        public override void Execute(CdgBitmap bitmap)
        {
            base.Execute(bitmap);

            int colour0 = GetDataByte(Colour0Index);
            int colour1 = GetDataByte(Colour1Index);
            int row = GetDataByte(RowIndex);
            int column = GetDataByte(ColumnIndex);

            // Get the pixel rows
            var pixels = new byte[TileHeight];
            for (var rowIndex = 0; rowIndex < TileHeight; rowIndex++)
            {
                pixels[rowIndex] = GetDataByte(PixelsIndex + rowIndex);
            }

            // Calculate pixel locations
            var x = column * TileWidth;
            var y = row * TileHeight;

            if (!TileType.HasValue) return;
            var type = TileType.Value;
            bitmap.BeginUpdate();

            // Change pixel values
            for (var rowIndex = 0; rowIndex < TileHeight; rowIndex++)
            {
                for (var pixelIndex = 0; pixelIndex < TileWidth; pixelIndex++)
                {
                    var bitTest = 0x20 >> pixelIndex;

                    if (type == TileBlockType.Normal)
                    {
                        bitmap.SetPixel(x + pixelIndex, y + rowIndex,
                            ((pixels[rowIndex] & bitTest) == bitTest) ?
                            colour1 : colour0);
                    }
                    else
                    {
                        var colour = bitmap.GetPixel(x + pixelIndex, y + rowIndex);
                        var newColour = colour ^ 
                            (((pixels[rowIndex] & bitTest) == bitTest) ?
                            colour1 : colour0);

                        bitmap.SetPixel(x + pixelIndex, y + rowIndex, newColour);
                    }
                }
            }

            bitmap.EndUpdate();
        }

        #endregion

        #region Compare Pixels

        public bool ComparePixels(TileBlock comp)
        {
            var result = true;

            for (var rowIndex = 0; rowIndex < TileHeight; rowIndex++)
            {
                var pixels = GetDataByte(PixelsIndex + rowIndex);
                var compPixels = comp.GetDataByte(PixelsIndex + rowIndex);

                if (pixels != compPixels)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        #endregion

        #region Validation

        public override List<Result> Validate(Rules rules)
        {
            var result = new List<Result>();

            // Simple checks
            if (Column > MaxColumn)
            {
                result.Add(new ChunkResult(this, 
                    string.Format("TileBlock: Invalid X ({0})", Column)));
            }

            if (Row > MaxRow)
            {
                result.Add(new ChunkResult(this, 
                    string.Format("TileBlock: Invalid Y ({0})", Row)));
            }

            if (OffColour > MaxColour)
            {
                result.Add(new ChunkResult(this, 
                    string.Format("TileBlock: Invalid off colour ({0})", OffColour)));
            }

            if (OnColour > MaxColour)
            {
                result.Add(new ChunkResult(this, 
                    string.Format("TileBlock: Invalid on colour ({0})", OnColour)));
            }

            // Fix incorrect on colour
//            FixColour(rules, result, true);

            // Fix invalid off colour
            FixColour(rules, result, false);

            // Fix invalid location
            FixLocation(rules, result);

            return result;
        }

        /// <summary>
        /// Fixes the tile colour by checking whether the colour is a 
        /// valid colour (from the rules) and if not, looking at 
        /// neighbouring tiles to determine the correct colour.
        /// </summary>
        /// <param name="rules">The rules required to fix the colour.</param>
        /// <param name="result">Results collection to add any errors/repairs to.</param>
        /// <param name="onColour">Whether to fix the On colour (true) or Off colour (false)</param>
        private void FixColour(Rules rules, ICollection<Result> result, bool onColour)
        {
            var repaired = false;
            if (onColour && !OnColour.HasValue) return;
            if (!onColour && !OffColour.HasValue) return;

            var originalColour = onColour ? OnColour.Value : OffColour.Value;

            var validColours = new List<int>();

            validColours.AddRange(onColour ? rules.PrimaryColours : rules.BackgroundColours);

            if (onColour)
            {
                validColours.AddRange(rules.HighlightColours);
            }

            if (validColours.Contains(originalColour)) return;
            // Invalid colour
            if (validColours.Count == 1)
            {
                // Use only option
                if (onColour)
                {
                    OnColour = validColours[0];
                }
                else
                {
                    OffColour = validColours[0];
                }

                repaired = true;
            }
            else
            {
                // Check for consistency with next TileBlock command
                var chunk = FindNext(rules.File, false);
                if (chunk != null)
                {
                    repaired = MatchChunkColour(onColour, chunk, validColours);
                }
            }

            if (!repaired)
            {
                // Check for consistency with previous TileBlock command
                var chunk = FindPrevious(rules.File, false);
                if (chunk != null)
                {
                    repaired = MatchChunkColour(onColour, chunk, validColours);
                }
            }

            if (repaired)
            {
                var repair = new TileBlockRepair(
                    this,
                    onColour
                        ? TileBlockRepair.RepairType.OnColour
                        : TileBlockRepair.RepairType.OffColour,
                    originalColour, onColour ? OnColour.Value : OffColour.Value
                ) {Status = Result.ResultStatus.Fixed};
                result.Add(repair);
            }
        }

	    private bool MatchChunkColour(bool onColour, TileBlock chunk, ICollection<int> validColours)
	    {
	        if (onColour && !chunk.OnColour.HasValue) return false;
            if (!onColour && !chunk.OffColour.HasValue) return false;

            var repairColour = onColour ? chunk.OnColour.Value : chunk.OffColour.Value;

            if (!validColours.Contains(repairColour)) return false;
            
            if (onColour)
            {
                OnColour = repairColour;
            }
            else
            {
                OffColour = repairColour;
            }

            return true;
        }

        /// <summary>
        /// Attempts to fix the tile location.
        /// </summary>
        /// <param name="rules">Rules required to fix the location.</param>
        /// <param name="result">Results collection to add errors/repairs to.</param>
        private void FixLocation(Rules rules, ICollection<Result> result)
        {
            // Use the tile's On colour to determine whether primary text is being drawn or highlight.
            if (!OnColour.HasValue) return;
            var primary = rules.PrimaryColours.Contains(OnColour.Value);

            if (Column.HasValue && Column > MaxColumn)
            {
                var previous = FindPrevious(rules.File, true);
                if (previous != null && previous.Column.HasValue && Row.HasValue)
                {
                    var newLocation = new Point(previous.Column.Value, Row.Value);
                    if (primary)
                    {
                        newLocation.Offset(1, 0);
                    }

                    var repair = new TileBlockRepair(this, TileBlockRepair.RepairType.Location,
                        new Point(Column.Value, Row.Value), newLocation) {Status = Result.ResultStatus.Fixed};

                    result.Add(repair);

                    Column = newLocation.X;
                }
            }

            if (Row.HasValue && Row > MaxRow)
            {
                var previous = FindPrevious(rules.File, true);
                if (previous != null && previous.Row.HasValue && Column.HasValue)
                {
                    var newLocation = new Point(Column.Value, previous.Row.Value);

                    var repair = new TileBlockRepair(this, TileBlockRepair.RepairType.Location,
                        new Point(Column.Value, Row.Value), newLocation) {Status = Result.ResultStatus.Fixed};

                    result.Add(repair);

                    Row = newLocation.Y;
                }
            }


            if (primary)
            {
                //FixPrimaryLocation(rules, result);
            }
        }

/*
	    private void FixPrimaryLocation(Rules rules, List<Result> result)
        {
            // This fix is based on EZH for now until I add the drawing order to the rules.
            // On EZH hits disks they sometimes draw the Primary colour from left to right
            // (i.e. column by column a row at a time), and sometimes down one, then to the 
            // right and up one (the text is over two rows).

            var previous = FindPrevious(rules.File, true);
            var next = FindNext(rules.File, true);

            var newLocation = Point.Empty;
            var problemFound = false;

            if (next != null && previous != null)
            {
                // In between two other tiles.  

                if (Row == previous.Row)
                {
                    // Row is probably correct - check column is greater than
                    // previous

                    if (Column <= previous.Column)
                    {
                        // Column is wrong - is the next chunk to the right?
                        if (next.Row == previous.Row && next.Column == previous.Column + 2)
                        {
                            // This tile belongs in between them
                            newLocation = new Point(previous.Column.Value + 1, Row.Value);
                        }
                        else
                        {
                            // We can't fix it for certain
                            problemFound = true;
                        }
                    }
                }
                else
                {
                    // Either the row is incorrect or the tile is on the next row
                    if (Row < previous.Row)
                    {
                        // Row is incorrect
                        if (previous.Row == next.Row)
                        {
                            newLocation = new Point(Column.Value, previous.Row.Value);
                        }
                        else
                        {
                            // We don't know where to put it.
                            problemFound = true;
                        }
                    }
                }

                if (!newLocation.IsEmpty)
                {
                    var repair = new TileBlockRepair(this, TileBlockRepair.RepairType.Location,
                               new Point(Column.Value, Row.Value), newLocation);

                    repair.Status = Result.ResultStatus.Fixed;
                    result.Add(repair);

                    Column = newLocation.X;
                    Row = newLocation.Y;
                }
                
                if (problemFound)
                {
                    result.Add(new ChunkResult(this, "Incorrect location"));
                }
            }
        }
*/

        #endregion

        #endregion

        #region Private Methods

	    /// <summary>
	    /// Find the next Tile block (of the same type and colour) relative
	    /// to this Tile Block chunk.
	    /// </summary>
	    /// <param name="file">File to search</param>
	    /// <param name="stopAtMemory">Whether to stop when reaching a memory preset command</param>
	    /// <returns>The found <see cref="TileBlock"/> or null.</returns>
	    private TileBlock FindNext(CdgFile file, bool stopAtMemory)
        {
            TileBlock result = null;

            const int maxSearch = 1000;
            if (Index == null) return null;
            
            for (var index = Index.Value + 1; index < Index + maxSearch && index > Index - maxSearch; index++)
            {
                var chunk = file.Chunks[index];

                if (stopAtMemory && chunk.Type == InstructionType.MemoryPreset)
                {
                    break;
                }

                if (chunk.Type != Type) continue;
                
                result = chunk as TileBlock;
                break;
            }

            return result;
        }

	    /// <summary>
	    /// Find the previous Tile block (of the same type and colour) relative
	    /// to this Tile Block chunk.
	    /// </summary>
	    /// <param name="file">File to search</param>
	    /// <param name="stopAtMemory">Whehther to stop searching when we reach a memory preset instruction.</param>
	    /// <returns>The found <see cref="TileBlock"/> or null.</returns>
	    private TileBlock FindPrevious(CdgFile file, bool stopAtMemory)
	    {
	        if (!Index.HasValue) return null;

            TileBlock result = null;

            const int maxSearch = 1000;

            for (var index = Index.Value - 1; index > Index.Value - maxSearch &&
                index > 0; index--)
            {
                var chunk = file.Chunks[index];
                if (stopAtMemory && chunk.Type == InstructionType.MemoryPreset)
                {
                    break;
                }

                if (chunk.Type == Type)
                {
                    result = chunk as TileBlock;
                    break;
                }
            }

            return result;
        }


        #endregion

        #region Object Overrides

        public override string ToString()
		{
			return string.Format("Tile Block ({0})", Type);
		}

		#endregion

        #region Data

	    int? _row;
        int? _column;
        int? _onColour;
        int? _offColour;
        TileBlockType? _tileType;

	    #endregion
    }
}
