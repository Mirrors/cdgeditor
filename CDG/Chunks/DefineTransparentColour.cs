namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a Define Transparent Colour CDG Chunk.
	/// </summary>
    public class DefineTransparentColour : Chunk
	{
	    private const int ColourIndexByteOffset = 0;

		#region Construction

        public DefineTransparentColour(byte[] data)
            : base(data)
		{
            _colourIndex = GetDataByte(ColourIndexByteOffset) & 0xF;
        }

		#endregion

        #region Properties

        public int? ColourIndex
        {
            get
            {
                return _colourIndex;
            }
            /*set
            {
                _ColourIndex = value;
                if (value.HasValue)
                {
                    SetDataByte(COLOUR_INDEX, (byte)value);
                }
            }*/
        }

        #endregion

		#region Object Overrides

		public override string ToString()
		{
			return "Define Transparent Colour";
		}

		#endregion

        #region Data

	    readonly int? _colourIndex;

        #endregion
    }
}
