namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a Scroll Copy CDG Chunk.
	/// </summary>
    public class ScrollCopy : ScrollChunk
    {
		#region Construction

        public ScrollCopy(byte[] data)
            : base(data)
		{
	        // Nothing to do here.
		}

		#endregion

        #region Public Methods

        public override void Execute(CdgBitmap bitmap)
        {
            base.Execute(bitmap);

            if (HorizontalScrollInstruction != null) bitmap.ScrollCopyHorizontal(HorizontalScrollInstruction.Value);
            if (VerticalScrollInstruction != null) bitmap.ScrollCopyVertical(VerticalScrollInstruction.Value);

            if (HorizontalScrollOffset != null) bitmap.HorizontalScrollOffset = HorizontalScrollOffset.Value;
            if (VerticalScrollOffset != null) bitmap.VerticalScrollOffset = VerticalScrollOffset.Value;
        }
        #endregion
        
        #region Object Overrides

		public override string ToString()
		{
			return "Scroll Copy";
		}

		#endregion
	}
}
