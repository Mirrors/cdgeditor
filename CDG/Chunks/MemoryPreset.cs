using System.Collections.Generic;
using Cdg.Validation;

namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a Memory Preset CDG Chunk
	/// </summary>
    public class MemoryPreset : Chunk
	{
	    private const int ColourOffset = 0;
	    private const int RepeatOffset = 1;

        // Validation constants
	    private const int MaxColour = 15;

		#region Construction

        public MemoryPreset(byte[] data)
            : base(data)
		{
            _colourIndex = GetDataByte(ColourOffset);
            _repeat = GetDataByte(RepeatOffset);
		}

		#endregion

        #region Properties

        public int? ColourIndex
        {
            get
            {
                return _colourIndex;
            }
            set
            {
                _colourIndex = value;
                if (value.HasValue)
                {
                    SetDataByte(ColourOffset, (byte)value);
                }
            }
        }

        public int? Repeat
        {
            get
            {
                return _repeat;
            }
            set
            {
                _repeat = value;
                if (value.HasValue)
                {
                    SetDataByte(RepeatOffset, (byte)value);
                }
            }
        }

        #endregion

        #region Public Methods

        public override void Execute(CdgBitmap bitmap)
        {
            base.Execute(bitmap);

            bitmap.BeginUpdate();
            if (ColourIndex != null) bitmap.Clear(ColourIndex.Value);
            bitmap.HorizontalScrollOffset = 0;
            bitmap.VerticalScrollOffset = 0;
            bitmap.EndUpdate();
        }

        #endregion

        #region Validation

        public override List<Result> Validate(Rules rules)
        {
            var result = new List<Result>();

            // Simple checks
            if (ColourIndex == null) return result;
            var originalColour = ColourIndex.Value;
            if (originalColour > MaxColour)
            {
                result.Add(
                    new ChunkResult(this, 
                        string.Format("MemoryPreset: Invalid Colour Index {0}", originalColour)));
            }

            // Rule based checks
/*            if (!rules.BackgroundColours.Contains(originalColour))
            {
                bool repaired = false;

                if (rules.BackgroundColours.Count == 1)
                {
                    // There's only 1 valid background colour - so use that.
                    this.ColourIndex = rules.BackgroundColours[0];
                    repaired = true;
                }
                else
                {
                    // Check for consistency with next MemoryPreset command
                    Chunk chunk = FindNext(rules.File, Type);
                    if (chunk == null)
                    {
                        // Check for consistency with previous MemoryPreset command
                        chunk = FindPrevious(rules.File, Type);
                    }

                    if (chunk != null)
                    {
                        ColourIndex = (chunk as MemoryPreset).ColourIndex;
                        repaired = true;
                    }
                }
            
                if (repaired)
                {
                    Validation.MemoryPresetRepair repair = new Validation.MemoryPresetRepair(this, originalColour, ColourIndex);
                    repair.Status = Validation.Result.ResultStatus.Fixed;
                    result.Add(repair);
                }
            } */

            return result;
        }

        #endregion
        
        #region Object Overrides

		public override string ToString()
		{
			return "Memory Preset";
		}

		#endregion

        #region Data

	    private int? _colourIndex;
	    private int? _repeat;

        #endregion
    }
}
