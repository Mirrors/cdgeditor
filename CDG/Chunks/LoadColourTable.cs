using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a  Load Colour Table CDG Chunk.
	/// </summary>
    public class LoadColourTable : Chunk
	{
		#region Constants

		/// <summary>
		/// The colour table has 16 entries, only 8 can fit in a CDG command,
		/// so they are split into the Low (0-7) and High (8-15) sets.
		/// </summary>
		public enum SetType
		{
			Low,
			High
		}

        const int NumColours = 8;
        
        #endregion
		
		#region Construction

        public LoadColourTable(byte[] data)
            : base(data)
		{
			if (InstructionByte != (byte)InstructionType.LoadColTableLow
                && InstructionByte != (byte)InstructionType.LoadColTableHigh)
            {
                InstructionByte = (byte)InstructionType.LoadColTableLow;
            }

            CreateDefaultColors();            
        }

        private void CreateDefaultColors()
        {
            // Create a sensible default palette if all black
            var allBlack = true;
            var colours = GetColours();
            foreach (var colour in colours)
            {
                if (colour.R != 0 || colour.G != 0 || colour.B != 0)
                {
                    allBlack = false;
                    break;
                }
            }

            if (allBlack)
            {
                var bitmap = new Bitmap(1, 1, PixelFormat.Format4bppIndexed);

                var palette = bitmap.Palette;

                var index = 0;
                palette.Entries[index++] = Color.FromArgb(0, 0, 0);
                palette.Entries[index++] = Color.FromArgb(127, 0, 0);
                palette.Entries[index++] = Color.FromArgb(0, 127, 0);
                palette.Entries[index++] = Color.FromArgb(0, 0, 127);
                palette.Entries[index++] = Color.FromArgb(127, 127, 0);
                palette.Entries[index++] = Color.FromArgb(127, 0, 127);
                palette.Entries[index++] = Color.FromArgb(0, 127, 127);
                palette.Entries[index++] = Color.FromArgb(64, 64, 64);
                palette.Entries[index++] = Color.FromArgb(127, 127, 127);
                palette.Entries[index++] = Color.FromArgb(255, 0, 0);
                palette.Entries[index++] = Color.FromArgb(0, 255, 0);
                palette.Entries[index++] = Color.FromArgb(0, 0, 255);
                palette.Entries[index++] = Color.FromArgb(255, 255, 0);
                palette.Entries[index++] = Color.FromArgb(255, 0, 255);
                palette.Entries[index++] = Color.FromArgb(0, 255, 255);
                palette.Entries[index] = Color.FromArgb(255, 255, 255);

                SetColours(palette);
            }
        }

		#endregion

		#region Public Properties

	    private SetType Set
		{
			get
			{
                var instruction = InstructionByte;
				switch (instruction)
				{
				    case (byte)InstructionType.LoadColTableLow:
				        return SetType.Low;
				    case (byte)InstructionType.LoadColTableHigh:
				        return SetType.High;
				}
			    throw new InvalidOperationException("Invalid instruction type for LoadColourTable chunk");
			}
	    }

		#endregion

        #region Public Methods

        public override void Execute(CdgBitmap bitmap)
        {
            if (!bitmap.UseTestColourTable)
            {
                var palette = bitmap.Palette;
                var colours = GetColours();

                var start = (Set == SetType.Low) ? 0 : colours.Count;
                for (var index = 0; index < colours.Count; index++)
                {
                    palette.Entries[index + start] = colours[index];
                }

                bitmap.Palette = palette;
            }

            base.Execute(bitmap);
        }

        /// <summary>
        /// Extracts the colours from the CDG data
        /// </summary>
        /// <returns>List of <see cref="Color"/> objects containing the colours.</returns>
        private List<Color> GetColours()
        {

            // Each colour is stored in two bytes in the format:
            //     [---high byte---]   [---low byte----]
            //      7 6 5 4 3 2 1 0     7 6 5 4 3 2 1 0
            //      X X r r r r g g     X X g g b b b b
            var colours = new List<Color>();

            for (var colourIndex = 0; colourIndex < NumColours; colourIndex++)
            {
                var byte0 = GetDataByte(colourIndex * 2);
                var byte1 = GetDataByte((colourIndex * 2) + 1);

                var red = (byte0 & 0x3F) >> 2;
                var green = ((byte0 & 0x3) << 2) | ((byte1 &0x3F) >> 4);
                var blue = byte1 & 0xF;

                var colour = Color.FromArgb(CdgValToWin(red), CdgValToWin(green), CdgValToWin(blue));
                colours.Add(colour);
            }

            return colours;
        }

        public void SetColours(ColorPalette palette)
        {
            var start = InstructionByte == (byte)InstructionType.LoadColTableLow ? 0 : NumColours;

            for (var index = 0; index < NumColours; index++)
            {
                // Each colour is stored in two bytes in the format:
                //     [---high byte---]   [---low byte----]
                //      7 6 5 4 3 2 1 0     7 6 5 4 3 2 1 0
                //      X X r r r r g g     X X g g b b b b

                var colour = palette.Entries[start + index];
                var red = WinValToCdg(colour.R);
                var green = WinValToCdg(colour.G);
                var blue = WinValToCdg(colour.B);

                var byte1 = (byte)((red << 2) | (green >> 2));
                var byte2 = (byte)(((green & 0x3) << 4) | blue);

                SetDataByte(index * 2, byte1);
                SetDataByte(index * 2 + 1, byte2);
            }
        }

        #endregion

        #region Validation
    
        // No validation possible.    

        #endregion

        #region Private Methods

	    private int CdgValToWin(int value)
        {
            return value * 17;
        }

	    private int WinValToCdg(int value)
        {
            return value / 17;
        }
        #endregion

        #region Object Overrides

        public override string ToString()
		{
            var entries = Set == SetType.Low ? "0-7" : "8-15";
			return string.Format("Load Colour Table (Entries {0})", entries);
		}

		#endregion

	}
}
