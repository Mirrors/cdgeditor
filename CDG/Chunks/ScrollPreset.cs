namespace Cdg.Chunks
{
	/// <summary>
	/// Encapsulates a Scroll Preset CDG Chunk
	/// </summary>
    public class ScrollPreset : ScrollChunk
	{
        #region Constants

	    private const int ScrollFillColor = 0;

	    #endregion
        
        #region Construction

        public ScrollPreset(byte[] data)
            : base(data)
		{
            _fillColor = GetDataByte(ScrollFillColor) & 0x0F;
		}

		#endregion

        #region Public Methods

        public override void Execute(CdgBitmap bitmap)
        {
            base.Execute(bitmap);

            if (HorizontalScrollInstruction.HasValue && _fillColor.HasValue) 
                bitmap.ScrollPresetHorizontal(HorizontalScrollInstruction.Value, _fillColor.Value);

            if (VerticalScrollInstruction.HasValue && _fillColor.HasValue) 
                bitmap.ScrollPresetVertical(VerticalScrollInstruction.Value, _fillColor.Value);

            if (HorizontalScrollOffset != null) bitmap.HorizontalScrollOffset = HorizontalScrollOffset.Value;
            if (VerticalScrollOffset != null) bitmap.VerticalScrollOffset = VerticalScrollOffset.Value;
        }

        #endregion
        
        #region Object Overrides

		public override string ToString()
		{
			return "Scroll Preset";
		}

		#endregion

        #region Properties

        public int? FillColor
        {
            get
            {
                return _fillColor;
            }
            set
            {
                _fillColor = value;
                if (_fillColor.HasValue)
                {
                    SetDataByte(ScrollFillColor, (byte)(_fillColor.Value & 0x0F));
                }
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Colour to fill scrolled pixels with
        /// </summary>
        int? _fillColor;

        #endregion
    }
}
