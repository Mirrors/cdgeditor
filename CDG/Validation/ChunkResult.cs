﻿using System.IO;
using Cdg.Chunks;

namespace Cdg.Validation
{
    public class ChunkResult : Result
    {
        /// <summary>
        /// Creates a new instance of a <see cref="ChunkResult"/>.
        /// </summary>
        /// <param name="chunk">The chunk that this chunk error is associated with.</param>
        /// <param name="description">The description of the error.</param>
        public ChunkResult(Chunk chunk, string description) :
            base(description)
        {
            Type = ResultType.ChunkResult;
            _chunk = chunk;
        }

        /// <summary>
        /// Creates a new instance of a <see cref="ChunkResult"/> from a file stream.
        /// </summary>
        /// <param name="file">The cdg file to get the chunk from.</param>
        /// <param name="reader">A <see cref="BinaryReader"/> to read the result from.</param>
        public ChunkResult(CdgFile file, BinaryReader reader) : base(reader)
        {
            Type = ResultType.ChunkResult; 
            int chunkIndex = reader.ReadInt32();
            _chunk = file.Chunks[chunkIndex];
        }

        #region Public Properties

        public virtual string Time
        {
            get
            {
                return _chunk.Time;
            }
        }
        
        public Chunk Chunk
        {
            get
            {
                return _chunk;
            }
        }

        #endregion

        #region Object Overrides

        /// <summary>
        /// Formats the chunk result as a string.
        /// </summary>
        /// <returns>A tring representation of the chunk result.</returns>
        public override string ToString()
        {
            return string.Format("Chunk [{0}] Error: {1}", _chunk.Time, base.ToString());
        }

        #endregion

        #region Result overrides

        public override void Save(BinaryWriter writer)
        {
            base.Save(writer);

            // Add the chunk index
            if (_chunk.Index != null) writer.Write(_chunk.Index.Value);
        }

        #endregion

        #region Data

        /// <summary>
        /// The chunk that this error is associated with.
        /// </summary>
        readonly Chunk _chunk;

        #endregion
    }
}
