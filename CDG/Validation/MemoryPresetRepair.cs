﻿using System.IO;
using Cdg.Chunks;

namespace Cdg.Validation
{
    /// <summary>
    /// Encapsulates the repair made to a memory preset command
    /// </summary>
    public class MemoryPresetRepair : ChunkRepair
    {
        #region Construction

        /// <summary>
        /// Creates a new instance of a <see cref="MemoryPresetRepair"/> from a file stream.
        /// </summary>
        /// <param name="cdgFile">The cdg file to get the chunk from.</param>
        /// <param name="reader">A <see cref="BinaryReader"/> to read the result from.</param>
        public MemoryPresetRepair(CdgFile cdgFile, BinaryReader reader)
            : base(cdgFile, reader)
        {
            Type = ResultType.MemoryPresetRepair;
            _originalColourIndex = reader.ReadInt32();
            _newColourIndex = reader.ReadInt32();
        }

        #endregion

        #region Result overrides

        public override void Save(BinaryWriter writer)
        {
            // Use base class to type and version
            base.Save(writer);

            // Write original and replacement colour indices
            writer.Write(_originalColourIndex);
            writer.Write(_newColourIndex);
        }

        #endregion

        #region ChunkRepair ovverides

        public override void Revert()
        {
            base.Revert();

            var memoryPreset = Chunk as MemoryPreset;
            if (memoryPreset != null) memoryPreset.ColourIndex = _originalColourIndex;
        }

        #endregion

        #region Data

        /// <summary>
        /// The original colour in case this repair needs to be reverted
        /// </summary>
        private readonly int _originalColourIndex;

        /// <summary>
        /// The colour that the repair has changed the memory preset to.
        /// </summary>
        private readonly int _newColourIndex;

        #endregion
    }
}
