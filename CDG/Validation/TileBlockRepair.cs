﻿using System;
using System.Drawing;
using System.IO;
using Cdg.Chunks;

namespace Cdg.Validation
{
    /// <summary>
    /// Encapsulates a tile block repair result.
    /// </summary>
    public class TileBlockRepair : ChunkRepair
    {
        #region Types

        public enum RepairType
        {
            OnColour,
            OffColour,
            Location
        }

        #endregion

        #region Construction

        public TileBlockRepair(TileBlock chunk, RepairType type, 
            int originalColour, int newColour) : 
            base(chunk, string.Format("Repair: TileBlock {0} {1} to {2}",
                type, originalColour, newColour))
        {
            Type = ResultType.TileBlockRepair;
            _repairType = type;
            _originalColour = originalColour;
            _newColour = newColour;
        }

        public TileBlockRepair(TileBlock chunk, RepairType type,
            Point originalLocation, Point newLocation) :
            base(chunk, string.Format("Repair: TileBlock Location from {0} to {1}",
                originalLocation, newLocation))
        {
            Type = ResultType.TileBlockRepair;
            _repairType = type;
            _originalLocation = originalLocation;
            _newLocation = newLocation;
        }

        /// <summary>
        /// Creates a new instance of a <see cref="TileBlockRepair"/> from a file stream.
        /// </summary>
        /// <param name="cdgFile">The cdg file to get the chunk from.</param>
        /// <param name="reader">A <see cref="BinaryReader"/> to read the result from.</param>
        public TileBlockRepair(CdgFile cdgFile, BinaryReader reader)
            : base(cdgFile, reader)
        {
            Type = ResultType.TileBlockRepair;
            
            // Get the repair type
            int repairTypeInt = reader.ReadInt32();

            if (Enum.IsDefined(typeof(RepairType), repairTypeInt))
            {
                _repairType = (RepairType)repairTypeInt;
                switch (_repairType)
                {
                    case RepairType.Location:
                    {
                        int oldX = reader.ReadInt32();
                        int oldY = reader.ReadInt32();
                        int newX = reader.ReadInt32();
                        int newY = reader.ReadInt32();

                        _originalLocation = new Point(oldX, oldY);
                        _newLocation = new Point(newX, newY);
                        break;
                    }
                    case RepairType.OffColour:
                    case RepairType.OnColour:
                    {
                        _originalColour = reader.ReadInt32();
                        _newColour = reader.ReadInt32();
                        break;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("Repair file is corrupt");
            }
        }

        #endregion

        #region ChunkResult overrides

        public override void Save(BinaryWriter writer)
        {
            // Let base class save the type and version
            base.Save(writer);

            // Write the repair type
            writer.Write((int)_repairType);

            switch (_repairType)
            {
                case RepairType.OffColour:
                case RepairType.OnColour:
                {
                    writer.Write(_originalColour);
                    writer.Write(_newColour);
                    break;
                }

                case RepairType.Location:
                {
                    writer.Write(_originalLocation.X);
                    writer.Write(_originalLocation.Y);
                    writer.Write(_newLocation.X);
                    writer.Write(_newLocation.Y);
                    break;
                }
            }
        }

        #endregion

        #region ChunkRepair Overrides

        public override void Revert()
        {
            base.Revert();

            var tileBlock = Chunk as TileBlock;
            if (tileBlock == null) return;

            switch(_repairType)
            {
                case RepairType.OffColour:
                    
                    tileBlock.OffColour = _originalColour;
                    break;
                case RepairType.OnColour:
                    tileBlock.OnColour = _originalColour;
                    break;
                case RepairType.Location:
                {
                    tileBlock.Column = _originalLocation.X;
                    tileBlock.Row = _originalLocation.Y;
                    break;
                }
                default:
                    return;
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// Type of repair performed.
        /// </summary>
        private readonly RepairType _repairType;

        /// <summary>
        /// Original colour (for a colour repair)
        /// </summary>
        private readonly int _originalColour;

        /// <summary>
        /// New colour (for a colour repair)
        /// </summary>
        private readonly int _newColour;

        /// <summary>
        /// Original location (for a location repair);
        /// </summary>
        private Point _originalLocation;

        /// <summary>
        /// New location (for a location repair);
        /// </summary>
        private Point _newLocation;

        #endregion
    }
}
