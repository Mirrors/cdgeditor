﻿using System.Collections.Generic;

namespace Cdg.Validation
{
    /// <summary>
    /// Encapsulates a CDG font.
    /// </summary>
    public class Font
    {
        #region Construction

        /// <summary>
        /// Creates a new instance of a Font.
        /// </summary>
        /// <param name="name">The name of the font.</param>
        public Font(string name)
        {
            _Name = name;
            Characters = new List<FontChar>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the pixel representations of the CDG font characters.
        /// </summary>
        public List<FontChar> Characters { get; private set; }

        #endregion

        #region Data

        /// <summary>
        /// The name of the font.
        /// </summary>
        string _Name;

        #endregion
    }
}
