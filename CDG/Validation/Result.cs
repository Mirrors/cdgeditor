﻿using System;
using System.IO;

namespace Cdg.Validation
{
    /// <summary>
    /// Base class for all CDG operation results.
    /// </summary>
    public class Result
    {
        #region Result Types

        protected enum ResultType
        {
            Generic,
            ChunkResult,
            MemoryPresetRepair,
            TileBlockRepair
        }

        public enum ResultStatus
        {
            None,
            Fixed,
            Reverted
        }

        #endregion

        #region Construction

        /// <summary>
        /// Creates a new instance of a <see cref="Result"/>.
        /// </summary>
        /// <param name="description">A description of the result.</param>
        protected Result(string description)
        {
            Type = ResultType.Generic;

            Description = description;
            _version = 1;
            Status = ResultStatus.None;
        }

        /// <summary>
        /// Creates a new instance of a <see cref="Result"/> from a file stream.
        /// </summary>
        /// <param name="reader">A <see cref="BinaryReader"/> to read the result from.</param>
        protected Result(BinaryReader reader)
        {
            Type = ResultType.Generic;
            _version = reader.ReadInt32();

            System.Diagnostics.Debug.Assert(_version == 1);

            Description = reader.ReadString();
            var statusInt = reader.ReadInt32();
            if (Enum.IsDefined(typeof(ResultStatus), statusInt))
            {
                Status = (ResultStatus)statusInt;
            }
            else
            {
                throw new InvalidDataException("Invalid result status");
            }
        }

        #endregion

        #region Public Properties

        private string Description { get; set; }

        public ResultStatus Status { private get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Saves the result to a file stream.
        /// </summary>
        /// <param name="writer">Stream to save the result to.</param>
        public virtual void Save(BinaryWriter writer)
        {
            writer.Write((int)Type);
            writer.Write(_version);
            writer.Write(Description);
            writer.Write((int)Status);
        }

        /// <summary>
        /// Loads a result from a file stream
        /// </summary>
        /// <returns></returns>
        public static Result Load(CdgFile cdgFile, BinaryReader reader)
        {
            Result result = null;

            try
            {
                // Read the type (if there is one)
                var typeInt = reader.ReadInt32();

                if (Enum.IsDefined(typeof(ResultType), typeInt))
                {
                    var type = (ResultType)typeInt;
                    switch (type)
                    {
                        case ResultType.Generic:
                            {
                                result = new Result(reader);
                                break;
                            }
                        case ResultType.ChunkResult:
                            {
                                result = new ChunkResult(cdgFile, reader);
                                break;
                            }
                        case ResultType.MemoryPresetRepair:
                            {
                                result = new MemoryPresetRepair(cdgFile, reader);
                                break;
                            }
                        case ResultType.TileBlockRepair:
                            {
                                result = new TileBlockRepair(cdgFile, reader);
                                break;
                            }
                    }
                }
                else
                {
                    throw new InvalidOperationException("Results file is corrupted: Invalid result type");
                }
            }
            catch (EndOfStreamException)
            {
                // No entries to read.
            }
            
            return result;
        }

        #endregion

        #region Data

        /// <summary>
        /// The type of result (for persistance)
        /// </summary>
        protected ResultType Type;

        /// <summary>
        /// The version number of the result
        /// </summary>
        readonly int _version;

        #endregion
    }
}
