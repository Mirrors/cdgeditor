﻿using System.Collections.Generic;

namespace Cdg.Validation
{
    /// <summary>
    /// Validation rules
    /// </summary>
    public class Rules
    {
        #region Construction

        public Rules()
        {
            PrimaryColours = new List<int>();
            HighlightColours = new List<int>();
            BackgroundColours = new List<int>();

            // Hard coded values for testing
            PrimaryColours.Add(1);
            HighlightColours.Add(10);
            BackgroundColours.Add(0);
        }

        #endregion

        /// <summary>
        /// Access to the file for validiting against other chunks.
        /// </summary>
        public CdgFile File;

        /// <summary>
        /// Valid primary colours
        /// </summary>
        public readonly List<int> PrimaryColours;

        /// <summary>
        /// Valid highlight colours
        /// </summary>
        public readonly List<int> HighlightColours;

        public readonly List<int> BackgroundColours;
    }
}
