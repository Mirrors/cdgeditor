﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Cdg.Chunks;

namespace Cdg
{
    /// <summary>
    /// Encapsulates a CDG bitmap object
    /// </summary>
    public class CdgBitmap
    {
        #region Constants

        private const int Width = 300;
        private const int Height = 216;

        #endregion

        #region Construction

        /// <summary>
        /// Creates a new instance of <see cref="CdgBitmap"/>
        /// </summary>
        public CdgBitmap()
        {
            Image = new Bitmap(Width, Height, PixelFormat.Format4bppIndexed);
        }

        #endregion

        #region Properties

        /// <summary>
        /// The internal bitmap image for displaying
        /// </summary>
        public Bitmap Image { get; private set; }

        public ColorPalette Palette
        {
            get
            {
                return Image.Palette;
            }
            set
            {
                Image.Palette = value;
                //RaiseBorderColourChanged();
            }
        }

        /// <summary>
        /// Gets or sets the CDG border colour index.
        /// </summary>
        public int BorderColour { get; set; }

        /// <summary>
        /// Whether to use the test colour table (ignoring the last 
        /// load colour table chunk)
        /// </summary>
        public bool UseTestColourTable
        {
            get
            {
                return _useTestColourTable;
            }
            set
            {
                _useTestColourTable = value;
                if (_useTestColourTable)
                {
                    SetTestColourTable();
                }
            }
        }

        /// <summary>
        /// Amount to scroll the output display left by in CDG pixels.
        /// </summary>
        public int HorizontalScrollOffset
        {
            get
            {
                return _horizontalScrollOffset;
            }
            set
            {
                _horizontalScrollOffset = value;
                RaiseInvalidated();
            }
        }

        /// <summary>
        /// Amount to scroll the output display up by in CDG pixels.
        /// </summary>
        public int VerticalScrollOffset
        {
            get
            {
                return _verticalScrollOffset;
            }
            set
            {
                _verticalScrollOffset = value;
                RaiseInvalidated();
            }
        }

        #endregion

        #region Events

       /* public event EventHandler BorderColourChanged;

        private void RaiseBorderColourChanged()
        {
            if (BorderColourChanged != null)
            {
                BorderColourChanged(this, new EventArgs());
            }
        }*/

        public event EventHandler Invalidated;

        private void RaiseInvalidated()
        {
            if (Invalidated != null)
            {
                Invalidated(this, new EventArgs());
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Begin a bitmap data update.
        /// </summary>
        /// <remarks>
        /// Caller must call BeginUpdate before manipulating _BitmapData.
        /// </remarks>
        public void BeginUpdate()
        {
            if (_bitmapData != null)
            {
                throw new InvalidOperationException();
            }

            _bitmapData = Image.LockBits(
                new Rectangle(0, 0, Image.Width, Image.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format4bppIndexed);
        }

        /// <summary>
        /// End a bitmap data update.
        /// </summary>
        /// <remarks>
        /// Caller must call BeginUpdate after manipulating _BitmapData.
        /// </remarks>
        public void EndUpdate()
        {
            if (_bitmapData == null)
            {
                throw new InvalidOperationException();
            }

            Image.UnlockBits(_bitmapData);
            _bitmapData = null;
            RaiseInvalidated();
        }

        /// <summary>
        /// Returns the colour index of the specified pixel.
        /// </summary>
        /// <param name="x">X Coordinate of the CDG Pixel to get.</param>
        /// <param name="y">Y Coordinate of the CDG Pixel to get.</param>
        /// <returns>The colour index of the pixel.</returns>
        public int GetPixel(int x, int y)
        {
            if (_bitmapData == null)
            {
                throw new InvalidOperationException();
            }

            int result = 0;

            // Protect against reading out of range
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                // Find the relevant byte
                IntPtr bytePtr = new IntPtr(
                    _bitmapData.Scan0.ToInt32() +
                    (_bitmapData.Stride * y) +
                    (x / 2));
                byte byteVal = Marshal.ReadByte(bytePtr);

                // return relevant nibble
                if ((x & 1) == 1)
                {
                    // Lower 4 bits
                    result = byteVal & 0xF;
                }
                else
                {
                    // Upper 4 bits
                    result = byteVal >> 4;
                }
            }

            return result;
        }

        /// <summary>
        /// Sets a CDG pixel to the specified colour index.
        /// </summary>
        /// <param name="x">X Coordinate of the CDG Pixel to get.</param>
        /// <param name="y">Y Coordinate of the CDG Pixel to get.</param>
        /// <param name="colour">The colour index of the pixel.</param>
        public void SetPixel(int x, int y, int colour)
        {
            if (_bitmapData == null)
            {
                throw new InvalidOperationException();
            }
            
            // Protect against writing out of range
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                // Find the relevant byte
                IntPtr bytePtr = new IntPtr(
                    _bitmapData.Scan0.ToInt32() +
                    (_bitmapData.Stride * y) +
                    (x / 2));
                byte byteVal = Marshal.ReadByte(bytePtr);

                // replace relevant nibble
                if ((x & 1) == 1)
                {
                    // Lower 4 bits
                    byteVal &= 0xF0;
                    byteVal = (byte)(byteVal | colour);
                }
                else
                {
                    // Upper 4 bits
                    byteVal &= 0x0F;
                    byteVal = (byte)(byteVal | colour << 4);
                }

                Marshal.WriteByte(bytePtr, byteVal);
            }
        }

        /// <summary>
        /// Clears the CDG bitmap
        /// </summary>
        public void Clear()
        {
            BeginUpdate();
            Clear(0);
            EndUpdate();
        }

        /// <summary>
        /// Sets the entire bitmap to the colour specified
        /// </summary>
        /// <param name="colourIndex">Colour index to use.</param>
        public void Clear(int colourIndex)
        {
            if (_bitmapData == null)
            {
                throw new InvalidOperationException();
            }

            // Image is two pixels in one byte, so put the colour index
            // into each nibble
            byte twoPixels = (byte)(colourIndex << 4 | colourIndex);
            memset(_bitmapData.Scan0, twoPixels, _bitmapData.Stride * Height);

            _horizontalScrollOffset = 0;
            _verticalScrollOffset = 0;
        }

        /// <summary>
        /// Scrolls the pixel data horizontally.
        /// </summary>
        /// <param name="direction">Direction to scroll.</param>
        /// <param name="colorIndex">Colour to fill new tiles with.</param>
        public void ScrollPresetHorizontal(ScrollChunk.HScrollInstruction direction, int colorIndex)
        {
            ScrollHorizontal(direction, colorIndex);
        }

        /// <summary>
        /// Scrolls the pixel data vertically.
        /// </summary>
        /// <param name="direction">Direction to scroll.</param>
        /// <param name="colorIndex">Colour to fill new tiles with.</param>
        public void ScrollPresetVertical(ScrollChunk.VScrollInstruction direction, int colorIndex)
        {
            ScrollVertical(direction, colorIndex);
        }

        /// <summary>
        /// Scrolls the pixel data horizontally, copying the values of the scrolled
        /// of tiles to the new tiles scrolled in,.
        /// </summary>
        /// <param name="direction">Direction to scroll.</param>
        public void ScrollCopyHorizontal(ScrollChunk.HScrollInstruction direction)
        {
            ScrollHorizontal(direction, -1);
        }

        /// <summary>
        /// Scrolls the pixel data vertically, copying the values of the scrolled
        /// of tiles to the new tiles scrolled in,.
        /// </summary>
        /// <param name="direction">Direction to scroll.</param>
        public void ScrollCopyVertical(ScrollChunk.VScrollInstruction direction)
        {
            ScrollVertical(direction, -1);
        }

        #endregion

        #region Private Methods

        private void SetTestColourTable()
        {
            var palette = Palette;
            
            var index = 0;
            palette.Entries[index++] = Color.FromArgb(0, 0, 0);
            palette.Entries[index++] = Color.FromArgb(255, 0, 0);
            palette.Entries[index++] = Color.FromArgb(0, 255, 0);
            palette.Entries[index++] = Color.FromArgb(0, 0, 255);
            palette.Entries[index++] = Color.FromArgb(255, 255, 0);
            palette.Entries[index++] = Color.FromArgb(255, 0, 255);
            palette.Entries[index++] = Color.FromArgb(0, 255, 255);
            palette.Entries[index++] = Color.FromArgb(255, 255, 255);
            palette.Entries[index++] = Color.FromArgb(64, 64, 64);
            palette.Entries[index++] = Color.FromArgb(127, 0, 0);
            palette.Entries[index++] = Color.FromArgb(0, 127, 0);
            palette.Entries[index++] = Color.FromArgb(0, 0, 127);
            palette.Entries[index++] = Color.FromArgb(127, 127, 0);
            palette.Entries[index++] = Color.FromArgb(127, 0, 127);
            palette.Entries[index++] = Color.FromArgb(0, 127, 127);
            palette.Entries[index] = Color.FromArgb(127, 127, 127);

            Palette = palette;
        }

        /// <summary>
        /// Scrolls the CDG bitmap data horizontally
        /// </summary>
        /// <param name="direction">Direction to scroll pixels.</param>
        /// <param name="fillColorIndex">Color index to fill with (-1 indicates
        /// to copy scrolled off values)</param>
        void ScrollHorizontal(ScrollChunk.HScrollInstruction direction, int fillColorIndex)
        {
            if (direction == ScrollChunk.HScrollInstruction.None)
            {
                return;
            }

            Bitmap tempBitmap = new Bitmap(Width, Height, PixelFormat.Format4bppIndexed);
            BitmapData tempData = tempBitmap.LockBits(
                new Rectangle(0, 0, Image.Width, Image.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format4bppIndexed);

            BeginUpdate();

            int sizeBytes = _bitmapData.Stride * Height;

            // 4 bits per pixel = 2 pixels per byte
            int tileWidthBytes = TileBlock.TileWidth / 2;
            int remainingWidth = Width / 2 - tileWidthBytes;

            // Copy all of the image to the temp buffer
            memcpy(tempData.Scan0, _bitmapData.Scan0, sizeBytes);

            byte colorByte = (byte)(fillColorIndex << 4 | fillColorIndex);

            switch (direction)
            {
                case ScrollChunk.HScrollInstruction.Left:
                {
                    // Take TILE_WIDTH pixels off the left.
                    for (int y = 0; y < Height; y++)
                    {
                        int yOffset = y * _bitmapData.Stride;
                        memcpy(new IntPtr(_bitmapData.Scan0.ToInt32() + yOffset),
                            new IntPtr(tempData.Scan0.ToInt32() + yOffset + tileWidthBytes),
                            remainingWidth);

                        if (fillColorIndex != -1)
                        {
                            memset(new IntPtr(_bitmapData.Scan0.ToInt32() + yOffset + remainingWidth),
                                colorByte, tileWidthBytes);
                        }
                        else
                        {
                            memcpy(new IntPtr(_bitmapData.Scan0.ToInt32() + yOffset + remainingWidth),
                                new IntPtr(tempData.Scan0.ToInt32() + yOffset),
                                tileWidthBytes);
                        }
                    }
                    break;
                }
                case ScrollChunk.HScrollInstruction.Right:
                {
                    // Take TILE_WIDTH pixels off the right.

                    for (int y = 0; y < Height; y++)
                    {
                        int yOffset = y * _bitmapData.Stride;
                        memcpy(new IntPtr(_bitmapData.Scan0.ToInt32() + yOffset + tileWidthBytes),
                            new IntPtr(tempData.Scan0.ToInt32() + yOffset),
                            remainingWidth);

                        if (fillColorIndex != -1)
                        {
                            memset(new IntPtr(_bitmapData.Scan0.ToInt32() + yOffset),
                                colorByte, tileWidthBytes);
                        }
                        else
                        {
                            memcpy(new IntPtr(_bitmapData.Scan0.ToInt32() + yOffset),
                                new IntPtr(tempData.Scan0.ToInt32() + yOffset + remainingWidth),
                                tileWidthBytes);
                        }
                    }
                    break;
                }
            }

            EndUpdate();
            tempBitmap.UnlockBits(tempData);
        }

        /// <summary>
        /// Scrolls the CDG bitmap data vertically
        /// </summary>
        /// <param name="direction">Direction to scroll pixels.</param>
        /// <param name="fillColorIndex">Color index to fill with (-1 indicates
        /// to copy scrolled off values)</param>
        void ScrollVertical(ScrollChunk.VScrollInstruction direction, int fillColorIndex)
        {
            if (direction == ScrollChunk.VScrollInstruction.None)
            {
                return;
            }

            Bitmap tempBitmap = new Bitmap(Width, Height, PixelFormat.Format4bppIndexed);
            BitmapData tempData = tempBitmap.LockBits(
                new Rectangle(0, 0, Image.Width, Image.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format4bppIndexed);

            BeginUpdate();

            int sizeBytes = _bitmapData.Stride * Height;
            int heightBytes = _bitmapData.Stride * TileBlock.TileHeight;

            // Copy all of the image to the temp buffer
            memcpy(tempData.Scan0, _bitmapData.Scan0, sizeBytes);

            switch (direction)
            {
                case ScrollChunk.VScrollInstruction.Up:
                {
                    // Take TILE_HEIGHT pixels off the top.
                    memcpy(_bitmapData.Scan0, new IntPtr(tempData.Scan0.ToInt32() + heightBytes),
                        sizeBytes - heightBytes);

                    if (fillColorIndex == -1)
                    {
                        // Copy pixel data from the top row into the bottom
                        memcpy(new IntPtr(_bitmapData.Scan0.ToInt32() + sizeBytes - heightBytes),
                            tempData.Scan0, heightBytes);
                    }
                    else
                    {
                        // Fill remaining bytes with the colour index
                        byte colorByte = (byte)(fillColorIndex << 4 | fillColorIndex);
                        memset(new IntPtr(_bitmapData.Scan0.ToInt32() + sizeBytes - heightBytes),
                            colorByte, heightBytes);
                    }

                    break;
                }
                case ScrollChunk.VScrollInstruction.Down:
                {
                    // Take TILE_HEIGHT pixels off the bottom.
                    memcpy(new IntPtr(_bitmapData.Scan0.ToInt32() + heightBytes), tempData.Scan0, 
                        sizeBytes - heightBytes);

                    if (fillColorIndex == -1)
                    {
                        // Copy pixel data from the bottom row to the top
                        memcpy(_bitmapData.Scan0, 
                            new IntPtr(tempData.Scan0.ToInt32() + sizeBytes - heightBytes), 
                            heightBytes);
                    }
                    else
                    {
                        // Fill top row bytes with the colour index
                        byte colorByte = (byte)(fillColorIndex << 4 | fillColorIndex);
                        memset(_bitmapData.Scan0, colorByte, heightBytes);
                    }

                    break;
                }
            }

            EndUpdate();
            tempBitmap.UnlockBits(tempData);
        }

        #endregion

        #region Win32

        [DllImport("msvcrt.dll")]
        private static extern void memset(IntPtr dest, byte val, int count);

        [DllImport("msvcrt.dll")]
        private static extern IntPtr memcpy(IntPtr dest, IntPtr src, int count);
 
        #endregion

        #region Data

        /// <summary>
        /// Bitmap data for direct bit access in drawing operations
        /// </summary>
        BitmapData _bitmapData;

        /// <summary>
        /// Whether to use the test colour table (ignoring the last 
        /// load colour table chunk)
        /// </summary>
        bool _useTestColourTable;

        /// <summary>
        /// Horizontal scroll amount (offset the display by up to 5 pixels left).
        /// ScrollCopy/ScrollPreset moves the CDG pixel data when the Scroll CMD value
        /// is left or right by TILE_WIDTH (6) pixels.
        /// </summary>
        int _horizontalScrollOffset;

        /// <summary>
        /// Vertical scroll amount (offset the display by up to 11 pixels either upwards).
        /// ScrollCopy/ScrollPreset moves the CDG pixel data when the Scroll CMD value
        /// is up or down by TILE_HEIGHT (12) pixels 
        /// </summary>
        int _verticalScrollOffset;

        #endregion
    }
}
