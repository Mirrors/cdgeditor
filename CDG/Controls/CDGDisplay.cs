﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Cdg.Controls
{
    /// <summary>
    /// Control to display CD+G data
    /// </summary>
    public partial class CdgDisplay : UserControl
    {
        #region Overview

        /*
         *  This control works by using a CDGBitmap object to store the pixel data
         *  in the 4bit indexed format that CD+G drawing uses.
         *  
         *  Unlike a real player, this control also shows the contents of the hidden 
         *  border area (the viewable CD+G is 48 tiles wide by 16 tiles high).
         *  
         *  A tile is 6 pixels wide by 12 pixels high.
         * 
         *  A border area of one tile surrounds the display and is normally obscured 
         *  by the border colour on a CD+G player.  This allows drawing into the border 
         *  area so that the drawn tiles can be subsequently scrolled into view.
         *  
         *  I have no CD+G karaoke discs that use this feature and only the limited
         *  documentation from Jim Bumgardner (http://www.jbum.com/cdg_revealed.html).
         *  
         *  Testing for this scroll/hidden tile functionality was carried out using
         *  Ots Studio (www.otslabs.com) and Winamp with the CD+G plugin by Yannick
         *  Heneault (http://www.winamp.com/plugin/cdg-plug-in/100775).
         *  
         *  The user control itself consists of a Panel control and a PictureBox control.
         *  The PictureBox control does all of the drawing with the user control adding
         *  a rectangle to indicate the location of the current Tile Block command when
         *  editing.
         *  
         *  The picture box dimensions are kept in the correct aspect ratio whenever the
         *  user control is resized.  A Panel control is used to clip the visible picture
         *  box when a horizontal and/or vertical scroll offset is applied.
         *  
         *  I found a nice description of the scrolling behaviour in the source code
         *  to pykaraoke http://www.kibosh.org/pykaraoke/ - a karaoke player written in
         *  python.
         *  
         *  To paraphrase - the scroll "Instruction" actually shifts the CDG pixel data
         *  around (here is where the copy or preset makes a difference - preset fills
         *  the new row or column of scrolled CDG tiles with a solid colour, whereas 
         *  copy puts the bitmap data from the scrolled off row or column into the new
         *  row or column).
         *  
         *  The offset just alters how the pixel data is displayed.  The offsets are 0-5
         *  for horizontal (0 = no shift, 5 = five pixels to the left) and 0-11 for 
         *  vertical (0 = no shift, 11 = 11 pixels upwards).
         *  
         *  So to scroll one pixel to the right, you need a Scroll instruction of Right
         *  with an offset of 5.  To scroll futher right you use a None instruction
         *  with the horizontal offset to 4, 3, 2, 1 and finally 0.  Vertically you'd 
         *  use a Down instruction with offset of 11, then None with 10, 9, 8 ... 2, 1, 0.
         */

        #endregion

        #region Constants

        private const int WidthInTiles = 50;
        private const int HeightInTiles = 18;
        private const int BorderWidth = 6;
        private const int BorderHeight = 12;
        private const int CdgWidth = 294;
        private const int CdgHeight = 204;
        private const float ImageRatio = CdgHeight / (float)CdgWidth;

        #endregion

        #region Construction

        /// <summary>
        /// Initialises a new instance of a <see cref="CdgDisplay"/> control
        /// </summary>
        public CdgDisplay()
        {
            InitializeComponent();

            Bitmap = new CdgBitmap();
            _PictureBox.Image = Bitmap.Image;

            Bitmap.Invalidated += _Bitmap_Invalidated;

            _PictureBox.MouseClick += _PictureBox_MouseClick;
            _PictureBox.MouseDoubleClick += _PictureBox_MouseDoubleClick;

            _PictureBox.Paint += _PictureBox_Paint;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns the CDG Bitmap so the caller can run CD+G instructions
        /// against it.
        /// </summary>
        public CdgBitmap Bitmap { get; private set; }

        /// <summary>
        /// Gets or sets the tile location that shows where the
        /// current TileBlock command is drawn
        /// </summary>
        public Point TileLocation
        {
            set
            {
                _location = value;
                Invalidate();
            }
        }

        /// <summary>
        /// Whether to use the test colour table (ignoring the last 
        /// load colour table chunk)
        /// </summary>
        public bool UseTestColourTable
        {
            set
            {
                Bitmap.UseTestColourTable = value;
                Invalidate(true);
            }
        }

        /// <summary>
        /// Whether to show tiles drawn in the border area.
        /// </summary>
        public bool VisibleBorderTiles
        {
            set
            {
                _visibleBorderTiles = value;
                Invalidate(true);
            }
        }

        /// <summary>
        /// Amount to scroll the output display by in CDG pixels.
        /// -ve = left, +ve = right.
        /// </summary>
        public int HorizontalScrollOffset
        {
            set
            {
                _horizontalScroll = value;
                CDGDisplay_Resize(this, new EventArgs());
            }
        }

        /// <summary>
        /// Amount to scroll the output display by in CDG pixels.
        /// -ve = up, +ve = down.
        /// </summary>
        public int VerticalScrollOffset
        {
            set
            {
                _verticalScroll = value;
                CDGDisplay_Resize(this, new EventArgs());
            }
        }

        #endregion

        #region Events

        public delegate void DisplayClickedEventHandler(int x, int y);
        public event DisplayClickedEventHandler Clicked;
        private void RaiseClicked(int x, int y)
        {
            if (Clicked != null)
            {
                Clicked(x, y);
            }
        }

        public event DisplayClickedEventHandler DoubleClicked;
        private void RaiseDoubleClicked(int x, int y)
        {
            if (DoubleClicked != null)
            {
                DoubleClicked(x, y);
            }
        }

        #endregion

        #region Private Methods

        void ScreenToCdg(int screenX, int screenY, out int x, out int y)
        {
            // Screen coordinates are relative to the top left corner
            x = (int)(screenX / _tileWidth);
            y = (int)(screenY / _tileHeight);
        }

        Rectangle GetTileRectangle(Point location)
        {
            var result = new Rectangle(
                (int)(_tileWidth * location.X),
                (int)(_tileHeight * location.Y),
                (int)_tileWidth, (int)_tileHeight);

            return result;
        }

        #endregion

        #region Event Handlers

        void _PictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (_location != Point.Empty)
            {
                var rect = GetTileRectangle(_location);
                rect.Inflate(new Size(1, 1));
                e.Graphics.DrawRectangle(new Pen(Color.FromArgb(50, Color.White)), rect);
            }
            DrawBorderOverlay(e.Graphics);
        }

        private void _ClipPanel_Paint(object sender, PaintEventArgs e)
        {
            // Draw border in area exposed by moving the picture box
            
            var borderColor = Bitmap.Palette.Entries[Bitmap.BorderColour];
            var alpha = _visibleBorderTiles ? 20 : 255;
            var borderBrush = new SolidBrush(Color.FromArgb(alpha, borderColor));

            var region = new Region(new Rectangle(0, 0, _ClipPanel.Width, _ClipPanel.Height));
            region.Exclude(new Rectangle(_PictureBox.Left, _PictureBox.Top, _PictureBox.Width, _PictureBox.Height));
            e.Graphics.FillRegion(borderBrush, region);            
        }

        /// <summary>
        /// Draw the border overlay over the picture box.
        /// </summary>
        /// <param name="graphics"></param>
        private void DrawBorderOverlay(Graphics graphics)
        {
            var borderColor = Bitmap.Palette.Entries[Bitmap.BorderColour];
            var alpha = _visibleBorderTiles ? 20 : 255;
            var borderBrush = new SolidBrush(Color.FromArgb(alpha, borderColor));

            var scaleX = _ClipPanel.Width / (float)(CdgWidth);
            var scaleY = _ClipPanel.Height / (float)(CdgHeight);

            // Calculate visible CDG area (taking scroll offsets into account)
            var left = (int)((BorderWidth + _horizontalScroll) * scaleX);
            var top = (int)((BorderHeight + _verticalScroll) * scaleY);
            var width = (int)((CdgWidth - (BorderWidth * 2)) * scaleX);
            var height = (int)((CdgHeight - (BorderHeight * 2))* scaleY);

            var visibleRect = new Rectangle(left, top, width, height);
            var region = new Region(new Rectangle(0, 0, _PictureBox.Width, _PictureBox.Height));
            region.Exclude(visibleRect);

            //graphics.FillRectangle(borderBrush, visibleRect);
            graphics.FillRegion(borderBrush, region);
        }

        void _PictureBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int x;
            int y;
            ScreenToCdg(e.X, e.Y, out x, out y);
            RaiseDoubleClicked(x, y);
        }

        void _PictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            // Work out the tile coordinates from the mouse position
            //MessageBox.Show(string.Format("X:{0:000} Y:{1:000}", e.X, e.Y));

            int x;
            int y;
            ScreenToCdg(e.X, e.Y, out x, out y);
            RaiseClicked(x, y);
        }

        void _Bitmap_Invalidated(object sender, EventArgs e)
        {
            Invalidate(true);
            HorizontalScrollOffset = Bitmap.HorizontalScrollOffset;
            VerticalScrollOffset = Bitmap.VerticalScrollOffset;
        }

        /// <summary>
        /// Resizes the picture box to fit, keeping the aspect ratio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CDGDisplay_Resize(object sender, EventArgs e)
        {
            // Centre/resize the picture box keeping the aspect ratio
            if (Height / (float)CdgWidth < ImageRatio)
            {
                _ClipPanel.Height = Height;
                _ClipPanel.Width = (int)(Height / ImageRatio);
                _ClipPanel.Top = 0;
                _ClipPanel.Left = (CdgWidth - _ClipPanel.Width) / 2;
            }
            else
            {
                _ClipPanel.Width = CdgWidth;
                _ClipPanel.Height = (int)(CdgWidth * ImageRatio);
                _ClipPanel.Left = 0;
                _ClipPanel.Top = (Height - _ClipPanel.Height) / 2;
            }

            var scaleX = _ClipPanel.Width / (float)(CdgWidth);
            var scaleY = _ClipPanel.Height / (float)(CdgHeight);

            _PictureBox.Width = (int)(CdgWidth * scaleX);
            _PictureBox.Height = (int)(CdgHeight * scaleY);

            // Apply horizontal and vertical scroll to the picture box location
            _PictureBox.Left = (int)(-_horizontalScroll * scaleX);
            _PictureBox.Top = (int)(-_verticalScroll * scaleY);

            _tileWidth = (float)_PictureBox.Width / WidthInTiles;
            _tileHeight = (float)_PictureBox.Height / HeightInTiles;
        }

        #endregion

        #region Data

        /// <summary>
        /// Location of the current tile command rectangle.
        /// In CDG Tile 
        /// </summary>
        private Point _location = Point.Empty;

        /// <summary>
        /// Tile Width in screen coordinates (calculated when control is sized).
        /// </summary>
        private float _tileWidth;

        /// <summary>
        /// Tile Height in screen coordinates (calculated when control is sized).
        /// </summary>
        private float _tileHeight;

        /// <summary>
        /// Whether to make tiles drawn in the border area visible.
        /// </summary>
        private bool _visibleBorderTiles;

        /// <summary>
        /// Horizontal scroll amount (offset the display by up to 5 pixels either way).
        /// ScrollCopy/ScrollPreset moves the CDG pixel data when the Scroll CMD value
        /// is left or right by TILE_WIDTH (6) pixels.
        /// </summary>
        private int _horizontalScroll;

        /// <summary>
        /// Vertical scroll amount (offset the display by up to 11 pixels either way).
        /// ScrollCopy/ScrollPreset moves the CDG pixel data when the Scroll CMD value
        /// is up or down by TILE_HEIGHT (12) pixels 
        /// </summary>
        private int _verticalScroll;

        #endregion                
    }
}
