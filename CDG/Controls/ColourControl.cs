﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Cdg.Controls
{
    public partial class ColourControl : UserControl
    {
        #region Construction

        public ColourControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        public event EventHandler Clicked;
        void RaiseClicked()
        {
            if (Clicked != null)
            {
                Clicked(this, new EventArgs());
            }
        }

        public event EventHandler ColourChanged;
        void RaiseColourChanged()
        {
            if (ColourChanged != null)
            {
                ColourChanged(this, new EventArgs());
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the index of the colour in the instruction.
        /// </summary>
        public int? Index
        {
            get
            {
                return _index;
            }
            set
            {
                _index = value;

                if (_index.HasValue)
                {
                    _IndexLabel.Text = string.Format("{0}", value);

                    _ColourPanel.BackColor = _palette != null ? _palette.Entries[_index.Value] : Color.Black;
                }
                else
                {
                    _IndexLabel.Text = "-";
                    _ColourPanel.BackColor = SystemColors.Control;
                }
            }
        }

        public bool ReadOnly
        {
            set
            {
                _readOnly = value;
            }
        }


        public bool Selected
        {
            set
            {
                BackColor = value ? SystemColors.Highlight : SystemColors.Control;
                _IndexLabel.ForeColor = value ? SystemColors.HighlightText : SystemColors.ControlText;
            }
        }

        /// <summary>
        /// Gets or sets the palette to get the colour from
        /// </summary>
        public ColorPalette Palette
        {
            get
            {
                return _palette;
            }
            set
            {
                _palette = value;
                Index = Index;
            }
        }

        #endregion

        #region Event Handlers

        private void _Panel_Click(object sender, MouseEventArgs e)
        {
            RaiseClicked();

            if (!_readOnly && e.Button == MouseButtons.Left)
            {
                PopulateDropDown();
                var control = sender as Control;
                if (control != null) _ColourContextMenu.Show(control.PointToScreen(e.Location));
            }
        }

        private void _ColourContextMenu_Opening(object sender, CancelEventArgs e)
        {
            PopulateDropDown();
        }

        private void item_Click(object sender, EventArgs e)
        {
            var toolStripItem = sender as ToolStripItem;
            if (toolStripItem != null) Index = (int)toolStripItem.Tag;
            RaiseColourChanged();
        }

        #endregion

        #region Private Methods

        private void PopulateDropDown()
        {
            if (_ColourContextMenu.Items.Count != 0) return;
            for (var i = 0; i < _palette.Entries.Length; i++)
            {
                var item = _ColourContextMenu.Items.Add(string.Format("{0}", i));
                item.Tag = i;
                item.Click += item_Click;
                item.BackColor = _palette.Entries[i];
            }
        }

        #endregion

        #region Data

        /// <summary>
        /// The index of the colour in the instruction
        /// </summary>
        int? _index;

        /// <summary>
        /// The palette to choose the colour from.
        /// </summary>
        ColorPalette _palette;

        bool _readOnly = true;

        #endregion
    }
}
