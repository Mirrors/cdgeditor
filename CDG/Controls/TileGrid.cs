﻿using System.Drawing;
using Cdg.Chunks;

namespace Cdg.Controls
{
    public partial class TileGrid : GridControl
    {
        #region Constants

        private const int TileWidth = 6;
        private const int TileHeight = 12;

        private class Pattern
        {
            private const int PixelsIndex = 8;

            public Pattern(byte[] pixels)
            {
                _pixels = pixels;
            }

            public void Set(Chunk dest)
            {
                _pixels.CopyTo(dest.Data, PixelsIndex);
            }

            readonly byte[] _pixels;
        }

        readonly Pattern[] _patterns = {
            new Pattern(new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00
            }),
            
            new Pattern(new byte[]
            {
                0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
                0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
            }),

            new Pattern(new byte[]
            {
                0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00
            }),

            new Pattern(new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F,
            }),

            new Pattern(new byte[]
            {
                0x38, 0x38, 0x38, 0x38, 0x38, 0x38,
                0x38, 0x38, 0x38, 0x38, 0x38, 0x38,
            }),

            new Pattern(new byte[]
            {
                0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
                0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
            }),

            new Pattern(new byte[]
            {
                0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C,
                0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C,
            }),

            new Pattern(new byte[]
            {
                0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x3F,
                0x3F, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C,
             }),

            new Pattern(new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x3F,
                0x3F, 0x00, 0x00, 0x00, 0x00, 0x00,
            }),
        };

        #endregion

        #region Construction

        /// <summary>
        /// Creates a new instance of a <see cref="TileGrid"/> control.
        /// </summary>
        public TileGrid() : base(new Size(TileWidth, TileHeight))
        {
            CellSize = 18;
        }

        #endregion

        #region Properties

        public TileBlock Chunk
        {
            set
            {
                _chunk = value;
                if (_chunk != null)
                {
                    SetPixels(_chunk);
                }
            }
        }

        #endregion

        #region GridControl Overrides

        protected override void OnChanged()
        {
            GetPixels(_chunk);
        }

        #endregion

        #region Public Methods

        public void SetPattern(int pattern)
        {
            _patterns[pattern].Set(_chunk);
            SetPixels(_chunk);

            Invalidate(true);
            RaiseChanged();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Sets the control pixels from the chunk.
        /// </summary>
        /// <param name="chunk">Chunk to set the control pixels to.</param>
        void SetPixels(TileBlock chunk)
        {
            Pixels.NullPixels = chunk.NullPixels;

            if (Pixels.NullPixels) return;
            for (var row = 0; row < TileHeight; row++)
            {
                var rowPixels = Pixels.GetRow(row);

                for (var column = 0; column < TileWidth; column++)
                {
                    var state = chunk.GetPixel(column, row);
                    rowPixels.Set(column, state);
                }
            }
        }

        /// <summary>
        /// Gets the pixels from the control into the chunk.
        /// </summary>
        /// <param name="chunk"></param>
        private void GetPixels(TileBlock chunk)
        {
            if (chunk == null) return;
            for (var row = 0; row < TileHeight; row++)
            {
                var rowPixels = Pixels.GetRow(row);

                for (var column = 0; column < TileWidth; column++)
                {
                    var state = rowPixels.Get(column);
                    chunk.SetPixel(column, row, state);
                }
            }
        }
        #endregion

        #region Data

        /// <summary>
        /// The chunk being edited
        /// </summary>
        private TileBlock _chunk;

        #endregion
    }
}
