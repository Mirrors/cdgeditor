using System.Reflection;

[assembly: AssemblyCompany("DBS Software")]
[assembly: AssemblyProduct("CDG Editor")]
[assembly: AssemblyCopyright("Copyright � Darryl Baker 2016")]
[assembly: AssemblyVersion("1.6.0")]